﻿using ScytaleC.PacketDecoders;
using ScytaleC.Interfaces;
using System;
using System.Text;
using System.Web.Script.Serialization;
using System.Windows.Forms;

namespace ScytaleC.tester
{
    public partial class MainForm : Form
    {
        private IPacketDetector packetDetector;

        public MainForm()
        {
            InitializeComponent();
            packetDetector = new PacketDetector();
            packetDetector.OnPacket += PacketDetector_OnPacket;
        }

        private void PacketDetector_OnPacket(object sender, PacketArgs e)
        {
            rtbOutput.AppendText(new JsonFormatter(e.SerializedData).Format());
        }

        private void button1_Click(object sender, EventArgs e)
        {
            rtbOutput.Clear();
            try
            {
                foreach (string line in rtbInput.Lines)
                {
                    // Convert hex string to byte array
                    DescrambledFrameArgs dfa = new DescrambledFrameArgs();
                    dfa.DescrambledFrame = Utils.HexStringToBytes(line);
                    if (dfa.DescrambledFrame.Length == 0)
                    {
                        continue;
                    }
                    dfa.Length = dfa.DescrambledFrame.Length;
                    try
                    {
                        packetDetector.Process(dfa);
                    }
                    catch(Exception ex)
                    {
                        if (!ex.Message.Contains("Index was outside the bounds of the array."))
                        {
                            rtbOutput.AppendText(string.Format("Exception {0}", ex.Message));
                            rtbOutput.AppendText(Environment.NewLine);
                        }
                    }
                }
            }
            catch(Exception ex)
            {
                rtbOutput.AppendText(string.Format("Exception {0}", ex.Message));
                rtbOutput.AppendText(Environment.NewLine);
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            rtbOutput.Clear();
            try
            {
                string txt = rtbInput.Text;
                rtbInput.Clear();
                rtbInput.Text = txt;
                foreach (string line in rtbInput.Lines)
                {
                    // Convert hex string to byte array
                    DescrambledFrameArgs dfa = new DescrambledFrameArgs();
                    dfa.DescrambledFrame = Utils.HexStringToBytes(line);
                    if (dfa.DescrambledFrame.Length == 0)
                    {
                        continue;
                    }

                    byte[] textPayload = new byte[640];
                    byte[] binaryPayload = new byte[640];
                    int startpos = (int)startPos.Value;
                    int j = startpos;
                    int i = 0;
                    for (; j < dfa.DescrambledFrame.Length - 1; j++)
                    {
                        textPayload[i] = (byte)(dfa.DescrambledFrame[j] & 0x7F);
                        binaryPayload[i] = dfa.DescrambledFrame[j];
                        i++;
                    }

                    Encoding enc = Encoding.GetEncoding("x-IA5",
                                          new EncoderExceptionFallback(),
                                          new DecoderExceptionFallback());
                    string s = "";
                    try
                    {
                        s = enc.GetString(textPayload, 0, j - (int)startPos.Value);
                    }
                    catch (Exception ex)
                    {
                        Utils.Log.Error(ex);
                    }
                    if (PacketDecoderUtils.IsBinary(binaryPayload))
                    {
                        rtbOutput.AppendText("BINARY: ");
                    }
                    else
                    {
                        rtbOutput.AppendText("ASCII: ");
                    }
                    rtbOutput.AppendText(new JsonFormatter(new JavaScriptSerializer().Serialize(s)).Format());
                    rtbOutput.AppendText(Environment.NewLine);
                }
            }
            catch (Exception ex)
            {
                rtbOutput.AppendText(string.Format("Exception {0}", ex.Message));
                rtbOutput.AppendText(Environment.NewLine);
            }
        }
    }
}
