﻿/*
 * microp11 2018
 * 
 * This file is part of Scytale-C.
 * 
 * Scytale-C is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Scytale-C is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Scytale-C.  If not, see <http://www.gnu.org/licenses/>.
 *  
 *   Bibliography:
 * 
 *   Calcutt, D. M., & Tetley, L. (2004). Satellite communications: principles and applications. Oxford: Elsevier.
 *   https://www.amazon.com/Satellite-Communications-Applications-David-Calcutt/dp/034061448X
 *   
 *   Nera. (2015) Nera Inmarsat-C Service Manual. Billingstad: Nera ASA.
 *   https://www.manualslib.com/manual/1201514/Nera-Inmarsat-C.html
 *
 * The interleave matrix (block) consists of 64 rows by 162 columns.
 * The matrix holds 10368 symbols.
 * The block is transmitted on a row by row basis.
 * The symbols in a row are transmitted in ascending order of column position, UW words first.
 * The rows are not transmitted in sequential order.
 * They are transmitted according to a permutted sequence.
 * If the rows in the interleaved block are numbered from i = 0 to i = 63 sequentially and the
 * transmitted order is from j = 0 sequentially to j = 63, then i and j are related as it follows:
 * i = (j x 39) modulo 64
 * j = (i x 23) modulo 64
 * 1st output j = 0, i = (0 x 39) modulo 64 = 0
 * 2nd output j = 1, i = (1 x 39) modulo 64 = 39
 * 3rd output j = 2, i = (2 x 39) modulo 64 = 14
 * 
 * For any of the results, both i an j can only have values from 0 to 63.
 * Any bits representing values of 64, 128 and above can be ignored.
 * The final value is given by the right-hand six bits only.
 * 
 * When the frame gets into the Depermuter on the receiver side, the original matrix is an
 * array of symbols, where the first arriving line (j above) occupies the highest index in the array.
 * The highest position in the array is occupied by the two UW symbols of the first line.
 * 
 * We first reverse this symbol array aka frame aka block aka matrix.
 * After reversing the array, we obtain the original matrix, permutted, where i=j=0
 * We depermute it by using the i derived from j above:
 * 1st output i = 0, j = (0 x 23) modulo 64 = 0
 * 2nd output i = 1, j = (1 x 23) modulo 64 = 23
 * 3rd output i = 2, j = (2 x 23) modulo 64 = 46
 *
 * We also know that each row has 162 columns. It means we can precompute a depermutting array that
 * would help us depermute in a simple loop the entire frame. We can precompute this array either in
 * the constructor or when processing the first packet. It is more economical than calculating the
 * values on-the-fly.
 * 
 */

using System;
using System.Diagnostics;
using System.Threading.Tasks.Dataflow;

namespace ScytaleC.Decoder
{
    class Depermuter
    {
        private int[] depermuttingArray;

        public Depermuter()
        {
            //generate depermutting array
            depermuttingArray = new int[64];
            for (int i = 0; i < 64; i++)
            {
                //j(i)
                depermuttingArray[i] = (i * 23) % 64;

                //take right-hand 6 bits only
                depermuttingArray[i] &= 0x3F;

                //get index in source UWFrame (assumes a reversed UWFrame)
                depermuttingArray[i] *= 162;
            }
        }

        public async void Decode(BufferBlock<UWFinderFrameArgs> uWFinderFrameBuffer, BufferBlock<DepermutedFrameArgs> depermutedFrameBuffer)
        {
            while (await uWFinderFrameBuffer.OutputAvailableAsync())
            {
                UWFinderFrameArgs args = uWFinderFrameBuffer.Receive();

                //reverse frame
                Array.Reverse(args.UWFrame);

                //create the destination
                byte[] destination = new byte[DataConsts.DepermutedFrameLength];

                //depermute
                for (int i = 0; i < 64; i++)
                {
                    Array.Copy(args.UWFrame, depermuttingArray[i], destination, i * 162, 162);
                }

                DepermutedFrameArgs dfa = new DepermutedFrameArgs
                {
                    Length = DataConsts.DepermutedFrameLength,
                    DepermutedFrame = destination,
                    IsHardDecision = args.IsHardDecision
                };
                depermutedFrameBuffer.Post(dfa);
            }
        }
    }
}
