﻿/*
 * microp11 2018
 * 
 * This file is part of Scytale-C.
 * 
 * Scytale-C is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Scytale-C is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Scytale-C.  If not, see <http://www.gnu.org/licenses/>.
 * 
 *   Bibliography:
 *   
 *   Nera. (2015) Nera Saturn C Technical Manual.Billingstad: Nera ASA.
 *   ftp://161.87.213.193.static.cust.telenor.com/Manuals/Saturn%20C/SatC_Marine_Tech_Manual_A.pdf
 *   
 *   This is the decoding chain employed by Scytale-C:
 *   Rx Source
 *   Demodulator
 *   UW Finder
 *   Depermuter
 *   Deinterleaver
 *   Viterbi Decoder
 *   Descrambler
 *   Packet detector and CRC
 *   Message detector
 * 
 */

using System;
using System.Numerics;

namespace ScytaleC.Decoder
{
    /// <summary>
    /// Demodulator output.
    /// UWFinder input.
    /// 
    /// The output is served in chunks of soft or hard bits, demodulator dependent.
    /// </summary>
    public class DemodulatedSymbolsArgs : EventArgs
    {
        public int Length { get; set; }
        public byte[] Symbols { get; set; }
        public bool IsHardDecision { get; set; }
    }

    /// <summary>
    /// UWFinder output.
    /// Depermuter input.
    /// 10368 symbols
    /// 
    /// From this moment on we are working on "frames" of data, the stream has been cut into parts of given length.
    /// The symbols in the UWFrame are still in the order they came out of the demodulator.
    /// </summary>
    public class UWFinderFrameArgs : EventArgs
    {
        public int SymbolCount { get; set; }
        public int Length { get; set; }
        public byte[] UWFrame { get; set; }
        public bool IsHardDecision { get; set; }
        public bool IsReversedPolarity { get; set; }
        public bool IsMidStreamReversePolarity { get; set; }
        public bool IsUncertain { get; set; }
        public int BER { get; set; }
    }

    /// <summary>
    /// Depermuter output.
    /// Deinterleaver input.
    /// 10368 symbols
    /// 
    /// At this point the content of the frame is in the same order as
    /// it was inside the decoder before permuting.
    /// </summary>
    public class DepermutedFrameArgs : EventArgs
    {
        public int Length { get; set; }
        public byte[] DepermutedFrame { get; set; }
        public bool IsHardDecision { get; set; }
    }

    /// <summary>
    /// Deinterleaver output.
    /// Viterbi decoder input.
    /// 10240 symbols
    /// 
    /// The UW symbols have been discarded and only data symbols remain.
    /// </summary>
    public class DeinterleavedFrameArgs : EventArgs
    {
        public int Length { get; set; }
        public byte[] DeinterleavedFrame { get; set; }
        public bool IsHardDecision { get; set; }
    }

    /// <summary>
    /// Viterbi decoder output.
    /// Descrambler input.
    /// 640 bytes
    /// </summary>
    public class ViterbiFrameArgs : EventArgs
    {
        public int Length { get; set; }
        public byte[] ViterbiFrame { get; set; }
    }

    /// <summary>
    /// Descrambler output.
    /// Packer decoder and CRC input.
    /// 640 bytes
    /// This contains the data frame ready to be decoded for packets.
    /// 
    /// The frame number and Time is set while decoding the bulletin board packet.
    /// Only the bulletin board packet contains this information and this way we
    /// provide a cheap reference to the containing frame for all contained packets.
    /// </summary>
    public class DescrambledFrameArgs : EventArgs
    {
        public int Length { get; set; }
        public byte[] DescrambledFrame { get; set; }
        public int FrameNumber { get; set; }
        public bool IsBadBulletinBoard { get; set; }
        public DateTime Time { get; set; }
    }

    /// <summary>
    /// Demodulator triggered event params.
    /// </summary>
    public class SignalAttributesArgs : EventArgs
    {
        public double Frequency { get; set; }
        public int MeanMagnitude { get; set; }
        public bool IsInSync { get; set; }
        public int SyncCounter { get; set; }
        public Complex ScatterPoint { get; set; }
        public bool IsCMAEnabled { get; set; }
        public bool IsAGCEnabled { get; set; }
    }

    /// <summary>
    /// BPSK Demodulator and FFT input.
    /// </summary>
    public class AudioSampleArgs : EventArgs
    {
        public int Length { get; set; }
        public Complex[] Samples { get; set; }
    }
}