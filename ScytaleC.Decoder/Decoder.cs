﻿/*
 * microp11 2018
 * 
 * This file is part of Scytale-C.
 * 
 * Scytale-C is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Scytale-C is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Scytale-C.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * https://stackoverflow.com/questions/461742/how-to-convert-an-ipv4-address-into-a-integer-in-c
 * 
 * 
 */

using System;
using System.Threading.Tasks.Dataflow;

namespace ScytaleC.Decoder
{
    public class Decoder
    {
        private BufferBlock<DemodulatedSymbolsArgs> demodulatedSymbolsBuffer;
        private BufferBlock<UWFinderFrameArgs> uWFinderFrameBuffer;
        private UWFinder uwFinder;

        private BufferBlock<DepermutedFrameArgs> depermutedFrameBuffer;
        private Depermuter depermuter;

        private BufferBlock<DeinterleavedFrameArgs> deinterleavedFrameBuffer;
        private Deinterleaver deinterleaver;

        private BufferBlock<ViterbiFrameArgs> viterbiFrameBuffer;
        private PKViterbiDecoder viterbiDecoder;

        private Descrambler descrambler;

        public event EventHandler<DemodulatedSymbolsArgs> OnDemodulatedSymbols;

        public Decoder(int tolerance, BufferBlock<byte[]> inBuffer, BufferBlock<byte[]> outBuffer)
        {
            //start the UWFinder
            demodulatedSymbolsBuffer = new BufferBlock<DemodulatedSymbolsArgs>();
            uWFinderFrameBuffer = new BufferBlock<UWFinderFrameArgs>();
            uwFinder = new UWFinder();
            uwFinder.SetTolerance(tolerance);
            uwFinder.Decode(demodulatedSymbolsBuffer, uWFinderFrameBuffer);

            //start the Depermuter
            depermutedFrameBuffer = new BufferBlock<DepermutedFrameArgs>();
            depermuter = new Depermuter();
            depermuter.Decode(uWFinderFrameBuffer, depermutedFrameBuffer);

            //start the Deinterleaver
            deinterleavedFrameBuffer = new BufferBlock<DeinterleavedFrameArgs>();
            deinterleaver = new Deinterleaver();
            deinterleaver.Decode(depermutedFrameBuffer, deinterleavedFrameBuffer);

            //start the Viterbi Decoder
            viterbiFrameBuffer = new BufferBlock<ViterbiFrameArgs>();
            viterbiDecoder = new PKViterbiDecoder();
            viterbiDecoder.Decode(deinterleavedFrameBuffer, viterbiFrameBuffer);

            //start the Decrambler
            descrambler = new Descrambler();
            descrambler.Decode(viterbiFrameBuffer, outBuffer);

            Decode(inBuffer);
        }

        private async void Decode(BufferBlock<byte[]> inBuffer)
        {
            while (await inBuffer.OutputAvailableAsync())
            {
                byte[] chunk = inBuffer.Receive();

                DemodulatedSymbolsArgs dsa = new DemodulatedSymbolsArgs
                {
                    Length = chunk.Length,
                    Symbols = chunk,
                    IsHardDecision = true
                };
                demodulatedSymbolsBuffer.Post(dsa);

                //update UI
                OnDemodulatedSymbols?.BeginInvoke(this, dsa, null, null);
            }
        }
    }
}
