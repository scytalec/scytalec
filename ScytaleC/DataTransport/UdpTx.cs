/*
 * microp11 2018
 * 
 * This file is part of Scytale-C.
 * 
 * Scytale-C is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Scytale-C is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Scytale-C.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * https://stackoverflow.com/questions/461742/how-to-convert-an-ipv4-address-into-a-integer-in-c
 * 
 * Sends a UDP packet over the selected network card.
 * 
 */

using System;
using System.Net;
using System.Net.NetworkInformation;
using System.Net.Sockets;
using System.Text;

namespace ScytaleC
{
    public class UdpTx
    {
        private UdpClient udpc;

        public UdpTx()
        {
            if (udpc != null)
            {
                udpc.Close();
                udpc = null;
            }

            udpc = new UdpClient();
        }

        public void Close()
        {
            udpc?.Close();
        }

        public void Send(string host, int port, string message)
        {
            byte[] data = Encoding.ASCII.GetBytes(message);
            udpc?.Connect(host, port);
            udpc?.Send(data, data.Length);
        }

        public void Send(string host, int port, byte[] data)
        {
            string message = BitConverter.ToString(data).Replace("-", string.Empty);
            Send(host, port, message);
        }
    }
}
