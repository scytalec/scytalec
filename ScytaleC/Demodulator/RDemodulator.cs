﻿/*
 * 
 */

using System;
using System.Numerics;
using System.Threading.Tasks.Dataflow;
using ScytaleC.Decoder;

namespace ScytaleC
{
    class RDemodulator
    {
        private int noSyncCounter;
        private double wcarrier;

        private readonly int order;
        private readonly double[] a;

        private double charge;
        private bool databit;
        private readonly double beta;
        private readonly double betaclock;
        private double thetacarrier;
        private double isamplefilt;
        private double qsamplefilt;
        private double ncocontrolcarrier;
        private readonly double[] isamplefilterbuffer;
        private readonly double[] qsamplefilterbuffer;
        private double thetaclockminusone;
        private double wclock;
        private int clockminusone;
        private double ncocontrolclock;
        private double qsamplefiltminustwo;
        private double qsamplefiltminusone;
        private readonly byte[] denrzidata;
        private double gaincontrol;
        private double cqsamplefiltminusthree;
        private double lock_lp;
        private double lowerafclimit;
        private double upperafclimit;
        public bool Locked { get; private set; }
        private int m;
        private byte[] dst;

        public RDemodulator()
        {
            wcarrier = (2 * Math.PI * DemodulatorConsts.CenterFrequency) / DemodulatorConsts.SampleRate;
            wclock = (2 * Math.PI * DemodulatorConsts.SymbolRate) / DemodulatorConsts.SampleRate;
            order = 64;
            lowerafclimit = (1800 * Math.PI) / DemodulatorConsts.SampleRate;
            upperafclimit = (4600 * Math.PI) / DemodulatorConsts.SampleRate;

            a = new double[order];
            charge = 0.0;
            databit = false;
            beta = 0.00022499999999999999D;
            betaclock = 0.0;
            thetacarrier = 0.0;
            isamplefilt = 0.0;
            qsamplefilt = 0.0;
            ncocontrolcarrier = 0.0;
            isamplefilterbuffer = new double[order];
            qsamplefilterbuffer = new double[order];
            thetaclockminusone = 0.0;
            clockminusone = 0;
            ncocontrolclock = 0.0;
            qsamplefiltminustwo = 0.0;
            qsamplefiltminusone = 0.0;
            denrzidata = new byte[1];
            Locked = false;
            gaincontrol = 1.0;
            cqsamplefiltminusthree = 0.0;
            lock_lp = 0.0;
            m = -1;
            dst = new byte[5000];

            for (int tablefiller = 0; tablefiller < order; tablefiller++)
            {
                double dest = (tablefiller - (order - 1) / 2.0) / order;
                a[tablefiller] = ((0.5 + Math.Cos(dest * 2.0 * Math.PI) / 2.0) * 2.0) / order;
            }
            
        }

        public void ProcessSamples(Complex[] samples, BufferBlock<DemodulatedSymbolsArgs> outBuffer)
        {
            double magnitude = 0.0;
            int length = samples.Length;

            for (int i = 0; i < length; i++)
            {
                magnitude += Math.Abs(samples[i].Real);
                gaincontrol = magnitude / length;
            }

            //double freqout = (wcarrier * DemodulatorConsts.SampleRate) / (2 * Math.PI);
            int gaincontrolout = (int)gaincontrol;

            double locki = 0.0;
            double lockq = 0.0;
            for (int i = 0; i < length; i++)
            {
                thetacarrier %= (2 * Math.PI);
                double icarrierosc = Math.Cos(thetacarrier);
                double qcarrierosc = Math.Sin(thetacarrier);
                double gaincontrolledsample = samples[i].Real / gaincontrol;
                double isample = icarrierosc * gaincontrolledsample;
                double qsample = qcarrierosc * gaincontrolledsample;
                isamplefilterbuffer[0] = isample;
                qsamplefilterbuffer[0] = qsample;
                double cqsamplefilt = 0.0;
                for (int j = 0; j < order; j++)
                {
                    cqsamplefilt += a[j] * qsamplefilterbuffer[j];
                }

                Array.Copy(isamplefilterbuffer, 0, isamplefilterbuffer, 1, isamplefilterbuffer.Length - 1);
                Array.Copy(qsamplefilterbuffer, 0, qsamplefilterbuffer, 1, qsamplefilterbuffer.Length - 1);
                isamplefilt = 0.90000000000000002 * isamplefilt + 0.10000000000000001 * isample;
                qsamplefilt = 0.90000000000000002 * qsamplefilt + 0.10000000000000001 * qsample;
                locki = isamplefilt * isamplefilt;
                lockq = qsamplefilt * qsamplefilt;
                double filteredphase = qsamplefilt * isamplefilt;
                wcarrier += beta * ncocontrolcarrier;
                thetacarrier += wcarrier + 0.029999999999999999 * ncocontrolcarrier;
                ncocontrolcarrier = filteredphase;

                if (wcarrier < lowerafclimit)
                {
                    wcarrier = lowerafclimit;
                }

                if (wcarrier > upperafclimit)
                {
                    wcarrier = upperafclimit;
                }

                double thetaclock = thetaclockminusone + wclock + -0.16 * ncocontrolclock;
                thetaclock %= (2 * Math.PI);

                double sinclockosc = Math.Sin(thetaclock);
                wclock += betaclock * ncocontrolclock;

                int clock;
                if (sinclockosc >= 0.0)
                {
                    clock = 1;
                }
                else
                {
                    clock = -1;
                }

                if (thetaclockminusone < Math.PI && thetaclock >= Math.PI)
                {
                    int decision;
                    if (qsamplefiltminusone > 0.0)
                    {
                        decision = 1;
                    }
                    else
                    {
                        decision = -1;
                    }
                    double steepness = cqsamplefilt - cqsamplefiltminusthree;
                    ncocontrolclock = decision * steepness;
                }

                charge += qsample;
                if (clock == 1 && clockminusone == -1)
                {
                    if (charge > 0.0)
                    {
                        databit = true;
                    }

                    if (charge < 0.0)
                    {
                        databit = false;
                    }

                    charge = 0.0;
                    if (databit)
                    {
                        denrzidata[0] = 1;
                    }
                    else
                    {
                        denrzidata[0] = 0;
                    }

                    m++;
                    if (m > dst.Length-1)
                    {
                        m = 0;
                        DemodulatedSymbolsArgs ca = new DemodulatedSymbolsArgs();
                        ca.Length = DemodulatorConsts.SymbolsPerChunk;
                        Array.Reverse(dst);
                        ca.Symbols = dst;
                        ca.IsHardDecision = true;
                        outBuffer.Post(ca);
                        //Array.Clear(dst, 0, DemodulatorConsts.SymbolsPerChunk);
                    }
                    dst[m] = denrzidata[0];
                }
                thetaclockminusone = thetaclock;
                cqsamplefiltminusthree = qsamplefiltminustwo;
                qsamplefiltminustwo = qsamplefiltminusone;
                qsamplefiltminusone = cqsamplefilt;
                clockminusone = clock;
            }

            double @lock = locki / lockq;

            if (@lock < lock_lp)
            {
                lock_lp = lock_lp * 0.90000000000000002 + @lock * 0.10000000000000001;
            }
            else
            {
                lock_lp = lock_lp * 0.98999999999999999 + @lock * 0.01;
            }

            if (lock_lp < 0.40000000000000002 && gaincontrolout > 50)
            {
                Locked = true;
            }

            if (lock_lp >= 1.0)
            {
                Locked = false;
            }
            noSyncCounter += (Locked) ? 0 : 1;
        }

        public void SetCenterFrequency(double centerFrequency)
        {
            wcarrier = (2 * Math.PI * centerFrequency) / DemodulatorConsts.SampleRate;
        }
        internal void SetLowerFrequency(int loFreq)
        {
            lowerafclimit = (2 * Math.PI * loFreq) / DemodulatorConsts.SampleRate;
        }

        internal void SetUpperFrequency(int hiFreq)
        {
            upperafclimit = (2 * Math.PI * hiFreq) / DemodulatorConsts.SampleRate;
        }
    }
}