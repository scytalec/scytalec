﻿/*
 * microp11 2018
 * 
 * This file is part of Scytale-C.
 * 
 * Scytale-C is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Scytale-C is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Scytale-C.  If not, see <http://www.gnu.org/licenses/>.
 * 
 *   Bibliography:
 *   
 *   Digital Communications Playlists
 *   Bernd Porr, UofG
 *   https://www.youtube.com/channel/UCeFwNKTf19bJL1K5yYTpdqg/playlists
 *
 *   bpsk encoder/decoder using java audio 
 *   f4grx/jbpsk 
 *   https://github.com/f4grx/jbpsk
 *
 *   Design and Implementation of High Performance BPSK Demodulator for Satellite Communications
 *   by Tofigh, E.
 *   MS Thesis, TUDelft
 *   https://repository.tudelft.nl/islandora/object/uuid%3Ac102b61b-5bad-4550-9dbb-33ca2e191bd9
 *
 *   Practical Costas loop design - Designing a simple and inexpensive BPSK Costas loop carrier recovery circuit
 *   Feigin, J. 
 *   in RF DESIGN; 25; 20-37; RF DESIGN
 *   by CARDIFF PUBLISHING COMPANY INC; 2002
 * 
 *   JDSKA, DSCA demodulator and decoder written in C++ Qt
 *   jontio/JDSCA 
 *   http://jontio.zapto.org/hda1/jdsca.html 
 *   https://github.com/jontio/JDSCA
 *   
 *   Telecommunication Breakdown: Concepts of Communication Transmitted via Software-Defined Radio
 *   by C. R. Johnson, Jr. and W. A. Sethares
 *   http://sethares.engr.wisc.edu/telebreak.html
 *   
 *   Software Defined Radio Using MATLAB & Simulink and the RTL-SDR
 *   by K. Barlee, University of Strathclyde, D. Atkinson, University of Strathclyde, R.W. Stewart, University of Strathclyde
 *   L. Crockett, University of Strathclyde
 *   ISBN: 978-0-9929787-1-6
 *   http://www.desktopsdr.com/
 *   
 *   Delfi-C3 Telemetry Reception, Rascal & Warbler
 *   http://www.delfispace.nl/operations/delfi-c3-telemetry-reception
 *      
 *   Communication for Beginners in Matlab: BPSK Modulation Demodulation With AWGN Channel
 *   rupam rupam
 *   https://www.youtube.com/watch?v=B4TFTT1rnYc
 *   
 *   Gaussianwaves.com - Signal Processing Simplified
 *   http://www.gaussianwaves.com/2013/11/symbol-timing-recovery-for-qpsk-digital-modulations/
 *  
 *   WinPSK
 *   http://www.moetronix.com/ae4jy/winpsk.htm
 *  
 *   David Dorran Playlists and Matlab
 *   https://www.youtube.com/user/ddorran/playlists
 *   https://dadorran.wordpress.com/
 *  
 *   MATLAB code for the MDemodulator used by Scytale-C (source code and Wiki)
 *   https://bitbucket.org/scytalec/scytalec-matlab
 *   https://bitbucket.org/scytalec/scytalec-matlab/wiki/Home
 *   
 */

using ScytaleC.Decoder;
using System;
using System.Diagnostics;
using System.IO;
using System.Numerics;
using System.Reflection;
using System.Threading.Tasks.Dataflow;

namespace ScytaleC
{
    internal class DemodulatorConsts
    {
        internal const int SymbolRate = 1200;
        internal const double SampleRate = 48000;
        internal const int SymbolsPerChunk = 5000;

        // As the carrier gets locked, the positive and the negative error are equally distributed and
        // their sum gets closer and closer to zero. This is the absolute number I came up with. 
        internal const double MaxAbsErrSum = 12;

        internal const double Alpha = 0.0064;

        // Signal attributes decimation factor
        internal const int SignalAttributesDecimationFactor = 2;

        public static double CenterFrequency = 2000.0;
    }

    public sealed class Demodulator
    {
        private double frequency = DataConsts.InmarsatCCenterFrequency;
        readonly double alpha = DemodulatorConsts.Alpha;
        readonly double beta;
        private double omega;
        private double phase = 0.0;
        private long sampleCount;
        private int noSyncCounter = 0;
        private double error;
        private bool isInSync = false;
        private int flagcounter;
        private Complex scatterPoint;

        // LPF1, LPF2
        // The literature suggestes that the square-shaped LPF filter should have a
        // bandwidth of no less than half the symbol rate. This will remove the most
        // noise possible without reducing the amplitude of the desired signal at
        // its sampling instants.
        private readonly double[] blpf12 = { 0.9, 0.9 };
        private FIR lpf1;
        private FIR lpf2;

        // The literature indicates that the loop filter should have a
        // response far outside the LPF1/LPF2.The fastest achievable settle
        // time is one in which the VCO has a gain 8x that of the LPF1/LPF2 pole
        // frequency.For LPF3, a factor of six times K or 12 times the LPF1/LPF2
        // pole is a better choice
        private readonly double[] blpf3 = { 0.48, 0.48 };
        private FIR lpf3;

        private RRC rrcRe;
        private RRC rrcIm;
        private RRC rrcRe2;
        private RRC rrcIm2;

        private Gardner gardner;
        private CMA cma;
        private bool cmaEnabled = false;
        private RDemodulator rd;
        private BufferBlock<byte[]> bb;

        private byte[] symbolBuffer;
        private bool agcEnabled = false;

        private int loFreq;
        private int hiFreq;

        public Demodulator()
        {
            flagcounter = 0;
            symbolBuffer = new byte[DemodulatorConsts.SymbolsPerChunk];

            // see literature
            beta = alpha * alpha / 4;

            // carrier
            omega = 2 * Math.PI * frequency / DemodulatorConsts.SampleRate;

            // I and Q LPFs
            lpf1 = new FIR(blpf12);
            lpf2 = new FIR(blpf12);

            // loop LPF
            lpf3 = new FIR(blpf3);

            rrcRe = new RRC(1, 17, DemodulatorConsts.SampleRate, DemodulatorConsts.SymbolRate);
            rrcIm = new RRC(1, 17, DemodulatorConsts.SampleRate, DemodulatorConsts.SymbolRate);
            rrcRe2 = new RRC(1, 11, DemodulatorConsts.SampleRate, DemodulatorConsts.SymbolRate);
            rrcIm2 = new RRC(1, 11, DemodulatorConsts.SampleRate, DemodulatorConsts.SymbolRate);

            gardner = new Gardner(DemodulatorConsts.SampleRate, DemodulatorConsts.SymbolRate);

            loFreq = DataConsts.LoFrequency;
            hiFreq = DataConsts.HiFrequency;

            rd = new RDemodulator();
            rd.SetCenterFrequency(2000);
            bb = new BufferBlock<byte[]>();
        }

        public bool CmaEnabled
        {
            get
            {
                return cmaEnabled;
            }

            set
            {
                cmaEnabled = value;
            }
        }

        public bool AgcEnabled
        {
            get
            {
                return agcEnabled;
            }

            set
            {
                agcEnabled = value;
            }
        }

        public void SetLoFreq(int value)
        {
            loFreq = value;
            rd.SetLowerFrequency(loFreq);
        }

        public void SetHiFreq(int value)
        {
            hiFreq = value;
            rd.SetUpperFrequency(hiFreq);
        }

        public void SetCenterFrequency(double centerFrequency)
        {
            omega = (2 * Math.PI * centerFrequency) / DemodulatorConsts.SampleRate;
            rd.SetCenterFrequency(centerFrequency);
        }

        public bool IsCMASupported()
        {
            return true;
        }

        public bool IsAGCSupported()
        {
            return true;
        }

        public void CmaReset()
        {
            cma?.CmaReset();
        }

        public async void Demodulate(BufferBlock<AudioSampleArgs> inBuffer, BufferBlock<DemodulatedSymbolsArgs> outBuffer, BufferBlock<SignalAttributesArgs> signalBuffer)
        {
            while (await inBuffer.OutputAvailableAsync())
            {
                AudioSampleArgs args = inBuffer.Receive();

                for (int i = 0; i < args.Length; i++)
                {
                    //log input
                    LoggerIQ.WriteFloatsToFile((float)args.Samples[i].Real, (float)args.Samples[i].Imaginary);
                }

                rd.ProcessSamples(args.Samples, outBuffer);

                double vI;
                double vQ;
                double I;
                double Q;
                double magnitude = 0.0;
                double meanMagnitude;
                Complex[] samples = args.Samples;
                int length = args.Length;
                double Irrc;
                double Qrrc;
                double Irrc2;
                double Qrrc2;

                // sample normalization
                for (int i = 0; i < length; i++)
                {
                    magnitude += Complex.Abs(samples[i]);
                }

                meanMagnitude = magnitude / length;


                for (int i = 0; i < length; i++)
                {
                    samples[i] = samples[i] / meanMagnitude;
                }

                // There are about 100 sets of samples for each packet.
                // There are 4096 samples per set.
                // We decimate the calls to this event for everyone's sake.
                // A factor of 2 will give us 50 calls per packet, that is 50 calls per 8.4s,
                // which is 5.9 calls per second. Should be more than enough for the human eye.
                if (sampleCount % DemodulatorConsts.SignalAttributesDecimationFactor == 0)
                {
                    SignalAttributesArgs sa = new SignalAttributesArgs
                    {
                        Frequency = frequency,
                        MeanMagnitude = (int)meanMagnitude,
                        IsInSync = isInSync,
                        SyncCounter = noSyncCounter,
                        ScatterPoint = scatterPoint,
                        IsCMAEnabled = false,
                        IsAGCEnabled = false
                    };
                    signalBuffer.Post(sa);
                }

                //process current args set of samples
                double err_summed = 0;

                for (int i = 0; i < length; i++)
                {
                    sampleCount++;

                    // Costas classic carrier recovery, ignore/eliminate the phase info)
                    // Use both sin and cos from the VCO and create the voltage by combining them
                    frequency = (omega * DemodulatorConsts.SampleRate) / (2 * Math.PI);

                    // phase
                    phase = phase + omega + alpha * error;

                    // carrier
                    omega += beta * error;

                    // keep frequency in range
                    if (frequency < loFreq)
                    {
                        frequency = loFreq;
                        omega = 2 * Math.PI * frequency / DemodulatorConsts.SampleRate;
                    }

                    if (frequency > hiFreq)
                    {
                        frequency = hiFreq;
                        omega = 2 * Math.PI * frequency / DemodulatorConsts.SampleRate;
                    }

                    // keep phase in range
                    while (phase > 2 * Math.PI)
                    {
                        phase -= 2 * Math.PI;
                    }

                    // VCO
                    vI = Math.Cos(phase);
                    vQ = -Math.Sin(phase);

                    // mixer
                    I = vI * samples[i].Real;
                    Q = vQ * samples[i].Imaginary;

                    // LPFs 
                    I = lpf1.Filter(I);
                    Q = lpf2.Filter(Q);

                    // VCO error
                    error = I * Q;

                    // loop filter
                    error = lpf3.Filter(error);

                    // Summing up all errors in this args sample set
                    // As the carrier gets locked, the positive and the negative error are equally distributed and
                    // their sum gets closer and closer to zero.
                    err_summed += error;

                    // RRC
                    // We are trying to get to make every single pulse a sharp triangle to provide the input for the Gardner
                    Irrc = rrcRe.Filter(I);
                    Qrrc = rrcIm.Filter(Q);
                    Irrc2 = rrcRe2.Filter(Irrc);
                    Qrrc2 = rrcIm2.Filter(Qrrc);

                    // Gardner
                    if (!gardner.Step(Irrc2, Qrrc2, out Complex gardnerOutputSample, out bool isOnPoint))
                    {
                        continue;
                    }

                    if (!isOnPoint)
                    {
                        continue;
                    }

                    scatterPoint = gardnerOutputSample;

                    //the I output is the demodulated soft symbol
                    if (double.IsNaN(scatterPoint.Real))
                    {
                        continue;
                    }

                    int iBit = (Math.Sign(scatterPoint.Real) + 1) / 2;
                    byte hardBit = (byte)iBit;

                    Array.Copy(symbolBuffer, 0, symbolBuffer, 1, symbolBuffer.Length - 1);
                    symbolBuffer[0] = hardBit;
                    flagcounter++;

                    if (flagcounter == DemodulatorConsts.SymbolsPerChunk)
                    {
                        byte[] symbols = new byte[DemodulatorConsts.SymbolsPerChunk];
                        Array.Copy(symbolBuffer, symbols, DemodulatorConsts.SymbolsPerChunk);

                        DemodulatedSymbolsArgs ca = new DemodulatedSymbolsArgs();
                        ca.Length = DemodulatorConsts.SymbolsPerChunk;
                        ca.Symbols = symbols;
                        ca.IsHardDecision = true;
                        //outBuffer.Post(ca);

                        flagcounter = 0;
                        Array.Clear(symbolBuffer, 0, DemodulatorConsts.SymbolsPerChunk);
                    }
                }

                isInSync = (Math.Abs(err_summed) < DemodulatorConsts.MaxAbsErrSum);

                noSyncCounter += (isInSync) ? 0 : 1;
            }
        }
    }
        public static class LoggerIQ
    {
        private static readonly string root;
        private static readonly BinaryWriter writer;

        static LoggerIQ()
        {
            root = $@"{Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location)}\\_data";
            DirectoryInfo di = Directory.CreateDirectory(root);
            string f = $@"{di.FullName}\IQ-{DateTime.UtcNow:yyyy-MM-dd-HH-mm}.dat";
            FileStream file = File.OpenWrite(f);
            writer = new BinaryWriter(file);
        }

        /// <summary>
        /// BinaryWriter is guaranteed to use little-endian format, so in your Matlab call,
        /// you should specify a machinefmt value of l to explicitly read it in little-endian format too.
        /// </summary>
        /// <param name="real"></param>
        /// <param name="imaginary"></param>
        internal static void WriteFloatsToFile(float real, float imaginary)
        {
            try
            {
                writer.Write(real);
                writer.Write(imaginary);
            }
            catch (Exception ex)
            {
                Debug.WriteLine($"{ex.Message}");
            }
        }
    }
}
