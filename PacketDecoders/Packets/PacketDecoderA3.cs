﻿/*
 * microp11, holger 2017
 * 
 * This file is part of Scytale-C.
 * 
 * Scytale-C is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Scytale-C is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Scytale-C.  If not, see <http://www.gnu.org/licenses/>.
 *
 *   Bibliography:
 * 
 *   Proesch, R., & Daskalaki-Proesch, A. (2013). Technical Handbook for Radio Monitoring VHF/UHF. Norderstedt, Germany: Books on Demand GmbH.
 *   https://www.amazon.ca/Technical-Handbook-Radio-Monitoring-VHF/dp/3732241475/
 *   
 *   Nera. (2015) Nera Inmarsat-C Service Manual. Billingstad: Nera ASA.
 *   https://www.manualslib.com/manual/1201514/Nera-Inmarsat-C.html
 * 
 *   Thrane & Thrane. (2006) TT-3026 easyTrack Transceiver Software Interface Reference Manual. Thrane & Thrane A/S
 *   http://docslide.us/documents/tt3026-software-reference-manual.html
 *   
 * 
 * A3 - Individual Poll
 * 
 * Actually I received 2 different types of this packet.
 * 
 * Variable size
 * A3 0F 053761 55 010986000421151399 A442
 * A3 0F 11481A 4C 01026640842915096A A908
 * A3 0F 0DFC9B 55 01093E4084C615E4CA EDD3
 * A3 0E 02BBC2 43 0056BF40059D4040 A76F
 * A3 10 A4D647 44 01214040002640400D0A 1ECB
 * 
 * Variable size containing IA5 encoded text
 * A3 26 61FEDE 55 0008A1008ADF81 D04F4C45D354C15220D0D552D04C454649CEC4455220202020 3D3B
 *    |  |      |  
 *    |  |      LES_Id
 *    |  MES_Id
 *    Length of packet
 * 
 * By compairing more than 6000 poll packets it turns out that the legth of packets containing IA5 encoded text is at least 38 bytes.
 * 
 * */

using ScytaleC.Interfaces;
using System;
using System.Diagnostics;
using System.Text;

namespace ScytaleC.PacketDecoders
{
    public class PacketDecoderA3 : PacketDecoder
    {
        public int MESId { get; set; }
        public string MesIdHex { get; set; }
        public int Sat { get; set; }
        public string SatName { get; set; }
        public int LesId { get; set; }
        public string LesName { get; set; }
        public string Unknown1Hex { get; set; }
        public string ShortMessage { get; set; }

        public override void Decode(DescrambledFrameArgs args, ref int pos)
        {
            Debug.WriteLine("A3");
            try
            {
                base.Decode(args, ref pos);
                if (!IsCRC)
                {
                    return;
                }

                DecodingStage_ = DecodingStage.Partial;

                MESId = PacketDecoderUtils.ReturnMesId(args.DescrambledFrame, pos + 2);
                MesIdHex = Utils.BytesToHexString(args.DescrambledFrame, pos + 2, 3);

                Sat = PacketDecoderUtils.ReturnSat(args.DescrambledFrame[pos + 5]);
                SatName = PacketDecoderUtils.ReturnSatName(Sat);

                LesId = PacketDecoderUtils.ReturnLesId(args.DescrambledFrame[pos + 5]);
                LesName = PacketDecoderUtils.ReturnLesName(Sat, LesId);

                if (PacketLength >= 38)
                {
                    byte[] textPayload = new byte[PacketLength - 15];
                    int j = pos + 13;
                    int payloadLength = 0;

                    for (int i = 0; j < pos + PacketLength - 2; i++)
                    {
                        textPayload[i] = (byte)(args.DescrambledFrame[j] & 0x7F);
                        j++;
                        payloadLength++;
                    }

                    //this should go agnostic binary and let the message decoder take care of the encoding
                    Encoding enc = Encoding.GetEncoding("x-IA5",
                                          new EncoderExceptionFallback(),
                                          new DecoderExceptionFallback());
                    ShortMessage = enc.GetString(textPayload, 0, textPayload.Length);
                    Unknown1Hex = Utils.BytesToHexString(args.DescrambledFrame, pos + 6, 6);

                }
                else
                {
                    ShortMessage = "";
                    Unknown1Hex = Utils.BytesToHexString(args.DescrambledFrame, pos + 6, PacketLength - 6);
                }

            }
            catch (Exception ex)
            {
                CrashReport = string.Format("{0} [{1}]", ex.Message, ex.StackTrace);
            }
        }

    }
}