﻿/*
 * microp11 2017
 * 
 * This file is part of Scytale-C.
 * 
 * Scytale-C is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Scytale-C is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Scytale-C.  If not, see <http://www.gnu.org/licenses/>.
 *
 *   Bibliography:
 * 
 *   Proesch, R., & Daskalaki-Proesch, A. (2013). Technical Handbook for Radio Monitoring VHF/UHF. Norderstedt, Germany: Books on Demand GmbH.
 *   https://www.amazon.ca/Technical-Handbook-Radio-Monitoring-VHF/dp/3732241475/
 *   
 *   Nera. (2015) Nera Inmarsat-C Service Manual. Billingstad: Nera ASA.
 *   https://www.manualslib.com/manual/1201514/Nera-Inmarsat-C.html
 * 
 *   Thrane & Thrane. (2006) TT-3026 easyTrack Transceiver Software Interface Reference Manual. Thrane & Thrane A/S
 *   http://docslide.us/documents/tt3026-software-reference-manual.html
 *   
 *   
 */

using ScytaleC.Interfaces;
using System;
using System.Diagnostics;

namespace ScytaleC.PacketDecoders

{
    /// <summary>
    /// 6C Channel Info
    /// </summary>
    public class PacketDecoder6C : PacketDecoder
    {
        public PacketDecoderUtils.Services8 Services { get; set; }
        public string ServicesHex { get; set; }
        public double UplinkChannelMHz { get; set; }
        public TDMSlots TdmSlots { get; set; }
        public string TdmSlotsHEX { get; set; }

        public override void Decode(DescrambledFrameArgs args, ref int pos)
        {
            Debug.WriteLine("6C");
            try
            {
                base.Decode(args, ref pos);

                if (!IsCRC)
                {
                    return;
                }

                DecodingStage_ = DecodingStage.Complete;

                //The second byte in 6C packets is the services byte.
                //Identical to the first byte of the services bytes of the 7D packet.
                int is8 = args.DescrambledFrame[pos + 1];
                Services = PacketDecoderUtils.ReturnServices8(is8);
                ServicesHex = Utils.BytesToHexString(args.DescrambledFrame, pos + 1, 1);

                // (2 Byte value - 6000) * 0.0025 + 1626.5 MHz
                UplinkChannelMHz = PacketDecoderUtils.ReturnUplinkChannelMHz(args.DescrambledFrame, pos + 2);

                // 28 slots of 2bits each
                TdmSlots = new TDMSlots(args.DescrambledFrame, pos + 4);
                TdmSlotsHEX = Utils.BytesToHexString(args.DescrambledFrame, pos + 4, 7);
            }
            catch (Exception ex)
            {
                CrashReport = string.Format("{0} [{1}]", ex.Message, ex.StackTrace);
            }
        }
    }

    public class TDMSlots
    {
        public bool SlotZeroMSBByte4 { get; set; }
        public int[] Slots { get; set; }

        //default constructor needed for dynamic casting
        public TDMSlots() { }

        public TDMSlots(byte[] slots, int pos)
        {
            SlotZeroMSBByte4 = true;
            Slots = new int[28];

            int j = 0;
            for (int i = 0; i < 28; i += 4)
            {
                Slots[i] = slots[pos + j] >> 6;
                Slots[i + 1] = slots[pos + j] >> 4 & 3;
                Slots[i + 2] = slots[pos + j] >> 2 & 3;
                Slots[i + 3] = slots[pos + j] & 3;
                j++;
            }
        }
    }
}