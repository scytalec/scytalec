﻿/*
 * microp11 2017
 * 
 * This file is part of Scytale-C.
 * 
 * Scytale-C is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Scytale-C is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Scytale-C.  If not, see <http://www.gnu.org/licenses/>.
 * 
 *   Bibliography:
 * 
 *   Proesch, R., & Daskalaki-Proesch, A. (2013). Technical Handbook for Radio Monitoring VHF/UHF. Norderstedt, Germany: Books on Demand GmbH.
 *   https://www.amazon.ca/Technical-Handbook-Radio-Monitoring-VHF/dp/3732241475/
 *   
 *   Nera. (2015) Nera Inmarsat-C Service Manual. Billingstad: Nera ASA.
 *   https://www.manualslib.com/manual/1201514/Nera-Inmarsat-C.html
 * 
 *   Thrane & Thrane. (2006) TT-3026 easyTrack Transceiver Software Interface Reference Manual. Thrane & Thrane A/S
 *   http://docslide.us/documents/tt3026-software-reference-manual.html
 *
 * 	
 * AA - frame type
 * 85 - frame length in bytes including length byte
 * 15 - this looks like the Station ID, same as position 7 in the 7D corresonding frame, however...
 * 60 - Message ID? other encoded info?
 * 01 - this seems to be the sequence in the message.
 * 
 * Will decode as such for the time being, I am sure will find out soon the meaning. Just analyze enough packets.
 * 
 * BD is the start of multiframe message
 * BE is the last part of the multiframe message.
 * 	
 * AA85156001D649DAC1C4C10D8AC279F4E573BA2020B6B9B60D8A46F2EF6DBA205261EA75204AEF73E5706820BCF261EA754070F2E5E3E9EF75737368E97070E96E67AEE3EF6D3E0D8AD37562EAE5E3F4BA20CD4CCB0D8AC461F4E5BA205468752C20B920CD61F22032B0313720B0B5BAB3B3BA31B320ABB0B0B0B00D8A0D8A524546BA20525B
 * AA85156002C14AD52031B337320D8A0D8ACDC1D35445520D8ACDC14C4C49CBC120CEC15245450D8A0D8ACE4F5445C420D94FD520C8D6205245434549D645C42043C8525452D320CE20C1C745CE542049CED35458CE20574520C8D620464F5257C152C445C420544F20D94FD520D649C120D3C1542043AE0D8A524551D545D3542043C852548E
 * AA8515600352D320CE20C1C745CE5420544F20434FD0D92054C845495220CDD3C720544F2070EF73F4E6E9F84070F2E5E3E9EF75737368E97070E96E67AEE3EF6D0D8A45CEC1C24C4520D5D320544F20464F5257C152C420544F20D94FD5AE0D8A0D8A574520C1524520D345CEC449CEC72054C84520434FD04945D3204F46204345525420BE
 * AA85156004524551D545D35445C420544F2054C84520C1C745CE54AE0D8A0D8A434FCE464952CD2049462054C84520C44946462049CE2043C152C74F2051D5C1CE544954D92049D320D34F525445C4204FD554AE20D04CD320464C572043C8525452D32049CED35458CE0D8A57C8494C45205445CEC4455249CEC720CE4F5220C1CEC420C429
 * BD39AA8515600545C1C446524549C7C85420434CC149CDAE0D8A0D8A57494C4C20D345CEC4204FD55220D64FD92049CED35458CE2057C845CE51
 * BE502045ADCDC1494C2049D3205245D3544F5245C4AE2054C84520D3D04545C42C2045434F20D3D04545C420D3C1CD450D8AC1D320C245464F524520C1CEC420C1D320D045522043C8525452D32049CED3
 * AA451561016162636465666768696A6B6C6D6E6F707172737475767778797A313233343536373839304142434445464748494A4B4C4D4E4F505152535455565758595A2D2100 
 * 
 * 
 * 
 * Decoded, the message above looks like:
 *  
 * MASTER
 * <name>
 * NOTED YOU HV RECEIVED CHRTRS N AGENT INSTXN WE HV FORWARDED TO YOU VIA SAT C.
 * REQUEST CHRT
 *
 * RS N AGENT TO COPY THEIR MSG TO <email>
 * ENABLE US TO FORWARD TO YOU.
 *
 * WE ARE SENDING THE COPIES OF CERT 
 *
 * REQUESTED TO THE AGENT.
 *
 * CONFIRM IF THE DIFF IN CARGO QUANTITY IS SORTED OUT. PLS FLW CHRTRS INSTXN
 * WHILE TENDERING NOR AND D
 *
 * ADFREIGHT CLAIM.
 *
 * WILL SEND OUR VOY INSTXN WHEN
 *
 * MAIL IS RESTORED. THE SPEED, ECO SPEED SAME
 * AS BEFORE AND AS PER CHRTRS IN
 *
 * STXN. CHRTRS ARE ALLOWED TO USE VESSEL GRABS.
 * BRGDS
 *  
 *  
 *  
 */

using ScytaleC.Interfaces;
using System;
using System.Diagnostics;

namespace ScytaleC.PacketDecoders
{
    public class PacketDecoderAA : PacketDecoder
    {
        public int Priority { get; set; }
        public string PriorityText { get; set; }
        public bool IsDistress { get; set; }
        public int Repetition { get; set; }
        public int Sat { get; set; }
        public string SatName { get; set; }
        public int LesId { get; set; }
        public string LESName { get; set; }
        public int LogicalChannelNo { get; set; }
        public int PacketNo { get; set; }

        public override void Decode(DescrambledFrameArgs args, ref int pos)
        {
            Debug.WriteLine("AA");

            //AA packets do not encode a Priority.
            //For the sake of consistency, we attach a Priority of "Message" = -1
            Priority = -1;
            PriorityText = PacketDecoderUtils.ReturnPriority(Priority);
            IsDistress = false;
            //This packets do not encode a Repetition. We attach a Repetition of zero
            Repetition = 0;

            try
            {
                base.Decode(args, ref pos);
                if (!IsCRC)
                {
                    return;
                }

                DecodingStage_ = DecodingStage.Complete;

                Sat = PacketDecoderUtils.ReturnSat(args.DescrambledFrame[pos + 2]);
                SatName = PacketDecoderUtils.ReturnSatName(Sat);
                LesId = PacketDecoderUtils.ReturnLesId(args.DescrambledFrame[pos + 2]);
                LESName = PacketDecoderUtils.ReturnLesName(Sat, LesId);

                LogicalChannelNo = args.DescrambledFrame[pos + 3];

                PacketNo = args.DescrambledFrame[pos + 4];
                byte[] payload = new byte[PacketLength - 2];

                //in bytes, presentation agnostic
                int j = pos + 5;
                int actualLength = 0;
                for (int i = 0; j < pos + PacketLength - 2; i++)
                {
                    payload[i] = args.DescrambledFrame[j];
                    j++;
                    actualLength++;
                }
                Payload_.Data8Bit = new byte[actualLength];
                Array.Copy(payload, Payload_.Data8Bit, actualLength);

                //These packets have no presentation.
                //We are running our own cheap detector for the sake of identifying the type of paylod.
                //This is hit and miss.
                Payload_.Presentation = PacketDecoderUtils.IsBinary(Payload_.Data8Bit, true) ? 7 : 0;
            }
            catch (Exception ex)
            {
                CrashReport = string.Format("{0} [{1}]", ex.Message, ex.StackTrace);
            }
        }
    }
}