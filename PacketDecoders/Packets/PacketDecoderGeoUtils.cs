﻿/*
 * microp11 2017
 * 
 * This file is part of Scytale-C.
 * 
 * Scytale-C is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Scytale-C is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Scytale-C.  If not, see <http://www.gnu.org/licenses/>.
 * 
 *   Bibliography:
 *   
 *   https://www.navcen.uscg.gov/pdf/gmdss/Safety_NET_Manual.pdf
 * 
 *    
 */


using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Xml.Serialization;

namespace ScytaleC.PacketDecoders
{
    public static class PacketDecoderGeoUtils
    {
        public static GeoArea ReturnArea(int messageType, string addressHex)
        {
            if (addressHex == null)
            {
                return null;
            }

            if (addressHex.Length == 0)
            {
                return null;
            }

            byte[] data = null;
            data = Utils.HexStringToBytes(addressHex);
            switch (messageType)
            {
                case 0x00:
                case 0x31:
                    return ReturnNavArea(data);

                case 0x04:
                case 0x34:
                    return ReturnRectangularArea(data);

                case 0x13:
                    return ReturnCoastalArea(data);

                case 0x14:
                case 0x24:
                case 0x44:
                    return ReturnCircularArea(data);

                default:
                    return null;
            }
        }

        /// <summary>
        /// Calculates EGC NavArea. It returns all points and the center.
        /// </summary>
        public static NavArea ReturnNavArea(byte[] area)
        {
            //  0 1 2 3 4 5 6 7 LSB
            //  |             | 
            //  --------------- 8 bits, NavArea
            //  MSB

            NavArea na = new NavArea();

            if (area.Length < 2)
            {
                return null;
            }

            na.Area = area[0];

            // the Xml part of the navarea is not added here.
            // it can be, but for now it is not.
            // the Quick UI will attach it as needed.

            na.Text = string.Format("NavArea {0}, {1}", ArabicToRoman(na.Area), PacketDecoderUtils.ReturnNavMetAreaCoordinator(na.Area));

            return na;
        }

        /// <summary>
        /// Calculates EGC Rectangular Area. It returns all corners and the center.
        /// </summary>
        public static RectangularArea ReturnRectangularArea(byte[] area)
        {
            //  0 1 2 3 4 5 6 7  0 1 2 3 4 5 6 7  0 1 2 3 4 5 6 7  0 1 2 3 4 5 6 7 LSB
            //  | |           |  |             |  | |           | |              |
            //  | |           |  |             |  | |           | ---------------- 8 bits, extension of rectangle in NM to East
            //  | |           |  |             |  | |           |
            //  | |           |  |             |  | ------------- 7 bits, extension of rectangle in NM to North
            //  | |           |  |             |  |
            //  | |           |  |             |  1 bit, East 0, West 1
            //  | |           |  |             | 
            //  | |           |  --------------- 8 bits, longitude in degrees of the SW corner
            //  | |           |
            //  | ------------- 7 bits, latitude in degrees of the SW corner
            //  |
            //  1 bit, North = 0, South 1
            // 
            //  MSB

            RectangularArea ra = new RectangularArea();

            if (area.Length < 4)
            {
                return null;
            }

            //North 0, South 1 (negative)
            ra.NSHemisphereLat = area[0] >> 7;
            ra.SWCornerLat = area[0] & 0x7F;
            ra.SWCornerLng = area[1];

            //East 0, West 1 (negative)
            ra.EWHemisphereLng = area[2] >> 7;
            ra.ExtensionNorthNM = area[2] & 0x7F;
            ra.ExtensionEastNM = area[3];

            if (ra.NSHemisphereLat == 1)
            {
                ra.SWCornerLat = -ra.SWCornerLat;
            }

            if (ra.EWHemisphereLng == 1)
            {
                ra.SWCornerLng = -ra.SWCornerLng;
            }

            ra.SWCorner = new GeoPoint(ra.SWCornerLat, ra.SWCornerLng);

            //go East from the SW corner
            ra.SECorner = FindPointAtDistanceFrom(ra.SWCorner, Math.PI / 2, ra.ExtensionEastNM * 1.852);

            //go North from the SE corner
            ra.NECorner = FindPointAtDistanceFrom(ra.SECorner, 0, ra.ExtensionNorthNM * 1.852);

            //go North from the SW corner
            ra.NWCorner = FindPointAtDistanceFrom(ra.SWCorner, 0, ra.ExtensionNorthNM * 1.852);

            //get the center of the rectangle
            GeoPoint halfSE = FindPointAtDistanceFrom(ra.SWCorner, Math.PI / 2, (ra.ExtensionEastNM / 2) * 1.852);
            ra.Center = FindPointAtDistanceFrom(halfSE, 0, (ra.ExtensionNorthNM / 2) * 1.852);

            ra.Text = string.Format("SW corner {0},{1}, extending {2} nm East and {3} nm North",
                ra.SWCornerLat, ra.SWCornerLng, ra.ExtensionEastNM, ra.ExtensionNorthNM);

            return ra;
        }

        /// <summary>
        /// Calculates EGC CoastalArea. It returns all points and the center.
        /// </summary>
        public static CoastalArea ReturnCoastalArea(byte[] area)
        {
            //  0 1 2 3 4 5 6 7  0 1 2 3 4 5 6 7 LSB
            //  MSB

            CoastalArea coa = new CoastalArea();

            if (area.Length < 2)
            {
                return null;
            }

            coa.Area = area[0];

            coa.Text = string.Format("Coastal Area {0}, {1}", ArabicToRoman(coa.Area), PacketDecoderUtils.ReturnNavMetAreaCoordinator(coa.Area));

            return coa;
        }

        /// <summary>
        /// Calculates EGC Circular Area. It returns all points and the center.
        /// </summary>
        public static CircularArea ReturnCircularArea(byte[] area)
        {
            //  0 1 2 3 4 5 6 7  0 1 2 3 4 5 6 7  0 1 2 3 4 5 6 7  0 1 2 3 4 5 6 7 LSB
            //  | |           |  |             |  | |                            |
            //  | |           |  |             |  | ------------------------------- 15 bits, radius in NM
            //  | |           |  |             |  |
            //  | |           |  |             |  1 bit, East 0, West 1
            //  | |           |  |             | 
            //  | |           |  --------------- 8 bits, longitude in degrees of the center
            //  | |           |
            //  | ------------- 7 bits, latitude in degrees of the center
            //  |
            //  1 bit, North = 0, South 1
            // 
            //  MSB

            CircularArea ca = new CircularArea();

            if (area.Length < 4)
            {
                return null;
            }

            //North 0, South 1 (negative)
            ca.NSHemisphereLat = area[0] >> 7;
            ca.CenterLat = area[0] & 0x7F;
            ca.CenterLng = area[1];

            //East 0, West 1 (negative)
            ca.EWHemisphereLng = area[2] >> 7;
            ca.RadiusNM = (area[2] & 0x7F) << 8 | area[3];

            if (ca.NSHemisphereLat == 1)
            {
                ca.CenterLat = -ca.CenterLat;
            }

            if (ca.EWHemisphereLng == 1)
            {
                ca.CenterLng = -ca.CenterLng;
            }

            ca.Center = new GeoPoint(ca.CenterLat, ca.CenterLng);

            ca.Text = string.Format("Center {0},{1}, radius {2} nm", ca.CenterLat, ca.CenterLng, ca.RadiusNM);

            return ca;
        }

        public static GeoPoint FindPointAtDistanceFrom(GeoPoint startPoint, double initialBearingRadians, double distanceKilometres)
        {
            const double radiusEarthKilometres = 6371.01;
            var distRatio = distanceKilometres / radiusEarthKilometres;
            var distRatioSine = Math.Sin(distRatio);
            var distRatioCosine = Math.Cos(distRatio);

            var startLatRad = DegreesToRadians(startPoint.Lat);
            var startLonRad = DegreesToRadians(startPoint.Lng);

            var startLatCos = Math.Cos(startLatRad);
            var startLatSin = Math.Sin(startLatRad);

            var endLatRads = Math.Asin((startLatSin * distRatioCosine) + (startLatCos * distRatioSine * Math.Cos(initialBearingRadians)));

            var endLonRads = startLonRad + Math.Atan2(
                          Math.Sin(initialBearingRadians) * distRatioSine * startLatCos,
                          distRatioCosine - startLatSin * Math.Sin(endLatRads));

            return new GeoPoint(RadiansToDegrees(endLatRads), RadiansToDegrees(endLonRads));
        }

        private static double DegreesToRadians(double degrees)
        {
            const double degToRadFactor = Math.PI / 180;
            return degrees * degToRadFactor;
        }

        private static double RadiansToDegrees(double radians)
        {
            const double radToDegFactor = 180 / Math.PI;
            return radians * radToDegFactor;
        }

        /// <summary>
        /// https://pastebin.com/w0hm9n5W
        /// </summary>
        /// <returns></returns>
        private static string ArabicToRoman(int number)
        {
            var romanNumerals = new string[][]
            {
            new string[]{"", "I", "II", "III", "IV", "V", "VI", "VII", "VIII", "IX"}, // ones
            new string[]{"", "X", "XX", "XXX", "XL", "L", "LX", "LXX", "LXXX", "XC"}, // tens
            new string[]{"", "C", "CC", "CCC", "CD", "D", "DC", "DCC", "DCCC", "CM"}, // hundreds
            new string[]{"", "M", "MM", "MMM"} // thousands
            };

            // split integer string into array and reverse array
            var intArr = number.ToString().Reverse().ToArray();
            var len = intArr.Length;
            var romanNumeral = "";
            var i = len;

            // starting with the highest place (for 3046, it would be the thousands 
            // place, or 3), get the roman numeral representation for that place 
            // and add it to the final roman numeral string
            while (i-- > 0)
            {
                romanNumeral += romanNumerals[i][Int32.Parse(intArr[i].ToString())];
            }

            return romanNumeral;
        }

    }

    public class GeoArea
    {
        public string Text { get; set; }
    }

    public class RectangularArea : GeoArea
    {
        public int NSHemisphereLat { get; set; }
        public int EWHemisphereLng { get; set; }
        public GeoPoint SWCorner { get; set; }
        public GeoPoint NWCorner { get; set; }
        public GeoPoint NECorner { get; set; }
        public GeoPoint SECorner { get; set; }
        public int SWCornerLat { get; set; }
        public int SWCornerLng { get; set; }
        public int ExtensionNorthNM { get; set; }
        public int ExtensionEastNM { get; set; }
        public GeoPoint Center { get; set; }
    }

    public class GeoPoint
    {
        public double Lat { get; set; }
        public double Lng { get; set; }

        // because of serialization every class that gets serialized/deserialized
        // must have a default constructor
        public GeoPoint() { }

        public GeoPoint(double lat, double lng)
        {
            Lat = lat;
            Lng = lng;
        }
    }

    public class CircularArea : GeoArea
    {
        public int NSHemisphereLat { get; set; }
        public int EWHemisphereLng { get; set; }
        public int CenterLat { get; set; }
        public int CenterLng { get; set; }
        public GeoPoint Center { get; set; }
        public int RadiusNM { get; set; }
    }

    public class NavArea : GeoArea
    {
        public byte Area { get; set; }
        public XmlNavArea XmlArea { get; set; }
    }

    public class CoastalArea : GeoArea
    {
        public byte Area { get; set; }
        public XmlNavArea XmlArea { get; set; }
    }

    public class XmlNavArea
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string BackgroundColor { get; set; }
        public string PerimeterColor { get; set; }

        [XmlIgnore]
        public Color BkgColor { get; set; }

        [XmlIgnore]
        public Color PrmColor { get; set; }

        public List<GeoPoint> Coordinates { get; set; }
    }
}
