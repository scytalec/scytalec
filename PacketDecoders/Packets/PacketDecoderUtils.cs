﻿/*
 * microp11 2017
 * 
 * This file is part of Scytale-C.
 * 
 * Scytale-C is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Scytale-C is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Scytale-C.  If not, see <http://www.gnu.org/licenses/>.
 * 
 *   Bibliography:
 * 
 *   Proesch, R., & Daskalaki-Proesch, A. (2013). Technical Handbook for Radio Monitoring VHF/UHF. Norderstedt, Germany: Books on Demand GmbH.
 *   https://www.amazon.ca/Technical-Handbook-Radio-Monitoring-VHF/dp/3732241475/
 *   
 *   Nera. (2015) Nera Inmarsat-C Service Manual. Billingstad: Nera ASA.
 *   https://www.manualslib.com/manual/1201514/Nera-Inmarsat-C.html
 * 
 *   Thrane & Thrane. (2006) TT-3026 easyTrack Transceiver Software Interface Reference Manual. Thrane & Thrane A/S
 *   http://docslide.us/documents/tt3026-software-reference-manual.html
 *   
 *   Inmarsat-C. EGC Services: SafetyNET & FleetNET Users manual (2002). Xantic B.V.
 *   http://www.xantic.net/internet/files/products/inmarsat/inmarsat_c/support_docs/InmC-EGC-SafetyNETandFleetNET%20mnl.pdf
 *   
 *   http://wikimapia.org/11712104/Inmarsat-Land-Earth-Station-Burum
 *   https://www.egmdss.com/gmdss-courses/mod/resource/view.php?inpopup=true&id=2371
 *   https://www.egmdss.com/gmdss-courses/mod/resource/view.php?id=2312
 *   http://www.marcomm.ru/UserFiles/Files/Doc/LES_codes_C.pdf
 *   https://www.inmarsat.com/wp-content/uploads/2013/10/Inmarsat_Fleet77_LES_codes.pdf
 *   http://weather.gmdss.org/navareas.html
 *   https://rmanimaran.wordpress.com/2011/01/27/convert-datetime-to-ticks-and-ticks-to-datetime-in-net-c/
 *    
 */

using ScytaleC.Interfaces;
using System;

namespace ScytaleC.PacketDecoders
{
    public class PacketDecoderUtils
    {
        public const int Distress = 3;

        /// <summary>
        /// Returns an array equal in length to the input array, while converting the content
        /// to 7-bit.
        /// </summary>
        /// <param name="data8Bit"></param>
        /// <returns></returns>
        public static byte[] Return7BitData(byte[] data8Bit)
        {
            if (data8Bit == null)
            {
                return new byte[0];
            }

            int length = data8Bit.Length;
            byte[] data7Bit = new byte[length];
            for (int i = 0; i < length; i++)
            {
                data7Bit[i] = (byte)(data8Bit[i] & 0x7F);
            }
            return data7Bit;
        }

        /// <summary>
        /// 2-byte CRC for Inmarsat-C calculator.
        /// Arithmetic is modulo 256.
        /// Perform encoding on data + zeros in checksum field.
        /// Perform checker on all data bytes including checksum.
        /// CB2 forms last byte of the packet.
        /// 
        /// How to implement this as a lookup?
        /// </summary>
        /// <param name="data"></param>
        /// <param name="pos"></param>
        /// <param name="length"></param>
        /// <returns></returns>
        public static int ComputeCRC(byte[] data, int pos, int length)
        {
            short C0 = 0;
            short C1 = 0;
            byte CB1;
            byte CB2;
            byte B;

            int i = 0;
            while (i < length)
            {
                if (i < length - 2)
                {
                    B = data[pos + i];
                }
                else
                {
                    B = 0;
                }
                C0 += B;
                C1 += C0;

                i++;
            }

            CB1 = (byte)(C0 - C1);
            CB2 = (byte)(C1 - 2 * C0);
            return (CB1 << 8) | CB2;
        }

        /// <summary>
        /// (2-byte value - 8000) * 0.0025 + 1530 HHz
        /// </summary>
        /// <param name="descrambledFrame"></param>
        /// <param name="v"></param>
        /// <returns></returns>
        internal static double ReturnDownlinkChannelMHz(byte[] data, int pos)
        {
            return ((data[pos] << 8 | data[pos + 1]) - 8000) * 0.0025 + 1530;
        }

        /// <summary>
        /// (2 Byte value - 6000) * 0.0025 + 1626.5 MHz
        /// This code may crash. The exception is caught by the caller.
        /// </summary>
        /// <param name="data"></param>
        /// <param name="pos"></param>
        /// <returns></returns>
        public static double ReturnUplinkChannelMHz(byte[] data, int pos)
        {
            return ((data[pos] << 8 | data[pos + 1]) - 6000) * 0.0025 + 1626.5;
        }

        /// <summary>
        /// TODO
        /// </summary>
        /// <param name="iss"></param>
        /// <returns></returns>
        public static Services ReturnServices(int iss)
        {
            Services ss = new Services();

            ss.MaritimeDistressAlerting = (iss & 0x8000) >> 15 == 1;
            ss.SafetyNet = (iss & 0x4000) >> 14 == 1;
            ss.InmarsatC = (iss & 0x2000) >> 13 == 1;
            ss.StoreFwd = (iss & 0x1000) >> 12 == 1;
            ss.HalfDuplex = (iss & 0x800) >> 11 == 1;
            ss.FullDuplex = (iss & 0x400) >> 10 == 1;
            ss.ClosedNetwork = (iss & 0x200) >> 9 == 1;
            ss.FleetNet = (iss & 0x100) >> 8 == 1;
            ss.PrefixSF = (iss & 0x80) >> 7 == 1;
            ss.LandMobileAlerting = (iss & 0x40) >> 6 == 1;
            ss.AeroC = (iss & 0x20) >> 5 == 1;
            ss.ITA2 = (iss & 0x10) >> 4 == 1;
            ss.DATA = (iss & 0x08) >> 3 == 1;
            ss.BasicX400 = (iss & 0x04) >> 2 == 1;
            ss.EnhancedX400 = (iss & 0x02) >> 1 == 1;
            ss.LowPowerCMES = (iss & 0x01) == 1;

            return ss;
        }

        /// <summary>
        /// or LSB nibble
        /// </summary>
        /// <param name="messageType"></param>
        /// <returns></returns>
        internal static int ReturnAddressLenght(int messageType)
        {
            switch (messageType)
            {
                case 0x00:
                    return 3;

                case 0x11:
                case 0x31:
                    return 4;

                case 0x02:
                case 0x72:
                    return 5;

                case 0x13:
                case 0x23:
                case 0x33:
                case 0x73:
                    return 6;

                case 0x04:
                case 0x14:
                case 0x24:
                case 0x34:
                case 0x44:
                    return 7;

                default:
                    return 3;
            }
        }

        // 1 August 2008 Update
        // There are more updates in the ref.
        // These should go in the config so they won't need code update! TODO
        //

        /// <summary>
        /// The Sat is one digit (0..9), the LesId is 2 digits (0..63)
        /// The full name is obtain by concatenating the Sat to the zero padded LesId and performing a lookup
        /// in the table below.
        /// </summary>
        /// <param name="sat"></param>
        /// <param name="lesId"></param>
        /// <returns></returns>
        public static string ReturnLesName(int sat, int lesId)
        {
            string value = string.Format("{0}{1}", sat, lesId.ToString("D2"));
            string name;

            switch (value)
            {
                case "001":
                case "101":
                case "201":
                case "301":
                    name = "Vizada-Telenor, USA";
                    break;

                case "002":
                case "102":
                case "302":
                    name = "Stratos Global (Burum-2), Netherlands";
                    break;

                case "202":
                    name = "Stratos Global (Aukland), New Zealand";
                    break;

                case "003":
                case "103":
                case "203":
                case "303":
                    name = "KDDI Japan";
                    break;

                case "004":
                case "104":
                case "204":
                case "304":
                    name = "Vizada-Telenor, Norway";
                    break;

                case "044":
                case "144":
                case "244":
                case "344":
                    name = "NCS";
                    break;

                case "105":
                case "335":
                    name = "Telecom, Italia";
                    break;

                case "305":
                case "120":
                    name = "OTESTAT, Greece";
                    break;

                case "306":
                    name = "VSNL, India";
                    break;

                case "110":
                case "310":
                    name = "Turk Telecom, Turkey";
                    break;

                case "211":
                case "311":
                    name = "Beijing MCN, China";
                    break;

                case "012":
                case "112":
                case "212":
                case "312":
                    name = "Stratos Global (Burum), Netherlands";
                    break;

                case "114":
                    name = "Embratel, Brazil";
                    break;

                case "116":
                case "316":
                    name = "Telekomunikacja Polska, Poland";
                    break;

                case "117":
                case "217":
                case "317":
                    name = "Morsviazsputnik, Russia";
                    break;

                case "021":
                case "121":
                case "221":
                case "321":
                    name = "Vizada (FT), France";
                    break;

                case "127":
                case "327":
                    name = "Bezeq, Israel";
                    break;

                case "210":
                case "328":
                    name = "Singapore Telecom, Singapore";
                    break;

                case "330":
                    name = "VISHIPEL, Vietnam";
                    break;

                default:
                    name = "Unknown";
                    break;
            }

            return string.Format("{0}, {1}", value, name);
        }

        public static bool IsBinary(byte[] data, bool checkAll = false)
        {
            bool isBinary = false;

            //try first 13 characters if not check all
            int check = 13;
            if (!checkAll)
            {
                check = Math.Min(check, data.Length-2);
            }
            else
            {
                check = data.Length;
            }

            for (int i = 0; i < check; i++)
            {
                if (!isBinary)
                {
                    byte[] chr = { (byte)(data[i] & 0x7F) };
                    string schr = System.Text.Encoding.UTF8.GetString(chr, 0, 1);
                    switch (schr)
                    {
                        case "\u0001":

                        case "\u0003":
                        
                        case "\u0005":
                        case "\u0006":
                        case "\u0007":
                        case "\u0008":

                        case "\u000B":
                        case "\u000C":

                        case "\u000E":
                        case "\u000F":
                        case "\u0010":
                        case "\u0011":
                        case "\u0012":
                        case "\u0013":
                        case "\u0014":
                        case "\u0015":
                        case "\u0016":
                        case "\u0017":
                        case "\u0018":
                        case "\u0019":
                        case "\u001A":

                        case "\u001C":
                        case "\u001D":
                        case "\u001E":
                        case "\u001F":

                        case "\u00A4":
                        case "$":
                            isBinary = true;
                            break;
                    }

                    if (isBinary)
                    { 
                        break;
                    }
                }
            }
            return isBinary;
        }

        internal static string ReturnSatName(int sat)
        {
            switch (sat)
            {
                case 0:
                    return "Atlantic Ocean Region West (AOR-W)";

                case 1:
                    return "Atlantic Ocean Region East (AOR-E)";

                case 2:
                    return "Pacific Ocean Region (POR)";

                case 3:
                    return "Indian Ocean Region (IOR)";

                case 9:
                    return "All Ocean Regions Covered by the LES";

                default:
                    return "Unknown";
            }
        }

        /// <summary>
        /// 3F = 63 which means we can have a max of 64 LES' on a sattelite.
        /// </summary>
        internal static int ReturnLesId(int number)
        {
            return number & 0x3F;
        }

        /// <summary>
        /// Uses 3 bytes of data from the data array passed as argument, starting at pos
        /// </summary>
        /// <param name="data"></param>
        /// <param name="pos"></param>
        /// <returns></returns>
        internal static int ReturnMesId(byte[] data, int pos)
        {
            return data[pos] << 16 | data[pos + 1] << 8 | data[pos + 2];
        }

        internal static int ReturnSat(int number)
        {
            return number >> 6 & 0x03;
        }

        /// <summary>
        /// Not 100% sure, conflicting docs.
        /// </summary>
        /// <param name="channelType"></param>
        /// <returns></returns>
        public static string ReturnChannelTypeName(int channelType)
        {
            switch (channelType)
            {
                case 1:
                    return "NCS";

                case 2:
                    return "LES TDM";

                case 3:
                    return "Joint NCS and TDM";

                case 4:
                    return "ST-BY NCS";

                default:
                    return "Reserved";
            }
        }

        internal static Status ReturnStatus(byte status)
        {
            Status sts = new Status();
            sts.Bauds600 = (status & 0x80) >> 7 == 1;
            sts.Operational = (status & 0x40) >> 6 == 1;
            sts.InService = (status & 0x20) >> 5 == 1;
            sts.Clear = (status & 0x10) >> 4 == 1;
            sts.LinksOpen = (status & 0x08) >> 3 == 1;

            return sts;
        }

        public static string ReturnPriority(int priority)
        {
            switch (priority)
            {
                case -1:
                    return "Message";

                case 0:
                    return "Routine";

                case 1:
                    return "Safety";

                case 2:
                    return "Urgency";

                case 3:
                    return "Distress";

                default:
                    return "Unknown";
            }
        }

        // For the International SafetyNET Service the presentation must always be 0 (zero).
        public static string ReturnPresentation(int enc)
        {
            switch (enc)
            {
                case 0:
                    return "IA 5";

                case 6:
                    return "ITA 2";

                case 7:
                    return "8-bit binary data";

                default:
                    return "Unknown";
            }
        }

        public static string ReturnServiceCodeAndAddressName(int code)
        {
            switch (code)
            {
                case 0x00:
                    return "System, All ships (general call)";

                case 0x02:
                    return "FleetNET, Group Call";

                case 0x04:
                    return "SafetyNET, Navigational, Meteorological or Piracy Warning to a Rectangular Area";

                case 0x11:
                    return "System, Inmarsat System Message";

                case 0x13:
                    return "SafetyNET, Navigational, Meteorological or Piracy Coastal Warning";

                case 0x14:
                    return "SafetyNET, Shore-to-Ship Distress Alert to Circular Area";

                case 0x23:
                    return "System, EGC System Message";

                case 0x24:
                    return "SafetyNET, Navigational, Meteorological or Piracy Warning to a Circular Area";

                case 0x31:
                    return "SafetyNET, NAVAREA/METAREA Warning, MET Forecast or Piracy Warning to NAVAREA/METAREA";

                case 0x33:
                    return "System, Download Group Identity";

                case 0x34:
                    return "SafetyNET, SAR Coordination to a Rectangular Area";

                case 0x44:
                    return "SafetyNET, SAR Coordination to a Circular Area";

                case 0x72:
                    return "FleetNET, Chart Correction Service";

                case 0x73:
                    return "SafetyNET, Chart Correction Service for Fixed Areas";

                default:
                    return "Unknown";
            }
        }

        public static string ReturnNavMetAreaCoordinator(int area)
        {
            switch (area)
            {
                case 1:
                    return "United Kingdom";

                case 2:
                    return "France";

                case 3:
                    return "Spain";

                case 4:
                    return "United States of America (East)";

                case 5:
                    return "Brazil";

                case 6:
                    return "Argentina";

                case 7:
                    return "South Africa";

                case 8:
                    return "India";

                case 9:
                    return "Pakistan";

                case 10:
                    return "Australia";

                case 11:
                    return "Japan";

                case 12:
                    return "United States of America (West)";

                case 13:
                    return "Russian Federation";

                case 14:
                    return "New Zealand";

                case 15:
                    return "Chile";

                case 16:
                    return "Peru";

                case 17:
                case 18:
                    return "Canada";

                case 19:
                    return "Norway";

                case 20:
                case 21:
                    return "Russian Federation";

                default:
                    return "Unknown";
            }
        }

        /// <summary>
        /// In case of an exception, the caller is responsible for handling it.
        /// </summary>
        /// <param name="packet"></param>
        /// <param name="pos"></param>
        /// <returns></returns>
        public static Station ReturnStation(byte[] packet, int pos)
        {
            Station stn = new Station();

            stn.Sat = ReturnSat(packet[pos]);
            stn.SatName = ReturnSatName(stn.Sat);
            stn.LesId = ReturnLesId(packet[pos]);
            stn.LesName = ReturnLesName(stn.Sat, stn.LesId);
            stn.ServicesStartHex = Utils.BytesToHexString(packet, pos + 1, 1);

            int ss = packet[pos + 2] << 8 | packet[pos + 3];
            stn.Services_ = ReturnServices(ss);
            stn.DownlinkChannelMHz = ReturnDownlinkChannelMHz(packet, pos + 4);
            return stn;
        }

        public struct Services
        {
            //ALERT, A
            public bool MaritimeDistressAlerting { get; set; }
            
            //SaNET, S
            public bool SafetyNet { get; set; }
            
            //Std-C, C
            public bool InmarsatC { get; set; }

            //St&Fw, T
            public bool StoreFwd { get; set; }

            //Half Duplex, H
            public bool HalfDuplex { get; set; }

            //Full Duplex, D
            public bool FullDuplex { get; set; }
            //Closed Network, C
            public bool ClosedNetwork { get; set; }

            //FlNET, F
            public bool FleetNet { get; set; }

            //Prefix Store&Forward
            public bool PrefixSF { get; set; }
            
            //LndAl, L
            public bool LandMobileAlerting { get; set; }

            //AeroC, A
            public bool AeroC { get; set; }

            //ITA-2, 2
            public bool ITA2 { get; set; }

            //Data, D
            public bool DATA { get; set; }

            //B
            public bool BasicX400 { get; set; }

            //X
            public bool EnhancedX400 { get; set; }

            //LowPw, L
            public bool LowPowerCMES { get; set; }
        }

        public struct Services8
        {
            //ALERT, A
            public bool MaritimeDistressAlerting { get; set; }

            //SaNET, S
            public bool SafetyNet { get; set; }

            //Std-C, C
            public bool InmarsatC { get; set; }

            //St&Fw, T
            public bool StoreFwd { get; set; }

            //Half Duplex, H
            public bool HalfDuplex { get; set; }

            //Full Duplex, D
            public bool FullDuplex { get; set; }
            //Closed Network, C
            public bool ClosedNetwork { get; set; }

            //FlNET, F
            public bool FleetNet { get; set; }
        }

        public struct Station
        {
            public int Sat { get; set; }
            public string SatName { get; set; }
            public int LesId { get; set; }
            public string LesName { get; set; }
            public string ServicesStartHex { get; set; }
            public Services Services_ { get; set; }
            public double DownlinkChannelMHz { get; set; }
        }

        public static Services8 ReturnServices8(int is8)
        {
            Services8 s8 = new Services8();

            s8.MaritimeDistressAlerting = (is8 & 0x80) >> 7 == 1;
            s8.SafetyNet = (is8 & 0x40) >> 6 == 1;
            s8.InmarsatC = (is8 & 0x20) >> 5 == 1;
            s8.StoreFwd = (is8 & 0x10) >> 4 == 1;
            s8.HalfDuplex = (is8 & 8) >> 3 == 1;
            s8.FullDuplex = (is8 & 4) >> 2 == 1;
            s8.ClosedNetwork = (is8 & 2) >> 1 == 1;
            s8.FleetNet = (is8 & 1) == 1;

            return s8;
        }

        public struct Status
        {
            public bool Bauds600 { get; set; }
            public bool Operational { get; set; }
            public bool InService { get; set; }
            public bool Clear { get; set; }
            public bool LinksOpen { get; set; }
        }

        /// <summary>
        /// Only the time is transmitted. There is no date part.
        /// </summary>
        /// <param name="frameNo"></param>
        /// <returns></returns>
        public static FrameTimeUTC ReturnFrameTime(int frameNo)
        {
            FrameTimeUTC ftUTC = new FrameTimeUTC();
            try
            {
                double hr = (frameNo * 8.64) / 3600;
                ftUTC.Hour = (int)hr;
                double min = (hr - (int)hr) * 60;
                ftUTC.Minute = (int)min;
                double sec = (min - (int)min) * 60;
                ftUTC.Second = (int)sec;
                double ms = (sec - (int)sec) * 1000;
                ftUTC.Millisecond = (int)ms;
            }
            catch (Exception ex)
            {
                new Exception(string.Format("Error decoding frame time: {0}", ex.Message));
            }

            return ftUTC;
        }

        /// <summary>
        /// Compute a frame date time as the data of the request and the time of the frame
        /// </summary>
        /// <param name="utcNow"></param>
        /// <param name="frameTimeUTC_"></param>
        /// <returns></returns>
        public static DateTime ReturnFrameUtcDateTime(DateTime utcNow, FrameTimeUTC frameTimeUTC_)
        {
            return new DateTime(utcNow.Year, utcNow.Month, utcNow.Day, frameTimeUTC_.Hour, frameTimeUTC_.Minute, frameTimeUTC_.Second, frameTimeUTC_.Millisecond, DateTimeKind.Utc);
        }
    }
}
