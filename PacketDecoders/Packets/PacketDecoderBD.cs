﻿/*
 * microp11 2017
 * 
 * This file is part of Scytale-C.
 * 
 * Scytale-C is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Scytale-C is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Scytale-C.  If not, see <http://www.gnu.org/licenses/>.
 * 
 *   Bibliography:
 * 
 *   Proesch, R., & Daskalaki-Proesch, A. (2013). Technical Handbook for Radio Monitoring VHF/UHF. Norderstedt, Germany: Books on Demand GmbH.
 *   https://www.amazon.ca/Technical-Handbook-Radio-Monitoring-VHF/dp/3732241475/
 *   
 *   Nera. (2015) Nera Inmarsat-C Service Manual. Billingstad: Nera ASA.
 *   https://www.manualslib.com/manual/1201514/Nera-Inmarsat-C.html
 * 
 *   Thrane & Thrane. (2006) TT-3026 easyTrack Transceiver Software Interface Reference Manual. Thrane & Thrane A/S
 *   http://docslide.us/documents/tt3026-software-reference-manual.html
 * 
 *  
 * BDF9B2FC31AE154201002801BB37C74CC1CEC42C2045C1D35420434FC1D354AE20204C796E6E2057E96E64E661F26DAE20436861F2F420C2C12031B038AE208A46EFECECEFF7E96E67204C796E6E2070E5F2E96DE5F4E5F2206275EF796167E52070E5F26D616EE56EF4EC792064E973E3EF6EF4E96E75E564BA208ACE206275EF7920B5B3ADB0B9AE31CE20B0B0B0AD3237AEB5452C208AD3206275EF7920B5B3ADB037AE32CE20B0B0B0AD3238AEB9452C208AD357206275EF7920B5B3ADB037AE32CE20B0B0B0AD32B5AE32452C208A57206275EF7920B5B3ADB038AE32CE20B0B0B0AD32B5AEB34520616E6420CE57206275EF7920B5B30E
 * 
 * BD 
 * F9   - After this we have a quasi-normal B2 (see BlockDecoderB2)
 * B2FC31AE154201002801BB37C74CC1CEC42C2045C1D35420434FC1D354AE20204C796E6E2057E96E64E661F26DAE20436861F2F420C2C12031B038AE208A46EFECECEFF7E96E67204C796E6E2070E5F2E96DE5F4E5F2206275EF796167E52070E5F26D616EE56EF4EC792064E973E3EF6EF4E96E75E564BA208ACE206275EF7920B5B3ADB0B9AE31CE20B0B0B0AD3237AEB5452C208AD3206275EF7920B5B3ADB037AE32CE20B0B0B0AD3238AEB9452C208AD357206275EF7920B5B3ADB037AE32CE20B0B0B0AD32B5AE32452C208A57206275EF7920B5B3ADB038AE32CE20B0B0B0AD32B5AEB34520616E6420CE57206275EF7920B5B30E
 *
 * 
 * BD39AA8515600545C1C446524549C7C85420434CC149CDAE0D8A0D8A57494C4C20D345CEC4204FD55220D64FD92049CED35458CE2057C845CE51
 *
 * BD
 * 39   - After this we have a quasi-normal AA (see BlockDecoderAA)
 * AA8515600545C1C446524549C7C85420434CC149CDAE0D8A0D8A57494C4C20D345CEC4204FD55220D64FD92049CED35458CE2057C845CE51
 * 
 *  TODO completeness, areas
 *  
 *  Finally I think I am getting closer to understanding these packets.
 *  It looks like the BD is just a shell for any packet that spans multiple frames. The continuations are BE packets.
 *  So instead of decoding it, we will just compose the encapsulated packet and will trigger a packet decode when fully in.
 * 
 */

using ScytaleC.Interfaces;
using System;
using System.Diagnostics;

namespace ScytaleC.PacketDecoders

{
    /// <summary>
    /// BD - Multiframe Message Start
    /// </summary>
    public class PacketDecoderBD : PacketDecoder
    {
        public int MultiframePacketDescriptor { get; set; }
        public string MultiframePacketDescriptorHex { get; set; }
        public int MultiFramePacketLength { get; private set; }

        //default constructor needed for dynamic casting
        public PacketDecoderBD() { }

        /// <summary>
        /// It looks like BD can contain any type of packet. If BE follows, it contains the continuation of the previous packet regardless of type.
        /// The BE does not need to know about content, only about its predecessor.
        /// </summary>
        public void Decode(DescrambledFrameArgs args, ref int pos, out MultiFramePacket mfa)
        {
            Debug.WriteLine("BD");
            mfa = new MultiFramePacket();

            try
            {
                //this will give us the length of the BD packet and the expected length of the encapsulated packet.
                base.Decode(args, ref pos);
                if (!IsCRC)
                {
                    return;
                }

                DecodingStage_ = DecodingStage.Complete;

                MultiframePacketDescriptor = args.DescrambledFrame[pos + 2] & 0xFF;
                MultiframePacketDescriptorHex = MultiframePacketDescriptor.ToString("X");

                if (MultiframePacketDescriptor >> 7 == 0)
                {
                    /// Short packet descriptor
                    /// The packet length including CRC does not include byte 0, we add 1
                    MultiFramePacketLength = (MultiframePacketDescriptor & 0x0F) + 1;
                }
                else if (MultiframePacketDescriptor >> 6 == 0x02)
                {
                    /// Medium packet descriptor
                    /// The packet length including CRC does not include the first 2 bytes, we add 2
                    MultiFramePacketLength = args.DescrambledFrame[pos + 3] + 2;
                }

                Debug.WriteLine("MultiframePacketLength {0} length {1} for a PacketLength of {2}", MultiframePacketDescriptorHex, MultiFramePacketLength, PacketLength);

                //compose encapsulated packet, length includes the CRC for the composed packet
                mfa.PacketData = new byte[MultiFramePacketLength];

                //set the length of payload data to be used for the new packet
                //the payload starts at index + 2 and does not include the CRC
                mfa.FirstPartCount = PacketLength - 2 - 2;
                Array.Copy(args.DescrambledFrame, pos + 2, mfa.PacketData, 0, mfa.FirstPartCount);
            }
            catch (Exception ex)
            {
                CrashReport = string.Format("{0} [{1}]", ex.Message, ex.StackTrace);
            }
        }
    }
}