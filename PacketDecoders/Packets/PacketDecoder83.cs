﻿/*
 * microp11, holger 2017
 * 
 * This file is part of Scytale-C.
 * 
 * Scytale-C is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Scytale-C is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Scytale-C.  If not, see <http://www.gnu.org/licenses/>.
 *
 *   Bibliography:
 * 
 *   Proesch, R., & Daskalaki-Proesch, A. (2013). Technical Handbook for Radio Monitoring VHF/UHF. Norderstedt, Germany: Books on Demand GmbH.
 *   https://www.amazon.ca/Technical-Handbook-Radio-Monitoring-VHF/dp/3732241475/
 *   
 *   Nera. (2015) Nera Inmarsat-C Service Manual. Billingstad: Nera ASA.
 *   https://www.manualslib.com/manual/1201514/Nera-Inmarsat-C.html
 * 
 *   Thrane & Thrane. (2006) TT-3026 easyTrack Transceiver Software Interface Reference Manual. Thrane & Thrane A/S
 *   http://docslide.us/documents/tt3026-software-reference-manual.html
 *   
 * 
 * 83 -Logical Channel Assignment
 * 
 * 83 10 BCF5AE 55 E4 1E 01 01 2E5E 2E20 42 03 6036
 *                                          |
 *                                          PacketDescriptor1, meaning unknown yet
 *                                          
 * 83 - LogicalChannelAssignment MES_Id: BCF5AE  LES_Id: 21  LCnr: 30  Uplink: 1640,02 MHz
 * StatusBits: 228  FrameLength: 1  Duration: 1  Downlink: 1539,52 MHz FrameOffset: 66
 * 
 * */

using ScytaleC.Interfaces;
using System;
using System.Diagnostics;

namespace ScytaleC.PacketDecoders
{
    public class PacketDecoder83 : PacketDecoder
    {
        public int MesId { get; set; }
        public string MesIdHex { get; set; }
        public int Sat { get; set; }
        public string SatName { get; set; }
        public int LesId { get; set; }
        public string LesName { get; set; }
        public int LogicalChannelNo { get; set; }
        public int FrameLength { get; set; }
        public int Duration { get; set; }
        public double DownlinkChannelMHz { get; set; }
        public byte StatusBits { get; set; }
        public double UplinkChannelMHz { get; set; }
        public int FrameOffset { get; set; }
        public byte PacketDescriptor1 { get; set; }
        public string PacketDescriptor1Hex { get; set; }

        public override void Decode(DescrambledFrameArgs args, ref int pos)
        {
            Debug.WriteLine("83");
            try
            {
                base.Decode(args, ref pos);
                if (!IsCRC)
                {
                    return;
                }

                DecodingStage_ = DecodingStage.Partial;

                MesId = PacketDecoderUtils.ReturnMesId(args.DescrambledFrame, pos + 2);
                MesIdHex = Utils.BytesToHexString(args.DescrambledFrame, pos + 2, 3);

                Sat = PacketDecoderUtils.ReturnSat(args.DescrambledFrame[pos + 5]);
                SatName = PacketDecoderUtils.ReturnSatName(Sat);

                LesId = PacketDecoderUtils.ReturnLesId(args.DescrambledFrame[pos + 5]);
                LesName = PacketDecoderUtils.ReturnLesName(Sat, LesId);

                StatusBits = args.DescrambledFrame[pos + 6];

                LogicalChannelNo = args.DescrambledFrame[pos + 7];

                FrameLength = args.DescrambledFrame[pos + 8];

                Duration = args.DescrambledFrame[pos + 9];

                DownlinkChannelMHz = PacketDecoderUtils.ReturnDownlinkChannelMHz(args.DescrambledFrame, pos + 10);

                UplinkChannelMHz = PacketDecoderUtils.ReturnUplinkChannelMHz(args.DescrambledFrame, pos + 12);

                FrameOffset = args.DescrambledFrame[pos + 14];

                PacketDescriptor1 = args.DescrambledFrame[pos + 15];
                PacketDescriptor1Hex = Utils.BytesToHexString(args.DescrambledFrame, pos + 15, 1);
            }
            catch (Exception ex)
            {
                CrashReport = string.Format("{0} [{1}]", ex.Message, ex.StackTrace);
            }
        }
    }
}