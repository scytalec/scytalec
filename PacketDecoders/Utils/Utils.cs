﻿/*
 * microp11 2017
 * 
 * TODO 
 * Helper class.
 * 
 * https://stackoverflow.com/questions/28751192/how-can-i-prevent-windows-os-from-entering-sleep-mode-net
 * https://stackoverflow.com/questions/321370/how-can-i-convert-a-hex-string-to-a-byte-array
 * 
 */

using NLog;
using ScytaleC.Interfaces;
using System;
using System.Globalization;
using System.IO;
using System.Runtime.InteropServices;
using System.Text;

namespace ScytaleC.PacketDecoders
{
    public class Utils
    {
        public static Logger Log = LogManager.GetLogger("errors");
        public static Logger LogFrames = LogManager.GetLogger("frames");

        private static readonly char[] hexChars = new char[] {
            '0', '1', '2', '3',
            '4', '5', '6', '7',
            '8', '9', 'A', 'B',
            'C', 'D', 'E', 'F'
        };

        /// <summary>
        /// Return the hex representation of the input array.
        /// </summary>
        /// <param name="input">Input byte array.</param>
        /// <param name="start">Starts converting bytes to hex from "start", inclusive.</param>
        /// <param name="length">Converts "length" bytes.</param>
        /// <returns>Returns the exception message if any error occurs.</returns>
        public static string BytesToHexString(byte[] input, int start, int length)
        {
            try
            {
                StringBuilder result = new StringBuilder();
                for (int i = start; i < start + length; i++)
                {
                    result.Append(hexChars[(byte)(input[i] & 0xF0) >> 4]);
                    result.Append(hexChars[input[i] & 0x0F]);
                }

                return result.ToString();
            }
            catch (Exception e)
            {
                return e.Message;
            }
        }

        public static byte[] HexStringToBytes(string hexString)
        {
            if (hexString.Length % 2 != 0)
            {
                hexString = hexString + "0";
            }

            byte[] HexAsBytes = new byte[hexString.Length / 2];
            for (int index = 0; index < HexAsBytes.Length; index++)
            {
                string byteValue = hexString.Substring(index * 2, 2);
                HexAsBytes[index] = byte.Parse(byteValue, NumberStyles.HexNumber, CultureInfo.InvariantCulture);
            }

            return HexAsBytes;
        }
    }

    public enum EXECUTION_STATE : uint
    {
        ES_CONTINUOUS = 0x80000000,
        ES_SYSTEM_REQUIRED = 0x00000001,
        ES_DISPLAY_REQUIRED = 0x00000002
    }

    public class NativeMethods
    {
        [DllImport("kernel32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        public static extern EXECUTION_STATE SetThreadExecutionState(EXECUTION_STATE esFlags);
    }

    public class WriteDoubleToFile :IDisposable
    {
        FileStream f;
        BinaryWriter bw;

        public WriteDoubleToFile(string fileName)
        {
            f = File.Open(fileName, FileMode.Append);

            bw = new BinaryWriter(f);
        }

        public void Dispose()
        {
            bw?.Close();
        }

        public void WriteDouble(double value)
        {
            bw.Write(value);
        }
    }

    public static class FileOutput
    {
        public static string SelectedPath { get; set; }
        public static string SelectedPathDirectory = SelectedPath + @"output\\";
        public static string OutputDemodulatedBitsFile = SelectedPathDirectory + @"OutputDemodulatedBits.txt";

        /// <summary>
        /// If the path does not exist, nothing gets saved.
        /// 
        /// Takes an array of bits, each bit is represented by a full byte (e.g. bit 0 = 0x00, bit 1 = 0x01) 
        /// It reverses the order of the array.
        /// Converts the array to a hex string.
        /// Saves each array in one line, then adds a Windows CRLF.
        /// 
        /// The resulted text represents the demodulator output, oldest bit at the beginning of the text file.
        /// </summary>
        /// <param name="e">An array of bytes, where each byte represents a bit as it come out of the demodulator.</param>
        public static void WriteOutputDemodulatedDataToFile(DemodulatedSymbolsArgs e)
        {
            if (!Directory.Exists(SelectedPathDirectory))
            {
                Directory.CreateDirectory(SelectedPathDirectory);
            }

            using (StreamWriter w = File.AppendText(OutputDemodulatedBitsFile))
            {
                Array.Reverse(e.Symbols);
                //w.Write(BitConverter.ToString(e.Data).Replace("-", string.Empty));
                //w.Write(System.Text.Encoding.ASCII.GetString(e.Data));
                string s = "";
                for (int i = 0; i < e.Length; i++)
                {
                    if (e.Symbols[i] == 0x00)
                    {
                        s += "0";
                    }
                    else
                    {
                        s += "1";
                    }
                }
                w.Write(s);
            }
        }
    }
}
