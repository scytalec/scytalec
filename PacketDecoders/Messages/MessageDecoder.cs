﻿/*
 * microp11 2017
 * 
 * This file is part of Scytale-C.
 * 
 * Scytale-C is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Scytale-C is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Scytale-C.  If not, see <http://www.gnu.org/licenses/>.
* 
*   Bibliography:
* 
*   Proesch, R., & Daskalaki-Proesch, A. (2013). Technical Handbook for Radio Monitoring VHF/UHF. Norderstedt, Germany: Books on Demand GmbH.
*   https://www.amazon.ca/Technical-Handbook-Radio-Monitoring-VHF/dp/3732241475/
*   
*   Nera. (2015) Nera Inmarsat-C Service Manual. Billingstad: Nera ASA.
*   https://www.manualslib.com/manual/1201514/Nera-Inmarsat-C.html
* 
*   Thrane & Thrane. (2006) TT-3026 easyTrack Transceiver Software Interface Reference Manual. Thrane & Thrane A/S
*   http://docslide.us/documents/tt3026-software-reference-manual.html
*   
*   Inmarsat-C. EGC Services: SafetyNET & FleetNET Users manual (2002). Xantic B.V.
*   http://www.xantic.net/internet/files/products/inmarsat/inmarsat_c/support_docs/InmC-EGC-SafetyNETandFleetNET%20mnl.pdf
*
*
*   Ita2 and binary go beyound the byte boundary. They will be decoded only when the message is fully assembled.
*/


using System.Text;

namespace ScytaleC.PacketDecoders
{
    public class MessageDecoder
    {
        private PacketDecoder pd;
        //private Ita2Decoder ita;
        private Encoding enc;

        public MessageDecoder(PacketDecoder pd)
        {
            this.pd = pd;
            //ita = new Ita2Decoder();
            enc = Encoding.GetEncoding("x-IA5", new EncoderExceptionFallback(), new DecoderExceptionFallback());
        }

        public void DecodeMessage()
        {
            pd.Payload_.PresentationName = PacketDecoderUtils.ReturnPresentation(pd.Payload_.Presentation);

            switch (pd.Payload_.Presentation)
            {
                //IA 5
                case 0:
                    byte[] data7Bit = PacketDecoderUtils.Return7BitData(pd.Payload_.Data8Bit);
                    pd.Payload_.Ia5 = enc.GetString(data7Bit, 0, data7Bit.Length);
                    break;
                
                //ITA 2
                case 6:
                    //pd.Payload_.Ita2 = ita.Decode(Utils.BytesToHexString(pd.Payload_.Data8Bit, 0, pd.Payload_.Data8Bit.Length));
                    break;

                //8-bit data
                case 7:
                    //there is nothing to do here
                    break;

                default:
                    // When the Presentation is not set at the packet level,
                    // for example in the case of the AA packets.
                    // We must ensure we do not get here with an unknown Presentation.
                    // Either the packet decoder or the packet detector must set it.
                    pd.Payload_.Ia5 = "TODO";
                    pd.Payload_.Ita2 = "TODO";
                    break;
            }
        }
    }
}
