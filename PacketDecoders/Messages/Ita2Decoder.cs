﻿/*
 * holger 2017
 * 
 * This file is part of Scytale-C.
 * 
 * Scytale-C is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Scytale-C is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Scytale-C.  If not, see <http://www.gnu.org/licenses/>.
 *
 *   Bibliography:
 * 
 *   Proesch, R., & Daskalaki-Proesch, A. (2013). Technical Handbook for Radio Monitoring VHF/UHF. Norderstedt, Germany: Books on Demand GmbH.
 *   https://www.amazon.ca/Technical-Handbook-Radio-Monitoring-VHF/dp/3732241475/
 *   
 *   Nera. (2015) Nera Inmarsat-C Service Manual. Billingstad: Nera ASA.
 *   https://www.manualslib.com/manual/1201514/Nera-Inmarsat-C.html
 * 
 *   Thrane & Thrane. (2006) TT-3026 easyTrack Transceiver Software Interface Reference Manual. Thrane & Thrane A/S
 *   http://docslide.us/documents/tt3026-software-reference-manual.html
 *   
 * 
 * A simple ITA2/CCITT-2 decoder
 * 
 *   https://en.wikipedia.org/wiki/Baudot_code
 *   
 *   https://github.com/TonyWhite/Baudot/blob/master/src/Baudot/CodificaBaudot.java
 *   
 *   
 *   TODO: Adding some error checking
 * 
 * 
 * */

using System;
using System.Collections.Generic;
using System.Linq;

namespace ScytaleC.PacketDecoders
{
    public class Ita2Decoder
    {

        Dictionary<string, int> conversion = new Dictionary<string, int>();
        Dictionary<int, string> chars = new Dictionary<int, string>();
        Dictionary<int, string> figs = new Dictionary<int, string>();

        bool isFigure;

        public Ita2Decoder()
        {
            init();
        }


        public void init()
        {
            isFigure = false;

            conversion.Add("00000", 0); chars.Add(0, ""); figs.Add(0, ""); // {NULL} we don't need this in Windows
            conversion.Add("00001", 1); chars.Add(1, "E"); figs.Add(1, "3");
            conversion.Add("00010", 2); chars.Add(2, ""); figs.Add(2, ""); // {LF} we don't need this in Windows
            conversion.Add("00011", 3); chars.Add(3, "A"); figs.Add(3, "-");
            conversion.Add("00100", 4); chars.Add(4, " "); figs.Add(4, " ");
            conversion.Add("00101", 5); chars.Add(5, "S"); figs.Add(5, "'");
            conversion.Add("00110", 6); chars.Add(6, "I"); figs.Add(6, "8");
            conversion.Add("00111", 7); chars.Add(7, "U"); figs.Add(7, "7");
            conversion.Add("01000", 8); chars.Add(8, "\n"); figs.Add(8, "\n"); //{CR}
            conversion.Add("01001", 9); chars.Add(9, "D"); figs.Add(9, "{ENQ}");
            conversion.Add("01010", 10); chars.Add(10, "R"); figs.Add(10, "4");
            conversion.Add("01011", 11); chars.Add(11, "J"); figs.Add(11, "{BELL}");
            conversion.Add("01100", 12); chars.Add(12, "N"); figs.Add(12, ",");
            conversion.Add("01101", 13); chars.Add(13, "F"); figs.Add(13, "!");
            conversion.Add("01110", 14); chars.Add(14, "C"); figs.Add(14, ":");
            conversion.Add("01111", 15); chars.Add(15, "K"); figs.Add(15, "(");
            conversion.Add("10000", 16); chars.Add(16, "T"); figs.Add(16, "5");
            conversion.Add("10001", 17); chars.Add(17, "Z"); figs.Add(17, "+");
            conversion.Add("10010", 18); chars.Add(18, "L"); figs.Add(18, ")");
            conversion.Add("10011", 19); chars.Add(19, "W"); figs.Add(19, "2");
            conversion.Add("10100", 20); chars.Add(20, "H"); figs.Add(20, "£");
            conversion.Add("10101", 21); chars.Add(21, "Y"); figs.Add(21, "6");
            conversion.Add("10110", 22); chars.Add(22, "P"); figs.Add(22, "0");
            conversion.Add("10111", 23); chars.Add(23, "Q"); figs.Add(23, "1");
            conversion.Add("11000", 24); chars.Add(24, "O"); figs.Add(24, "9");
            conversion.Add("11001", 25); chars.Add(25, "B"); figs.Add(25, "?");
            conversion.Add("11010", 26); chars.Add(26, "G"); figs.Add(26, "&");
            conversion.Add("11011", 27); chars.Add(27, ""); figs.Add(27, ""); //Switch to figures
            conversion.Add("11100", 28); chars.Add(28, "M"); figs.Add(28, ".");
            conversion.Add("11101", 29); chars.Add(29, "X"); figs.Add(29, "/");
            conversion.Add("11110", 30); chars.Add(30, "V"); figs.Add(30, ";");
            conversion.Add("11111", 31); chars.Add(31, ""); figs.Add(31, ""); //Switch to letters
        }

        private string doMapping(string itabin)
        {
            //get key to chars and figures
            int kvalue;
            if (conversion.TryGetValue(itabin, out kvalue))
            {
                if (kvalue == 27) isFigure = true;
                if (kvalue == 31) isFigure = false;
            }

            //get letter or figure
            if (!isFigure)
            {
                string svalue;
                if (chars.TryGetValue(kvalue, out svalue))
                {
                    return (svalue);
                }
            }
            else
            {
                string svalue;
                if (figs.TryGetValue(kvalue, out svalue))
                {
                    return (svalue);
                }
            }

            return ("");
        }

        private string ConvertHexToBinary(string Hexstring)
        {
            string binarystring = String.Join(String.Empty, Hexstring.Select(c => Convert.ToString(Convert.ToInt32(c.ToString(), 16), 2).PadLeft(4, '0')));

            return (binarystring);

        }

        public string Decode(string Hexstring)
        {

            string decodedita = "";

            //we use this to store the hexvalues as binary strings
            string[] binarystringarray = new string[((Hexstring.Length / 2) + 1)];
            int arraycounter = 0;

            for (int j = 0; j < Hexstring.Length; j++)
            {
                string hex = Hexstring.Substring(j, 2);
                binarystringarray[arraycounter] = ConvertHexToBinary(hex);
                arraycounter++;
                j++;
            }

            // 39 48 39 A0 32 7D 8F BF B6 23 9F 0F 23 91 48
            // BELLATRIX-J: MAIL
            // 11001 00001 10010 10010 00011 10000 01010 00110 11101 11011 00011 11111 01011 11011 01110 00100 11111 11100 00011 00110 10010 01000 00010 01001
            // B     E     L     L     A     T     R     I     X     fig   -     ltr   J     fig   :     space ltr   M     A     I     L     cr    lf    D
            //
            // The 8 bit binarystring we are getting from Hex need some reassembling. The ITA2 char is in the lower 5 bits.
            // The remaining higher 3 bits are used as lower bits in the next binary string
            // 39         48
            // 001 11001  01001000
            // 11001      0 10010 00001
            // 11001      00001 10010
            //   B          E     L
            // 0 is the lower bit for the next binary string. And so on...

            for (int j = 0; j < binarystringarray.Length - 1; j++)
            {

                string totest = binarystringarray[j];
                string itabin = "";

                if (totest.Length == 8) // 1 char, 3 leftover
                {
                    itabin = totest.Substring(totest.Length - 5, 5);
                    string rest = totest.Substring(0, totest.Length - 5);
                    binarystringarray[j + 1] = binarystringarray[j + 1] + rest;

                    decodedita += doMapping(itabin);
                }
                else if (totest.Length == 11) // 2 chars, 1 leftover
                {
                    itabin = totest.Substring(totest.Length - 5, 5);
                    decodedita += doMapping(itabin);

                    itabin = totest.Substring(totest.Length - 10, 5);
                    decodedita += doMapping(itabin);

                    string rest = totest.Substring(0, totest.Length - 10);
                    binarystringarray[j + 1] = binarystringarray[j + 1] + rest;

                }
                else if (totest.Length == 9) // 1 char, 4 leftover
                {
                    itabin = totest.Substring(totest.Length - 5, 5);
                    decodedita += doMapping(itabin);

                    string rest = totest.Substring(0, totest.Length - 5);
                    binarystringarray[j + 1] = binarystringarray[j + 1] + rest;

                }
                else if (totest.Length == 12) // 2 chars, 2 leftover
                {
                    itabin = totest.Substring(totest.Length - 5, 5);
                    decodedita += doMapping(itabin);

                    itabin = totest.Substring(totest.Length - 10, 5);
                    decodedita += doMapping(itabin);

                    string rest = totest.Substring(0, totest.Length - 10);
                    binarystringarray[j + 1] = binarystringarray[j + 1] + rest;

                }
                else if (totest.Length == 10) // 2 chars, no leftover
                {
                    itabin = totest.Substring(totest.Length - 5, 5);
                    decodedita += doMapping(itabin);

                    itabin = totest.Substring(totest.Length - 10, 5);
                    decodedita += doMapping(itabin);

                }

            }

            return (decodedita);

        }
    }
}
