﻿/*
 * microp11 2018
 * 
 * This file is part of Scytale-C.
 * 
 * Scytale-C is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Scytale-C is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Scytale-C.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

using System;
using System.Diagnostics;
using System.IO;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Windows.Forms;

namespace ScytaleC
{
    public partial class LHFForm : Form
    {
        private bool sendingLine;
        private bool readerInitialized;
        private TextReader reader;
        private UdpTx udpTx;
        private readonly int MAX_LINES = 100;

        public LHFForm()
        {
            InitializeComponent();

            sendingLine = false;
            readerInitialized = false;
            udpTx = new UdpTx();
        }

        private void btnSource_Click(object sender, EventArgs e)
        {
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                textBox1.Text = openFileDialog1.FileName;
                readerInitialized = false;
            }
        }

        private void btnSendNext_Click(object sender, EventArgs e)
        {
            if (sendingLine) return;

            if (!readerInitialized) InitializeReader();

            sendingLine = true;

            try
            {
                LockWindowUpdate(rtbFrames.Handle);

                #region RestrictNumberOfPacketsDisplayed
                if (cbLimit.Checked)
                {
                    int maxPacketLinesOnDisplay = MAX_LINES;
                    if (rtbFrames.Lines.Length > maxPacketLinesOnDisplay)
                    {
                        rtbFrames.SelectionStart = rtbFrames.GetFirstCharIndexFromLine(0);
                        rtbFrames.SelectionLength = rtbFrames.Lines[rtbFrames.Lines.Length - maxPacketLinesOnDisplay].Length + 100;
                        rtbFrames.SelectedText = String.Empty;
                    }
                }
                #endregion

                //read next line
                if (reader.Peek() > 0)
                {
                    string line = reader.ReadLine();

                    string[] parts = line.Split(' ');
                    if (parts.Length > 2)
                    {
                        string frameNumber = parts[0];
                        string sid = parts[1];
                        string frame = parts[2];
                        string toSend;

                        //udp
                        byte[] bsid = BitConverter.GetBytes(Convert.ToInt64(sid));
                        sid = Utils.BytesToHexString(bsid, 0, bsid.Length);
                        udpTx.Send(txtUdpAddress.Text, (int)nUdpPort.Value, Utils.HexStringToBytes(frame + sid));

                        //display
                        byte[] bframe = Utils.HexStringToBytes(frame);
                        if (cbShowTime.Checked)
                        {
                            string time = GetTimeFromFrame(frameNumber);
                            toSend = string.Format("{0} {1}   ({2}) {3}", frameNumber, time, sid, frame);
                        }
                        else
                        {
                            toSend = string.Format("{0}   ({1}) {2}", frameNumber, sid, frame);
                        }
                        rtbFrames.AppendText(toSend);
                    }
                    else if (parts.Length > 1)
                    {
                        string sid = parts[0];
                        string frame = parts[1];
                        string toSend;

                        //udp
                        byte[] bsid = BitConverter.GetBytes(Convert.ToInt64(sid));
                        sid = Utils.BytesToHexString(bsid, 0, bsid.Length);
                        udpTx.Send(txtUdpAddress.Text, (int)nUdpPort.Value, Utils.HexStringToBytes(frame + sid));

                        //display
                        byte[] bframe = Utils.HexStringToBytes(frame);
                        int frameNumber = bframe[2] << 8 | bframe[3];
                        if (cbShowTime.Checked)
                        {
                            string time = GetTimeFromFrame(frameNumber);
                            toSend = string.Format("{0} {1}   ({2}) {3}", frameNumber, time, sid, frame);
                        }
                        else
                        {
                            toSend = string.Format("{0}   ({1}) {2}", frameNumber, sid, frame);
                        }
                        rtbFrames.AppendText(toSend);
                    }
                    else
                    {
                        rtbFrames.AppendText(line);
                    }
                    rtbFrames.AppendText(Environment.NewLine);
                    rtbFrames.ScrollToCaret();
                }
            }
            catch (Exception ex)
            {
                rtbFrames.AppendText(Environment.NewLine);
                rtbFrames.AppendText(ex.Message);
            }
            finally
            {
                sendingLine = false;
                LockWindowUpdate(IntPtr.Zero);
            }
        }

        /// <summary>
        /// Gets the Zulu and Juliet time from an Inmarsat-C frame number, every frame represents 8.64 seconds
        /// </summary>
        /// <typeparam name="T">string or integer</typeparam>
        /// <param name="arg">frame number</param>
        /// <returns>A string in the format HH:mm:ssZ HH:mm:ssJ</returns>
        private string GetTimeFromFrame<T>(T arg)
        {
            double time;

            try
            {
                time = (arg is string) ? Convert.ToInt32(arg) : (int)(object)arg;
                time *= 8.64;

                DateTime dtZ = DateTime.MinValue.AddDays(1).AddSeconds(time);
                DateTime dtJ = dtZ.ToLocalTime();

                int diff = dtJ.DayOfYear - dtZ.DayOfYear;
                if (diff != 0)
                {
                    return string.Format("[{0:HH:mm:ssZ} {1:HH:mm:ssJ}({2})]", dtZ, dtJ, diff);
                }
                else
                {
                    return string.Format("[{0:HH:mm:ssZ} {1:HH:mm:ssJ}]", dtZ, dtJ);
                }
            }
            catch
            {
                return string.Empty;
            }
        }

        private void InitializeReader()
        {
            if (reader != null) reader.Close();
            reader = File.OpenText(textBox1.Text);

            readerInitialized = true;
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            if (checkBox1.Checked) btnSendNext_Click(null, null);
        }

        private void LHFForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (reader != null) reader.Close();
        }

        private void numericUpDown1_ValueChanged(object sender, EventArgs e)
        {
            timer1.Interval = (int)numericUpDown1.Value;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            readerInitialized = false;
        }

        [DllImport("user32.dll")]
        public static extern bool LockWindowUpdate(IntPtr hWndLock);

        private void LHFForm_Load(object sender, EventArgs e)
        {
            lblVersion.Text = string.Format("v.{0} BETA", FileVersionInfo.GetVersionInfo(Assembly.GetExecutingAssembly‌​().Location).Product‌​Version);
        }

        private void linkLabel_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            try
            {
                linkLabel.LinkVisited = true;
                Process.Start("https://bitbucket.org/scytalec/scytalec/");
            }
            catch { }
        }
    }
}
