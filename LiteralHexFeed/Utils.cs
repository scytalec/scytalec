﻿/*
 * microp11 2018
 * 
 * This file is part of Scytale-C.
 * 
 * Scytale-C is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Scytale-C is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Scytale-C.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

using System;
using System.Globalization;
using System.Text;

namespace ScytaleC
{
    public static class Utils
    {
        private static char[] hexChars = new char[] {
            '0', '1', '2', '3',
            '4', '5', '6', '7',
            '8', '9', 'A', 'B',
            'C', 'D', 'E', 'F'
        };

        /// <summary>
        /// Return the hex representation of the input array.
        /// </summary>
        /// <param name="input">Input byte array.</param>
        /// <param name="start">Starts converting bytes to hex from "start", inclusive.</param>
        /// <param name="length">Converts "length" bytes.</param>
        /// <returns>Returns the exception message if any error occurs.</returns>
        public static string BytesToHexString(byte[] input, int start, int length)
        {
            try
            {
                StringBuilder result = new StringBuilder();
                for (int i = start; i < start + length; i++)
                {
                    result.Append(hexChars[(byte)(input[i] & 0xF0) >> 4]);
                    result.Append(hexChars[input[i] & 0x0F]);
                }

                return result.ToString();
            }
            catch (Exception e)
            {
                return e.Message;
            }
        }

        public static byte[] HexStringToBytes(string hexString)
        {
            if (hexString.Length % 2 != 0)
            {
                hexString = hexString + "0";
            }

            byte[] HexAsBytes = new byte[hexString.Length / 2];
            for (int index = 0; index < HexAsBytes.Length; index++)
            {
                string byteValue = hexString.Substring(index * 2, 2);
                HexAsBytes[index] = byte.Parse(byteValue, NumberStyles.HexNumber, CultureInfo.InvariantCulture);
            }

            return HexAsBytes;
        }
    }
}
