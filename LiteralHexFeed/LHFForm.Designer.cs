﻿namespace ScytaleC
{
    partial class LHFForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.nUdpPort = new System.Windows.Forms.NumericUpDown();
            this.label3 = new System.Windows.Forms.Label();
            this.txtUdpAddress = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.btnSource = new System.Windows.Forms.Button();
            this.btnSendNext = new System.Windows.Forms.Button();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.numericUpDown1 = new System.Windows.Forms.NumericUpDown();
            this.label2 = new System.Windows.Forms.Label();
            this.rtbFrames = new System.Windows.Forms.RichTextBox();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.button1 = new System.Windows.Forms.Button();
            this.cbShowTime = new System.Windows.Forms.CheckBox();
            this.cbLimit = new System.Windows.Forms.CheckBox();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.linkLabel = new System.Windows.Forms.LinkLabel();
            this.lblVersion = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.nUdpPort)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).BeginInit();
            this.SuspendLayout();
            // 
            // nUdpPort
            // 
            this.nUdpPort.Location = new System.Drawing.Point(625, 37);
            this.nUdpPort.Maximum = new decimal(new int[] {
            65535,
            0,
            0,
            0});
            this.nUdpPort.Minimum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.nUdpPort.Name = "nUdpPort";
            this.nUdpPort.Size = new System.Drawing.Size(52, 20);
            this.nUdpPort.TabIndex = 3;
            this.nUdpPort.Value = new decimal(new int[] {
            15003,
            0,
            0,
            0});
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(506, 21);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(89, 13);
            this.label3.TabIndex = 97;
            this.label3.Text = "Destination UDP:";
            // 
            // txtUdpAddress
            // 
            this.txtUdpAddress.Location = new System.Drawing.Point(509, 37);
            this.txtUdpAddress.Name = "txtUdpAddress";
            this.txtUdpAddress.Size = new System.Drawing.Size(110, 20);
            this.txtUdpAddress.TabIndex = 2;
            this.txtUdpAddress.Text = "localhost";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(9, 20);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(410, 13);
            this.label1.TabIndex = 101;
            this.label1.Text = "Source as literal hex one frame prepended by frame number (optional) and sid per " +
    "line:";
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(12, 36);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(434, 20);
            this.textBox1.TabIndex = 0;
            // 
            // btnSource
            // 
            this.btnSource.Location = new System.Drawing.Point(452, 35);
            this.btnSource.Name = "btnSource";
            this.btnSource.Size = new System.Drawing.Size(28, 22);
            this.btnSource.TabIndex = 1;
            this.btnSource.Text = "...";
            this.btnSource.UseVisualStyleBackColor = true;
            this.btnSource.Click += new System.EventHandler(this.btnSource_Click);
            // 
            // btnSendNext
            // 
            this.btnSendNext.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnSendNext.Location = new System.Drawing.Point(369, 423);
            this.btnSendNext.Name = "btnSendNext";
            this.btnSendNext.Size = new System.Drawing.Size(100, 22);
            this.btnSendNext.TabIndex = 7;
            this.btnSendNext.Text = "Play Next";
            this.btnSendNext.UseVisualStyleBackColor = true;
            this.btnSendNext.Click += new System.EventHandler(this.btnSendNext_Click);
            // 
            // checkBox1
            // 
            this.checkBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.checkBox1.AutoSize = true;
            this.checkBox1.Location = new System.Drawing.Point(521, 428);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(75, 17);
            this.checkBox1.TabIndex = 8;
            this.checkBox1.Text = "Play every";
            this.checkBox1.UseVisualStyleBackColor = true;
            // 
            // numericUpDown1
            // 
            this.numericUpDown1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.numericUpDown1.Increment = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.numericUpDown1.Location = new System.Drawing.Point(593, 426);
            this.numericUpDown1.Maximum = new decimal(new int[] {
            65535,
            0,
            0,
            0});
            this.numericUpDown1.Minimum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.numericUpDown1.Name = "numericUpDown1";
            this.numericUpDown1.Size = new System.Drawing.Size(52, 20);
            this.numericUpDown1.TabIndex = 9;
            this.numericUpDown1.Value = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.numericUpDown1.ValueChanged += new System.EventHandler(this.numericUpDown1_ValueChanged);
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(646, 430);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(20, 13);
            this.label2.TabIndex = 106;
            this.label2.Text = "ms";
            // 
            // rtbFrames
            // 
            this.rtbFrames.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.rtbFrames.Location = new System.Drawing.Point(12, 85);
            this.rtbFrames.Name = "rtbFrames";
            this.rtbFrames.Size = new System.Drawing.Size(665, 335);
            this.rtbFrames.TabIndex = 107;
            this.rtbFrames.Text = "";
            this.rtbFrames.WordWrap = false;
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Interval = 1000;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.Filter = "Log files|*.log";
            // 
            // button1
            // 
            this.button1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.button1.Location = new System.Drawing.Point(272, 424);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(56, 22);
            this.button1.TabIndex = 6;
            this.button1.Text = "Restart";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // cbShowTime
            // 
            this.cbShowTime.AutoSize = true;
            this.cbShowTime.Location = new System.Drawing.Point(12, 67);
            this.cbShowTime.Name = "cbShowTime";
            this.cbShowTime.Size = new System.Drawing.Size(104, 17);
            this.cbShowTime.TabIndex = 4;
            this.cbShowTime.Text = "Show frame time";
            this.cbShowTime.UseVisualStyleBackColor = true;
            // 
            // cbLimit
            // 
            this.cbLimit.AutoSize = true;
            this.cbLimit.Location = new System.Drawing.Point(133, 67);
            this.cbLimit.Name = "cbLimit";
            this.cbLimit.Size = new System.Drawing.Size(178, 17);
            this.cbLimit.TabIndex = 5;
            this.cbLimit.Text = "Limit display lines (less flickering)";
            this.cbLimit.UseVisualStyleBackColor = true;
            // 
            // statusStrip1
            // 
            this.statusStrip1.BackColor = System.Drawing.SystemColors.ControlLight;
            this.statusStrip1.Location = new System.Drawing.Point(0, 454);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(690, 22);
            this.statusStrip1.TabIndex = 111;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // linkLabel
            // 
            this.linkLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.linkLabel.AutoSize = true;
            this.linkLabel.BackColor = System.Drawing.SystemColors.ControlLight;
            this.linkLabel.ForeColor = System.Drawing.SystemColors.ControlDark;
            this.linkLabel.LinkColor = System.Drawing.SystemColors.ControlDark;
            this.linkLabel.Location = new System.Drawing.Point(515, 459);
            this.linkLabel.Name = "linkLabel";
            this.linkLabel.Size = new System.Drawing.Size(163, 13);
            this.linkLabel.TabIndex = 10;
            this.linkLabel.TabStop = true;
            this.linkLabel.Text = "Open source Inmarsat-C decoder";
            this.linkLabel.VisitedLinkColor = System.Drawing.SystemColors.ControlDarkDark;
            this.linkLabel.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel_LinkClicked);
            // 
            // lblVersion
            // 
            this.lblVersion.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.lblVersion.BackColor = System.Drawing.SystemColors.ControlLight;
            this.lblVersion.ForeColor = System.Drawing.Color.DeepPink;
            this.lblVersion.Location = new System.Drawing.Point(269, 459);
            this.lblVersion.Name = "lblVersion";
            this.lblVersion.Size = new System.Drawing.Size(240, 13);
            this.lblVersion.TabIndex = 113;
            this.lblVersion.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // LHFForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(690, 476);
            this.Controls.Add(this.lblVersion);
            this.Controls.Add(this.linkLabel);
            this.Controls.Add(this.cbLimit);
            this.Controls.Add(this.cbShowTime);
            this.Controls.Add(this.numericUpDown1);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.rtbFrames);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.checkBox1);
            this.Controls.Add(this.btnSendNext);
            this.Controls.Add(this.btnSource);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.nUdpPort);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.txtUdpAddress);
            this.Controls.Add(this.statusStrip1);
            this.Name = "LHFForm";
            this.Text = "Scytale-C Frame Player";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.LHFForm_FormClosing);
            this.Load += new System.EventHandler(this.LHFForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.nUdpPort)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.NumericUpDown nUdpPort;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtUdpAddress;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Button btnSource;
        private System.Windows.Forms.Button btnSendNext;
        private System.Windows.Forms.CheckBox checkBox1;
        private System.Windows.Forms.NumericUpDown numericUpDown1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.RichTextBox rtbFrames;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.CheckBox cbShowTime;
        private System.Windows.Forms.CheckBox cbLimit;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.Label lblVersion;
        private System.Windows.Forms.LinkLabel linkLabel;
    }
}

