# README #

### What is this project for? ###

* This is a C# PC-based open source decoder for [Inmarsat-C](https://www.inmarsat.com/services/safety/inmarsat-c/).

### Credits ###

This project would not have been possible without the help of:

* Otti, who researched most of the documentation, contributed code, ran tests and more. Otti also came up with the name Scytale-C (https://en.wikipedia.org/wiki/Scytale),

* Jonti, who kindly advised, send and allowed me to use and modify his code, 

* Youssef, who kindly took me through some parts of the SDR# code,

* Holger, who kindly contributed and tested several decoders,

* The Community, ever welcoming and cheerful.

### Installation ###
To get your decoder up and running, you will need some of the binaries from the downloads section.

* For audio devices input, wav file input, audio over tcp or IQ over tcp (as in using a dedicated TcpIp SDRSharp plugin):
    1. Download x64-ScytaleC-(ver).zip and x64-ScytaleC.QuickUI-(ver).zip
    2. Extract in a folder of your choosing (no spaces in path) and run the executable ScytaleC.exe.
    3. To start viewing messages, run the acompanying UI executable, ScytaleC.QuickUI.exe.
* For IQ input using SDRSharp.ScytaleC plugin:
    1. Download and install the x64-SDRSharp.ScytaleC-(ver).zip.
    2. Download and run x64-ScytaleC.QuickUI-(ver).zip  
* For IQ input using GNU Radio:
    1. Download and install either x64_Scytalec.Decoder.UI_(v)1.zip the or x64_Scytalec.Decoder.Cmd_(ver).zip from https://bitbucket.org/scytalec/scytalec.decoder/downloads/. To help with the setup see also http://thinair.aelogic.com/french-guiana-firing-exercise/.
    2. Download and run x64-ScytaleC.QuickUI-(ver).zip

The UI communicates with the decoder through UDP. If your PC does not have a network card, please install and configure a virtual network interface.
    
### Note ###

This work is a learning experience. Resources and CPU consumption were second to code understandability. Feel free to use
this code as a base for your own development. If you have a question, if you find a bug, please contact me or preferably 
use the issue tracker associated with this repository. The bibliography is extensive and I am sure there is a lot more to
be added. If you find something interesting, please let me know.

There are several repositories within the ScytaleC project: https://bitbucket.org/scytalec/. They are all open source. Feel free to browse, improve or suggest improvements. 

### Issue tracker ###

https://bitbucket.org/scytalec/scytalec/issues/

### Who do I talk to? ###

* Repo owner or admin: microp11 at aelogic dot com
* Other community or team contact.

### I would like to help ###

Excellent initiative! Here are a few areas that could always use an extra hand:

* Wiki - Perhaps add a tutorial of how to use the application, and then update it when necessary.
* Packet decoding - There are always new packets to be decoded. There are decoded packets that have unknown parts. The database support implemented since 1.2 allows for easy collection and correlation of data. The ScytaleC.Tester utility that has been provided with the zip file from day one can be used to determine which portions of a packet contain text. Using this utility I was able for example to decode the BD and BE packets.
* Ideas - Bring another point of view, root for a new feature, or even implement one yourself.
* Coding - Write your code and ask for a pull request.

### Donations ###

Scytale-C is an exercise and comes with an intrinsic reward. There is no expectation of monetary gain. We understand however that a donation is also a way to contribute, and if you wish to buy us a beer, you're more than welcome! Up to date, since the release of this project we obtained 197 Canadian dollars and a gift. Thank you.  
[![Donate with PayPal](btn_dn.gif)](https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=RFCSY3EWYXUPU).

### Licensing ###

GNU General Public License 3, microp11 2017
 
Scytale-C is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

Scytale-C is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

See http://www.gnu.org/licenses/.
