﻿/*
 * 
 * http://www.c-sharpcorner.com/article/serialization-and-deserialization-in-c-sharp/
 * 
 * preserve datagridview position after reload
 * https://stackoverflow.com/questions/2442419/how-to-save-position-after-reload-datagridview
 * 
 * Worldwide Met-Ocean Information and Warning Service (WWMIWS)
 * http://weather.gmdss.org/
 * 
 * https://www.red-gate.com/simple-talk/blogs/c-getting-the-directory-of-a-running-executable/
 * https://www.codeproject.com/Questions/61507/How-to-hide-CheckBox-for-particular-cell-in-Datagr
 * https://stackoverflow.com/questions/6265228/selecting-a-row-in-datagridview-programmatically
 * 
 */


using ScytaleC.Interfaces;
using ScytaleC.PacketDecoders;
using ScytaleC.QuickUI.Properties;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Windows.Forms;

namespace ScytaleC.QuickUI
{
    public partial class MainForm : Form
    {
        private List<UdpReceiver> udprL;
        private IPacketDetector packetDetector;
        private ConcurrentQueue<string> udpQ;
        private bool udpQBusy;
        private Db database;
        private GMapper uniGMapper;
        private XmlNavAreas navAreas;

        private List<Uni> unis;
        private int SelectedMIdOrLCNo;
        private bool rowUniSelectedByKeyPress;

        private Color darkBackColor = SystemColors.Control;
        private Color controlTextColor = SystemColors.ControlDark;
        private Color controlTextLiteColor;
        private Color lightBackColor = SystemColors.Control;
        private Color generalBackgroundForeColor = SystemColors.ControlDark;

        /// <summary>
        /// Main form initializer.
        /// </summary>
        public MainForm()
        {
            InitializeComponent();

            cb6C.Checked = Settings.Default.cb6C;
            cb7D.Checked = Settings.Default.cb7D;
            cb92.Checked = Settings.Default.cb92;
            cbAA.Checked = Settings.Default.cbAA;
            cbAB.Checked = Settings.Default.cbAB;
            cbB1.Checked = Settings.Default.cbB1;
            cbB2.Checked = Settings.Default.cbB2;
            cbBD.Checked = Settings.Default.cbBD;
            cbBE.Checked = Settings.Default.cbBE;
            cb83.Checked = Settings.Default.cb83;
            cb27.Checked = Settings.Default.cb27;
            cb2A.Checked = Settings.Default.cb2A;
            cb08.Checked = Settings.Default.cb08;
            cb81.Checked = Settings.Default.cb81;
            cbA3.Checked = Settings.Default.cbA3;
            cbA8.Checked = Settings.Default.cbA8;

            txtRawFrameUDPPort.Text = Settings.Default.txtRawFrameUDPPort;
            nMaxDisplayedPackets.Value = Settings.Default.nMaxDisplayedPackets;
            nMaxDisplayedDebugs.Value = Settings.Default.nMaxDisplayedDebugs;
            nMaxStoredHours.Value = Settings.Default.nMaxStoredHours;
            cbUseLocalDbStore.Checked = Settings.Default.cbUseLocalDbStore;
            cbSavePackets.Checked = Settings.Default.cbSavePackets;
            cbDarkTheme.Checked = Settings.Default.cbDarkTheme;
            nBkgR.Value = Settings.Default.nBkgR;
            nBkgG.Value = Settings.Default.nBkgG;
            nBkgB.Value = Settings.Default.nBkgB;
            nBkg2.Value = Settings.Default.nBkg2;
            nTextR.Value = Settings.Default.nTextR;
            nTextG.Value = Settings.Default.nTextG;
            nTextB.Value = Settings.Default.nTextB;
            nText2.Value = Settings.Default.nText2;
            nChartDarkening.Value = Settings.Default.nChartDarkening;
            uniMsgBox.Font = Settings.Default.uFont;
            txtFont.Text = string.Format("{0}, {1:0.##}", uniMsgBox.Font.Name, uniMsgBox.Font.SizeInPoints);
            cbLogMessages.Checked = Settings.Default.cbLogMessages;
            txtLogFolder.Text = Settings.Default.txtLogFolder;
            folderBrowserDialog1.SelectedPath = Settings.Default.txtLogFolder;
            cbLogRaw.Checked = Settings.Default.cbLogRaw;

            packetDetector = new PacketDetector();
            packetDetector.OnPacket += PacketDetector_OnPacket;

            udpQ = new ConcurrentQueue<string>();
            udpQBusy = false;

            udprL = new List<UdpReceiver>();

            lbl0.Tag = 0;
            lbl1.Tag = 0;
            lbl2.Tag = 0;
            lbl3.Tag = 0;
            lbl4.Tag = 0;
            lbl5.Tag = 0;
            lbl6.Tag = 0;
            lbl7.Tag = 0;
            lbl8.Tag = 0;
            lbl9.Tag = 0;

            Uri location = new Uri(Assembly.GetEntryAssembly().GetName().CodeBase);
            string exePath = new FileInfo(location.AbsolutePath).Directory.FullName;

            try
            {
                database = new Db(exePath);

                database.OnUniUpdated += Database_OnUniUpdated;
                database.OnUniDeleted += Database_OnUniDeleted;
                unis = new List<Uni>();
                unisBindingSource.DataSource = unis;
            }
            catch (Exception ex)
            {
                Utils.Log.Error(ex);
            }

            try
            {
                // instantiate a GMapper object passing the gmap windows forms control
                // we are also passing the label that is dipslyed by the map element as sometimes the
                // control does not seem to initialize properly.
                // this way we can pass the exception to the user so it can be 
                // reported back
                uniGMapper = new GMapper(uniGMap);

                // load the nav areas from Xml
                navAreas = new XmlNavAreas(exePath);
                navAreas.LoadNavAreasFromXml();
            }
            catch (Exception ex)
            {
                Utils.Log.Error(ex);
            }

            FileLogger.DefaultPath = txtLogFolder.Text;

            //nothing selected
            SelectedMIdOrLCNo = -1;

            //indicates that the user used the keyboard to select a new row in the grid
            rowUniSelectedByKeyPress = false;

            uniMsgBox.AddContextMenu(uniGMapper);
            uniGMap.AddContextMenu(uniGMapper);
        }

        /// <summary>
        /// Notifies the data binding source that a particular EGC has been removed from the database and updates the grid.
        /// </summary>
        private void Database_OnUniDeleted(object sender, UniDeletedArgs e)
        {
            BeginInvoke(new Action(() =>
            {
                // inform grid about removed record
                Uni uni = unis.FirstOrDefault(x => x.UniId == e.UniId);

                if (uni == null)
                {
                    return;
                }

                int idx = unis.IndexOf(uni);
                unisBindingSource.RemoveAt(idx);

                #region preserve grid position between updates

                if (!btnUniAutoScroll.Checked)
                {
                    int saveRow = 0;
                    if (dgvUni.Rows.Count > 0)
                        saveRow = dgvUni.FirstDisplayedCell.RowIndex;

                    unisBindingSource.ResetBindings(false);

                    if (saveRow != 0 && saveRow < dgvUni.Rows.Count)
                        dgvUni.FirstDisplayedScrollingRowIndex = saveRow;
                }
                else
                {
                    //scroll to bottom
                    unisBindingSource.ResetBindings(false);

                    if (dgvUni.Rows[dgvUni.Rows.Count - 1].Selected != true)
                    {

                        dgvUni.FirstDisplayedScrollingRowIndex = dgvUni.Rows.Count - 1;

                        //select last line
                        if (btnUniAutoLastSelect.Checked)
                        {
                            dgvUni.Rows[dgvUni.Rows.Count - 1].Selected = true;
                        }
                    }
                }

                #endregion

                //force update screen on cursor
                dgvUni_CellClick(sender, new DataGridViewCellEventArgs(-1, dgvUni.SelectedRows[0].Index));
            }));
        }

        /// <summary>
        /// Notifies the data binding source that a particular Uni has been updated in the database, and updates the grid.
        /// </summary>
        private void Database_OnUniUpdated(object sender, UniUpdatedArgs e)
        {
            BeginInvoke(new Action(() =>
            {
                // inform grid about updated record
                Uni uni = unis.FirstOrDefault(x => x.UniId == e.Uni_.UniId);

                if (uni == null)
                {
                    //the element does not exist in the grid, we add it
                    unis.Add(e.Uni_);
                }
                else
                {
                    //update existing
                    int idx = unis.IndexOf(uni);
                    unis[idx] = e.Uni_;
                }

                #region preserve grid position between updates
                if (!btnUniAutoScroll.Checked)
                {
                    int saveRow = 0;
                    if (dgvUni.Rows.Count > 0)
                        saveRow = dgvUni.FirstDisplayedCell.RowIndex;

                    unisBindingSource.ResetBindings(false);

                    if (saveRow != 0 && saveRow < dgvUni.Rows.Count)
                        dgvUni.FirstDisplayedScrollingRowIndex = saveRow;
                }
                else
                {
                    //scroll to bottom
                    unisBindingSource.ResetBindings(false);

                    if (dgvUni.Rows[dgvUni.Rows.Count - 1].Selected != true)
                    {

                        dgvUni.FirstDisplayedScrollingRowIndex = dgvUni.Rows.Count - 1;

                        //select last line
                        if (btnUniAutoLastSelect.Checked)
                        {
                            dgvUni.Rows[dgvUni.Rows.Count - 1].Selected = true;
                        }
                    }
                }
                #endregion

                //force update screen on cursor
                dgvUni_CellClick(sender, new DataGridViewCellEventArgs(-1, dgvUni.SelectedRows[0].Index));
            }));
        }

        /// <summary>
        /// Main dispatcher of the decoded packets.
        /// It checks if too many streams are received and only allows 5 to be processed.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void PacketDetector_OnPacket(object sender, PacketArgs e)
        {
            long streamId = e.StreamId;

            BeginInvoke(new Action(() =>
            {
                if (cbUseLocalDbStore.Checked)
                {
                    database.AddAnyPacket(e);
                }
                else
                {
                    //add only the ones we need for EGC and Messages
                    int packetDescriptor = e.PacketDescriptor;
                    switch (packetDescriptor)
                    {
                        case 0xB1:
                        case 0xB2:
                        case 0xAA:
                        case 0x7D:
                            database.AddAnyPacket(e);
                            break;
                    }
                }

                try
                {
                    bool pageAttachedToStreamId = false;
                    if ((int)lbl0.Tag == 0 || (int)lbl0.Tag == (int)streamId)
                    {
                        if ((int)lbl0.Tag == 0)
                        {
                            lbl0.Tag = (int)streamId;
                        }
                        pageAttachedToStreamId = true;
                    }
                    else if ((int)lbl1.Tag == 0 || (int)lbl1.Tag == (int)streamId)
                    {
                        if ((int)lbl1.Tag == 0)
                        {
                            lbl1.Tag = (int)streamId;
                        }
                        pageAttachedToStreamId = true;
                    }
                    else if ((int)lbl2.Tag == 0 || (int)lbl2.Tag == (int)streamId)
                    {
                        if ((int)lbl2.Tag == 0)
                        {
                            lbl2.Tag = (int)streamId;
                        }
                        pageAttachedToStreamId = true;
                    }
                    else if ((int)lbl3.Tag == 0 || (int)lbl3.Tag == (int)streamId)
                    {
                        if ((int)lbl3.Tag == 0)
                        {
                            lbl3.Tag = (int)streamId;
                        }
                        pageAttachedToStreamId = true;
                    }
                    else if ((int)lbl4.Tag == 0 || (int)lbl4.Tag == (int)streamId)
                    {
                        if ((int)lbl4.Tag == 0)
                        {
                            lbl4.Tag = (int)streamId;
                        }
                        pageAttachedToStreamId = true;
                    }
                    else if ((int)lbl5.Tag == 0 || (int)lbl5.Tag == (int)streamId)
                    {
                        if ((int)lbl5.Tag == 0)
                        {
                            lbl5.Tag = (int)streamId;
                        }
                        pageAttachedToStreamId = true;
                    }
                    else if ((int)lbl6.Tag == 0 || (int)lbl6.Tag == (int)streamId)
                    {
                        if ((int)lbl6.Tag == 0)
                        {
                            lbl6.Tag = (int)streamId;
                        }
                        pageAttachedToStreamId = true;
                    }
                    else if ((int)lbl7.Tag == 0 || (int)lbl7.Tag == (int)streamId)
                    {
                        if ((int)lbl7.Tag == 0)
                        {
                            lbl7.Tag = (int)streamId;
                        }
                        pageAttachedToStreamId = true;
                    }
                    else if ((int)lbl8.Tag == 0 || (int)lbl8.Tag == (int)streamId)
                    {
                        if ((int)lbl8.Tag == 0)
                        {
                            lbl8.Tag = (int)streamId;
                        }
                        pageAttachedToStreamId = true;
                    }
                    else if ((int)lbl9.Tag == 0 || (int)lbl9.Tag == (int)streamId)
                    {
                        if ((int)lbl9.Tag == 0)
                        {
                            lbl9.Tag = (int)streamId;
                        }
                        pageAttachedToStreamId = true;
                    }

                    if (!pageAttachedToStreamId)
                    {
                        rtbPackets.AppendText(string.Format("Stream {0} decoded. Not displayed in a streaming lane.", streamId));
                        rtbPackets.AppendText(Environment.NewLine);
                        return;
                    }

                    PresentPackets(this, e);
                }
                catch (Exception ex)
                {
                    Utils.Log.Error(ex);
                }
            }));
        }

        /// <summary>
        /// Every decoded packet gets here. They get dispatched as needed.
        /// </summary>
        private void PresentPackets(MainForm mainForm, PacketArgs e)
        {
            long streamId = e.StreamId;

            switch (e.PacketDescriptor)
            {
                case 0xB1:
                    PacketDecoderB1 pdB1 = new System.Web.Script.Serialization.JavaScriptSerializer().Deserialize<PacketDecoderB1>(e.SerializedData);
                    AppendPacketText(e, pdB1, cbB1.Checked, btnJson.Checked);
                    break;

                case 0xB2:
                    PacketDecoderB2 pdB2 = new System.Web.Script.Serialization.JavaScriptSerializer().Deserialize<PacketDecoderB2>(e.SerializedData);
                    AppendPacketText(e, pdB2, cbB2.Checked, btnJson.Checked);
                    break;

                case 0xBD:
                    PacketDecoderBD pdBD = new System.Web.Script.Serialization.JavaScriptSerializer().Deserialize<PacketDecoderBD>(e.SerializedData);
                    AppendPacketText(e, pdBD, cbBD.Checked, btnJson.Checked);
                    break;

                case 0xBE:
                    PacketDecoderBE pdBE = new System.Web.Script.Serialization.JavaScriptSerializer().Deserialize<PacketDecoderBE>(e.SerializedData);
                    AppendPacketText(e, pdBE, cbBE.Checked, btnJson.Checked);
                    break;

                case 0x92:
                    PacketDecoder92 pd92 = new System.Web.Script.Serialization.JavaScriptSerializer().Deserialize<PacketDecoder92>(e.SerializedData);
                    AppendPacketText(e, pd92, cb92.Checked, btnJson.Checked);
                    break;

                case 0xAA:
                    PacketDecoderAA pdAA = new System.Web.Script.Serialization.JavaScriptSerializer().Deserialize<PacketDecoderAA>(e.SerializedData);
                    AppendPacketText(e, pdAA, cbAA.Checked, btnJson.Checked);
                    break;

                case 0xAB:
                    PacketDecoderAB pdAB = new System.Web.Script.Serialization.JavaScriptSerializer().Deserialize<PacketDecoderAB>(e.SerializedData);
                    AppendPacketText(e, pdAB, cbAB.Checked, btnJson.Checked);
                    break;

                case 0x6C:
                    PacketDecoder6C pd6C = new System.Web.Script.Serialization.JavaScriptSerializer().Deserialize<PacketDecoder6C>(e.SerializedData);
                    AppendPacketText(e, pd6C, cb6C.Checked, btnJson.Checked);
                    break;

                case 0x7D:
                    PacketDecoder7D pd7D = new System.Web.Script.Serialization.JavaScriptSerializer().Deserialize<PacketDecoder7D>(e.SerializedData);
                    SetBroadcastTextAndLabel(streamId, pd7D);
                    AppendPacketText(e, pd7D, cb7D.Checked, btnJson.Checked);
                    break;

                case 0x83:
                    PacketDecoder83 pd83 = new System.Web.Script.Serialization.JavaScriptSerializer().Deserialize<PacketDecoder83>(e.SerializedData);
                    AppendPacketText(e, pd83, cb83.Checked, btnJson.Checked);
                    break;

                case 0x27:
                    PacketDecoder27 pd27 = new System.Web.Script.Serialization.JavaScriptSerializer().Deserialize<PacketDecoder27>(e.SerializedData);
                    AppendPacketText(e, pd27, cb27.Checked, btnJson.Checked);
                    break;

                case 0x2A:
                    PacketDecoder2A pd2A = new System.Web.Script.Serialization.JavaScriptSerializer().Deserialize<PacketDecoder2A>(e.SerializedData);
                    AppendPacketText(e, pd2A, cb2A.Checked, btnJson.Checked);
                    break;

                case 0x08:
                    PacketDecoder08 pd08 = new System.Web.Script.Serialization.JavaScriptSerializer().Deserialize<PacketDecoder08>(e.SerializedData);
                    AppendPacketText(e, pd08, cb08.Checked, btnJson.Checked);
                    break;

                case 0x81:
                    PacketDecoder81 pd81 = new System.Web.Script.Serialization.JavaScriptSerializer().Deserialize<PacketDecoder81>(e.SerializedData);
                    AppendPacketText(e, pd81, cb81.Checked, btnJson.Checked);
                    break;

                case 0xA3:
                    PacketDecoderA3 pdA3 = new System.Web.Script.Serialization.JavaScriptSerializer().Deserialize<PacketDecoderA3>(e.SerializedData);
                    AppendPacketText(e, pdA3, cbA3.Checked, btnJson.Checked);
                    break;

                case 0xA8:
                    PacketDecoderA8 pdA8 = new System.Web.Script.Serialization.JavaScriptSerializer().Deserialize<PacketDecoderA8>(e.SerializedData);
                    AppendPacketText(e, pdA8, cbA8.Checked, btnJson.Checked);
                    break;

                default:
                    string sl = e.StreamId.ToString();
                    string txt = string.Format("*{0,5} {1,4:####} {2:X2} {3}", sl.Substring(sl.Length - 5), e.OriginalFrameArgs.FrameNumber, e.PacketDescriptor, e.HexSequence);
                    AppendPacketText(txt, DecodingStage.None, true);
                    break;
            }
        }
        /// <summary>
        /// Sets a broadcast box title for each received stream.
        /// </summary>
        private void SetBroadcastTextAndLabel(long streamId, PacketDecoder7D pd7D)
        {
            try
            {
                if (pd7D.FrameNumber == -999)
                {
                    return;
                }
                string text = string.Format("{0} {1}", pd7D.ChannelTypeName, pd7D.LesId);
                string sl = streamId.ToString();
                string label = string.Format("{0}, {1}   (*{2,5})", pd7D.LesName, pd7D.SatName, sl.Substring(sl.Length - 5));
                SetBroadcastPageTextAndLabel(streamId, text, label);
            }
            catch (Exception ex)
            {
                Utils.Log.Error(ex);
            }
        }

        /// <summary>
        /// Allocates a broadcast page for each received stream.
        /// </summary>
        private void SetBroadcastPageTextAndLabel(long streamId, string text, string label)
        {
            string txt = string.Format("{0}:", text);
            if ((int)lbl0.Tag == (int)streamId)
            {
                if (lbl0a.Text != txt)
                {
                    lbl0.Text = "";
                    lbl0a.Text = txt;
                }
            }
            else if ((int)lbl1.Tag == (int)streamId)
            {
                lbl1a.Text = string.Format("{0}:", text);
            }
            else if ((int)lbl2.Tag == (int)streamId)
            {
                lbl2a.Text = string.Format("{0}:", text);
            }
            else if ((int)lbl3.Tag == (int)streamId)
            {
                lbl3a.Text = string.Format("{0}:", text);
            }
            else if ((int)lbl4.Tag == (int)streamId)
            {
                lbl4a.Text = string.Format("{0}:", text);
            }
            else if ((int)lbl5.Tag == (int)streamId)
            {
                lbl5a.Text = string.Format("{0}:", text);
            }
            else if ((int)lbl6.Tag == (int)streamId)
            {
                lbl6a.Text = string.Format("{0}:", text);
            }
            else if ((int)lbl7.Tag == (int)streamId)
            {
                lbl7a.Text = string.Format("{0}:", text);
            }
            else if ((int)lbl8.Tag == (int)streamId)
            {
                lbl8a.Text = string.Format("{0}:", text);
            }
            else if ((int)lbl9.Tag == (int)streamId)
            {
                lbl9a.Text = string.Format("{0}:", text);
            }
        }

        /// <summary>
        /// Controls the displaying of the output based on user settings.
        /// </summary>
        private void AppendPacketText(PacketArgs e, PacketDecoder pd, bool debug, bool json)
        {
            long streamId = e.StreamId;

            int decodingStage = pd.DecodingStage_;
            bool isCRC = pd.IsCRC;
            string sl = e.StreamId.ToString();
            AppendPacketText(string.Format("*{0,5} {1,4:####} {2:X2} {3}", sl.Substring(sl.Length - 5), e.OriginalFrameArgs.FrameNumber, e.PacketDescriptor, e.HexSequence), decodingStage, isCRC);

            if (debug)
            {
                switch (e.PacketDescriptor)
                {
                    case 0x6C:
                    case 0x92:
                    case 0xAA:
                    case 0xAB:
                    case 0xB1:
                    case 0xB2:
                    case 0xBD:
                    case 0xBE:
                    case 0x83:
                    case 0x27:
                    case 0x2A:
                    case 0x08:
                    case 0x81:
                    case 0xA3:
                    case 0xA8:
                        if (json)
                        {
                            AppendDebugText(new JsonFormatter(e.SerializedData).Format());
                        }
                        else
                        {
                            AppendDebugText(e.SerializedData);
                        }
                        break;

                    case 0x7D:
                        if (json)
                        {
                            AppendDebugText(new JsonFormatter(e.SerializedData).Format());
                        }
                        else
                        {
                            AppendDebugText(e.SerializedData);
                        }

                        SetStreamLabel(streamId, e.OriginalFrameArgs.FrameNumber);
                        break;

                    default:
                        AppendPacketText(new JsonFormatter(e.SerializedData).Format(), decodingStage, isCRC);
                        break;
                }
            }
        }

        /// <summary>
        /// Display the debug strings received as a Json formatted string.
        /// Limits the number of output lines so it keeps the application responsive if left running for a long time.
        /// </summary>
        private void AppendDebugText(string txt)
        {
            rtbDebug.AppendText(txt);

            #region RestrictNumberOfDebugLinesDisplayed

            int maxDebugLinesOnDisplay = (int)nMaxDisplayedDebugs.Value;
            if (rtbDebug.Lines.Length > maxDebugLinesOnDisplay)
            {
                rtbDebug.SelectionStart = rtbDebug.GetFirstCharIndexFromLine(0);
                rtbDebug.SelectionLength = rtbDebug.Lines[rtbDebug.Lines.Length - maxDebugLinesOnDisplay].Length + 100;
                rtbDebug.SelectedText = String.Empty;
            }

            #endregion

            rtbDebug.AppendText(Environment.NewLine);

            ////scroll to last
            if (btnDebugAutoScroll.Checked)
            {
                rtbDebug.ScrollToCaret();
            }
        }

        /// <summary>
        /// Displays the received packet numbers in their own running label marquee.
        /// </summary>
        private void SetStreamLabel(long streamId, int frameNumber)
        {
            int maxTextLength = 1000;

            if ((int)lbl0.Tag == (int)streamId)
            {
                if (lbl0.Text.Length > maxTextLength)
                {
                    lbl0.Text = lbl0.Text.Substring(0, maxTextLength);
                }
                lbl0.Text = string.Format("{0}, {1}", frameNumber, lbl0.Text);
            }
            else if ((int)lbl1.Tag == (int)streamId)
            {
                if (lbl1.Text.Length > maxTextLength)
                {
                    lbl1.Text = lbl1.Text.Substring(0, maxTextLength);
                }
                lbl1.Text = string.Format("{0}, {1}", frameNumber, lbl1.Text);
            }
            else if ((int)lbl2.Tag == (int)streamId)
            {
                if (lbl2.Text.Length > maxTextLength)
                {
                    lbl2.Text = lbl2.Text.Substring(0, maxTextLength);
                }
                lbl2.Text = string.Format("{0}, {1}", frameNumber, lbl2.Text);
            }
            else if ((int)lbl3.Tag == (int)streamId)
            {
                if (lbl3.Text.Length > maxTextLength)
                {
                    lbl3.Text = lbl3.Text.Substring(0, maxTextLength);
                }
                lbl3.Text = string.Format("{0}, {1}", frameNumber, lbl3.Text);
            }
            else if ((int)lbl4.Tag == (int)streamId)
            {
                if (lbl4.Text.Length > maxTextLength)
                {
                    lbl4.Text = lbl4.Text.Substring(0, maxTextLength);
                }
                lbl4.Text = string.Format("{0}, {1}", frameNumber, lbl4.Text);
            }
            else if ((int)lbl5.Tag == (int)streamId)
            {
                if (lbl5.Text.Length > maxTextLength)
                {
                    lbl5.Text = lbl5.Text.Substring(0, maxTextLength);
                }
                lbl5.Text = string.Format("{0}, {1}", frameNumber, lbl5.Text);
            }
            else if ((int)lbl6.Tag == (int)streamId)
            {
                if (lbl6.Text.Length > maxTextLength)
                {
                    lbl6.Text = lbl6.Text.Substring(0, maxTextLength);
                }
                lbl6.Text = string.Format("{0}, {1}", frameNumber, lbl6.Text);
            }
            else if ((int)lbl7.Tag == (int)streamId)
            {
                if (lbl7.Text.Length > maxTextLength)
                {
                    lbl7.Text = lbl7.Text.Substring(0, maxTextLength);
                }
                lbl7.Text = string.Format("{0}, {1}", frameNumber, lbl7.Text);
            }
            else if ((int)lbl8.Tag == (int)streamId)
            {
                if (lbl8.Text.Length > maxTextLength)
                {
                    lbl8.Text = lbl8.Text.Substring(0, maxTextLength);
                }
                lbl8.Text = string.Format("{0}, {1}", frameNumber, lbl8.Text);
            }
            else if ((int)lbl9.Tag == (int)streamId)
            {
                if (lbl9.Text.Length > maxTextLength)
                {
                    lbl9.Text = lbl9.Text.Substring(0, maxTextLength);
                }
                lbl9.Text = string.Format("{0}, {1}", frameNumber, lbl9.Text);
            }
        }

        /// <summary>
        /// Display the packets received as a hex string. Uses colouring.
        /// Limits the number of output lines so it keeps the application responsive if left running for a long time.
        /// </summary>
        private void AppendPacketText(string txt, int decodingStg, bool isCRC)
        {
            if (decodingStg == DecodingStage.None)
            {
                rtbPackets.SelectionColor = Color.BurlyWood;
            }
            else if (decodingStg == DecodingStage.Partial)
            {
                rtbPackets.SelectionColor = Color.Blue;
            }
            if (!isCRC)
            {
                rtbPackets.SelectionColor = Color.Red;
            }
            rtbPackets.AppendText(txt);
            rtbPackets.SelectionColor = Color.Black;

            #region RestrictNumberOfPacketsDisplayed

            int maxPacketLinesOnDisplay = (int)nMaxDisplayedPackets.Value;
            if (rtbPackets.Lines.Length > maxPacketLinesOnDisplay)
            {
                rtbPackets.SelectionStart = rtbPackets.GetFirstCharIndexFromLine(0);
                rtbPackets.SelectionLength = rtbPackets.Lines[rtbPackets.Lines.Length - maxPacketLinesOnDisplay].Length + 100;
                rtbPackets.SelectedText = String.Empty;
            }

            #endregion

            rtbPackets.AppendText(Environment.NewLine);

            ////scroll to last
            if (btnPacketsAutoScroll.Checked)
            {
                rtbPackets.ScrollToCaret();
            }
        }

        /// <summary>
        /// Toggles the left panel menu on/off.
        /// </summary>
        private void btnMenu_Click(object sender, EventArgs e)
        {
            if ((string)btnMenu.Tag == "menu-close")
            {
                SetButtons(2);
            }
            else
            {
                SetButtons(3);
            }

            panelMenu.Visible = !panelMenu.Visible;
        }

        /// <summary>
        /// Creates a state machine of the user actions.
        /// </summary>
        private void SetButtons(int action)
        {
            switch (action)
            {
                case 0:
                    btnStart.Image = Resources.button_blue_stop;
                    EnableDisableControls(false);
                    btnStart.Tag = "stop";
                    break;

                case 1:
                    btnStart.Image = Resources.button_blue_play;
                    EnableDisableControls(true);
                    btnStart.Tag = "start";
                    break;

                case 2:
                    btnMenu.Image = Resources.exchange;
                    btnMenu.Tag = "menu-open";
                    break;

                case 3:
                    btnMenu.Image = Resources.exchange_back;
                    btnMenu.Tag = "menu-close";
                    break;
            }
        }

        /// <summary>
        /// Changes the aspect of the UI elements based on user actions.
        /// </summary>
        private void EnableDisableControls(bool enabled)
        {
            txtRawFrameUDPPort.ReadOnly = !enabled;
        }

        /// <summary>
        /// User triggered. Starts the listening to UDP packets.
        /// </summary>
        private void btnStart_Click(object sender, EventArgs e)
        {
            try
            {
                if ((string)btnStart.Tag == "start")
                {
                    Start();
                }
                else
                {
                    Stop();
                }
            }
            catch (Exception ex)
            {
                Utils.Log.Error(ex);
            }
        }

        /// <summary>
        /// Stops the listening for UDP packets.
        /// </summary>
        private void Stop()
        {
            SetButtons(1);

            try
            {
                foreach (UdpReceiver udpr in udprL)
                {
                    udpr.OnUDPPacket -= Udpr_OnUDPPacket;
                    udpr.Dispose();
                }
                udprL.Clear();
            }
            catch( Exception ex)
            {
                Utils.Log.Error(ex);
            }

            while (udpQ.Count > 0)
            {
                udpQ.TryDequeue(out string line);
            }

            database.Flush();
        }

        /// <summary>
        /// Starts the listening for UDP packets.
        /// </summary>
        private void Start()
        {
            SetButtons(0);

            //Set network interface used
            try
            {
                string[] ports = txtRawFrameUDPPort.Text.Split(new[] { ',' }, System.StringSplitOptions.RemoveEmptyEntries);

                foreach (string port in ports)
                {
                    UdpReceiver udpr = new UdpReceiver(Convert.ToInt32(port));
                    udpr.OnUDPPacket += Udpr_OnUDPPacket;
                    udpr.Start();
                    udprL.Add(udpr);
                }
            }
            catch (Exception ex)
            {
                Utils.Log.Error(ex);
                Stop();
            }
        }

        /// <summary>
        /// Triggered with any UPD packet received.
        /// What we have received is a hex string.
        /// By queueing the udp calls, we avoid reentrancy when using multiple providers.
        /// </summary>
        private void Udpr_OnUDPPacket(object sender, Interfaces.UDPPacketArgs e)
        {
            udpQ.Enqueue(Encoding.ASCII.GetString(e.UDPPacket));
        }

        /// <summary>
        /// Save user preferences on exit.
        /// Triggered on application close.
        /// </summary>
        private void MainForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            Settings.Default.txtRawFrameUDPPort = txtRawFrameUDPPort.Text;
            Settings.Default.nMaxDisplayedPackets = nMaxDisplayedPackets.Value;
            Settings.Default.nMaxDisplayedDebugs = nMaxDisplayedDebugs.Value;
            Settings.Default.nMaxStoredHours = nMaxStoredHours.Value;
            Settings.Default.cbUseLocalDbStore = cbUseLocalDbStore.Checked;
            Settings.Default.cbSavePackets = cbSavePackets.Checked;
            Settings.Default.cbDarkTheme = cbDarkTheme.Checked;
            Settings.Default.nBkgR = nBkgR.Value;
            Settings.Default.nBkgG = nBkgG.Value;
            Settings.Default.nBkgB = nBkgB.Value;
            Settings.Default.nTextR = nTextR.Value;
            Settings.Default.nTextG = nTextG.Value;
            Settings.Default.nTextB = nTextB.Value;
            Settings.Default.nChartDarkening = nChartDarkening.Value;
            Settings.Default.uFont = uniMsgBox.Font;

            Settings.Default.cb6C = cb6C.Checked;
            Settings.Default.cb7D = cb7D.Checked;
            Settings.Default.cb92 = cb92.Checked;
            Settings.Default.cbAA = cbAA.Checked;
            Settings.Default.cbAB = cbAB.Checked;
            Settings.Default.cbB1 = cbB1.Checked;
            Settings.Default.cbB2 = cbB2.Checked;
            Settings.Default.cbBD = cbBD.Checked;
            Settings.Default.cbBE = cbBE.Checked;
            Settings.Default.cb83 = cb83.Checked;
            Settings.Default.cb27 = cb27.Checked;
            Settings.Default.cb2A = cb2A.Checked;
            Settings.Default.cb08 = cb08.Checked;
            Settings.Default.cb81 = cb81.Checked;
            Settings.Default.cbA3 = cbA3.Checked;
            Settings.Default.cbA8 = cbA8.Checked;

            Settings.Default.cbLogMessages = cbLogMessages.Checked;
            Settings.Default.txtLogFolder = txtLogFolder.Text;
            Settings.Default.cbLogRaw = cbLogRaw.Checked;

            Settings.Default.Save();
        }

        /// <summary>
        /// Display the Credits window.
        /// </summary>
        private void btnCredits_Click(object sender, EventArgs e)
        {
            try
            {
                About frm = new About();
                frm.Show();
            }
            catch(Exception ex)
            {
                Utils.Log.Error(ex);
            }
        }

        /// <summary>
        /// Toggling of packet decoding. A packets is being decoded only if it in the checked state.
        /// TO BE REMOVED
        /// </summary>
        private void toolStripButton_Click(object sender, EventArgs e)
        {
            (sender as ToolStripButton).Checked = !(sender as ToolStripButton).Checked;
        }

        /// <summary>
        /// Copies the Debug Box content to the Clipboard. Text only, no formatting.
        /// </summary>
        private void btnDebugCopy_Click(object sender, EventArgs e)
        {
            StringBuilder sb = new StringBuilder();

            foreach (string line in rtbDebug.Lines)
                sb.AppendLine(line);

            if (sb.Length > 0)
            {
                Clipboard.SetText(sb.ToString());
            }
        }

        /// <summary>
        /// Clears the respective Packets box, based on sender.
        /// </summary>
        private void btnClear_Click(object sender, EventArgs e)
        {
            if (sender == btnPacketsClear)
            {
                rtbPackets.Clear();
            }
            else if (sender == btnDebugClear)
            {
                rtbDebug.Clear();
            }
        }

        /// <summary>
        /// Copies the packets to the clipboard. Text only, no formatting.
        /// </summary>
        private void btnPacketsCopy_Click(object sender, EventArgs e)
        {
            StringBuilder sb = new StringBuilder();

            foreach (string line in rtbPackets.Lines)
                sb.AppendLine(line);

            if (sb.Length > 0)
            {
                Clipboard.SetText(sb.ToString());
            }
        }

        /// <summary>
        /// Consumes a frame at every tick. By using a queue we can have multiple UDP streams feeding the same UI.
        /// This is one of the cheapest solutions for multi-input.
        /// </summary>
        private void udpConsumerTimer_Tick(object sender, EventArgs e)
        {
            if (udpQBusy)
            {
                return;
            }

            udpQBusy = true;

            try
            {
                if (udpQ.TryDequeue(out string receivedText))
                {
                    Invoke(new Action(() =>
                    {
                        //rtbPackets.AppendText(Environment.NewLine);
                        long streamId = 0;

                        // Convert hex string to byte array
                        // We expect 640 bytes as DescrambledFrameArgs and another 8 bytes as the StreamId
                        byte[] receivedBytes = Utils.HexStringToBytes(receivedText);
                        if (receivedBytes.Length < DataConsts.DescrambledFrameLength)
                        {
                            udpQBusy = false;
                            return;
                        }
                        //receiving packets from a decoder that does not pass the full DateTime (1.1Beta release)
                        else if (receivedBytes.Length == DataConsts.DescrambledFrameLength + 4)
                        {
                            streamId = BitConverter.ToInt32(receivedBytes, DataConsts.DescrambledFrameLength);
                        }
                        else if (receivedBytes.Length == DataConsts.DescrambledFrameLength + 8)
                        {
                            streamId = BitConverter.ToInt64(receivedBytes, DataConsts.DescrambledFrameLength);
                        }

                        DescrambledFrameArgs dfa = new DescrambledFrameArgs
                        {
                            DescrambledFrame = new byte[DataConsts.DescrambledFrameLength]
                        };
                        Array.Copy(receivedBytes, dfa.DescrambledFrame, DataConsts.DescrambledFrameLength);
                        dfa.Length = DataConsts.DescrambledFrameLength;

                        if (cbSavePackets.Checked) Utils.LogFrames.Debug("{0:D16} {1}", streamId, Utils.BytesToHexString(dfa.DescrambledFrame, 0, dfa.Length));

                        packetDetector.Process(dfa, streamId);
                    }));
                }
            }
            catch (Exception ex)
            {
                Utils.Log.Error(ex);
            }
            udpQBusy = false;
        }

        /// <summary>
        /// Opens the Disclaimer dialog when the app is opened for the first time, honouring the Don't Show Again option.
        /// </summary>
        private void MainForm_Load(object sender, EventArgs e)
        {
            lblVersion.Text = string.Format("v.{0}", FileVersionInfo.GetVersionInfo(Assembly.GetExecutingAssembly‌​().Location).Product‌​Version);
            try
            {
                rtbNotes.LoadFile(AppDomain.CurrentDomain.BaseDirectory + "notesUI.rtf", RichTextBoxStreamType.PlainText);
            }
            catch (Exception ex)
            {
                rtbNotes.Text = ex.Message;
            }

            uniGMapper.Initialize();
            BeginInvoke(new Action(() =>
            {
                LoadOnceGridFromDb();
            }));

            if (Settings.Default.cbDontShowAgain)
            {
                return;
            }

            BeginInvoke(new Action(() =>
            {
                Disclaimer disclaimer = new Disclaimer();
                disclaimer.FormClosed += Disclaimer_FormClosed;
                disclaimer.ShowDialog(this);
            }));
        }

        /// <summary>
        /// Gets triggered when the Disclaimer dialog is closed.
        /// If the user accepted the disclaimer, we load the EGC grid with data from the previous session.
        /// </summary>
        private void Disclaimer_FormClosed(object sender, FormClosedEventArgs e)
        {
            BeginInvoke(new Action(() =>
            {
                if (((Disclaimer)sender).BtnNoPressed)
                {
                    Close();
                }
                else
                {
                    Settings.Default.cbDontShowAgain = ((Disclaimer)sender).DontShowAgainChecked;
                }
            }));
        }

        /// <summary>
        /// Called one single time to load the grid with any existing data from a previous session.
        /// </summary>
        private void LoadOnceGridFromDb()
        {
            //Unis
            try
            {
                List<Uni> newUnis = new List<Uni>();
                database.LoadUnis(ref newUnis);

                if (unis.Count == 0)
                {
                    unis.AddRange(newUnis);
                }
                else
                {
                    foreach (Uni newUni in newUnis)
                    {
                        Uni element = unis.FirstOrDefault(x => x.UniId == newUni.UniId);
                        if (element != null)
                        {
                            //we have found the item and we update it
                            int idx = unis.IndexOf(element);
                            unis[idx] = newUni;
                        }
                        else if (element == null)
                        {
                            //we did not find this item and we add it to the existing list
                            unis.Add(newUni);
                        }
                    }
                }

                unisBindingSource.ResetBindings(false);

                //select the first row by default
                if (unis.Count > 0 && SelectedMIdOrLCNo == -1)
                {
                    dgvUni_CellClick(this, new DataGridViewCellEventArgs(-1, 0));
                }
            }
            catch (Exception ex)
            {
                Utils.Log.Error(ex);
            }
        }

        /// <summary>
        /// Get fancy names for BB statuses.
        /// </summary>
        /// <param name="status_"></param>
        private void SetStatus(PacketDecoderUtils.Status status_, Label label)
        {
            StringBuilder sb = new StringBuilder();
            string s = status_.Bauds600 ? "Bauds 600, " : "";
            sb.Append(s);
            s = status_.Operational ? "Operational, " : "";
            sb.Append(s);
            s = status_.InService ? "In Service, " : "";
            sb.Append(s);
            s = status_.Clear ? "Clear, " : "";
            sb.Append(s);
            s = status_.LinksOpen ? "Links Open, " : "";
            sb.Append(s);

            label.Text = sb.ToString().Remove(sb.Length - 2);
        }

        /// <summary>
        /// Get fancy names for BB services.
        /// </summary>
        private void SetServices(PacketDecoderUtils.Services services_, Label label)
        {
            StringBuilder sb = new StringBuilder();
            string s = services_.MaritimeDistressAlerting ? "Maritime Distress Alerting, " : "";
            sb.Append(s);
            s = services_.SafetyNet ? "SafetyNet, " : "";
            sb.Append(s);
            s = services_.InmarsatC ? "Inmarsat-C, " : "";
            sb.Append(s);
            s = services_.StoreFwd ? "Store Fwd, " : "";
            sb.Append(s);
            s = services_.HalfDuplex ? "Half Duplex, " : "";
            sb.Append(s);
            s = services_.FullDuplex ? "Full Duplex, " : "";
            sb.Append(s);
            s = services_.ClosedNetwork ? "Closed Network, " : "";
            sb.Append(s);
            s = services_.FleetNet ? "FleetNet, " : "";
            sb.Append(s);
            s = services_.PrefixSF ? "Prefix SF, " : "";
            sb.Append(s);
            s = services_.LandMobileAlerting ? "Land Mobile Alerting, " : "";
            sb.Append(s);
            s = services_.AeroC ? "Aero-C, " : "";
            sb.Append(s);
            s = services_.ITA2 ? "ITA2, " : "";
            sb.Append(s);
            s = services_.DATA ? "Data, " : "";
            sb.Append(s);
            s = services_.BasicX400 ? "Basic X400, " : "";
            sb.Append(s);
            s = services_.EnhancedX400 ? "Enhanced X400, " : "";
            sb.Append(s);
            s = services_.LowPowerCMES ? "Low Power C MES, " : "";
            sb.Append(s);

            label.Text = sb.ToString().Remove(sb.Length - 2);
        }

        /// <summary>
        /// Just before the main form closes, diconnect the mapper.
        /// </summary>
        private void MainForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            uniGMapper.Disconnect();
            dbFlushToDiskTimer.Enabled = false;
            database.Flush();
        }

        /// <summary>
        /// Sets the background color of the panel displaying the uni messages.
        /// </summary>
        private void SetUniTypeBackground(bool isEGC, int priority, int contentType)
        {
            pnlUniPriority.Visible = isEGC;
            splitterUniMap.Visible = isEGC;
            pnlUniMap.Visible = isEGC;

            if (isEGC)
            {
                uniMsgBox.Dock = DockStyle.Top;
            }
            else
            {
                uniMsgBox.Dock = DockStyle.Fill;
            }

            if (isEGC)
            {
                switch (priority)
                {
                    case 0:
                        //routine
                        if (cbDarkTheme.Checked)
                        {
                            uniGridSplitter.BackColor = Color.DarkTurquoise;
                            splitterUniMap.BackColor = Color.DarkTurquoise;
                        }
                        else
                        {
                            uniGridSplitter.BackColor = Color.PaleTurquoise;
                            splitterUniMap.BackColor = Color.PaleTurquoise;
                        }
                        break;

                    case 1:
                        //safety
                        if (cbDarkTheme.Checked)
                        {
                            uniGridSplitter.BackColor = Color.DarkGreen;
                            splitterUniMap.BackColor = Color.DarkGreen;
                        }
                        else
                        {
                            uniGridSplitter.BackColor = Color.LightGreen;
                            splitterUniMap.BackColor = Color.LightGreen;
                        }
                        break;

                    case 2:
                        //urgency
                        if (cbDarkTheme.Checked)
                        {
                            uniGridSplitter.BackColor = Color.DarkGoldenrod;
                            splitterUniMap.BackColor = Color.DarkGoldenrod;
                        }
                        else
                        {
                            uniGridSplitter.BackColor = Color.Gold;
                            splitterUniMap.BackColor = Color.Gold;
                        }
                        break;

                    case 3:
                        //distress
                        if (cbDarkTheme.Checked)
                        {
                            uniGridSplitter.BackColor = Color.DarkRed;
                            splitterUniMap.BackColor = Color.DarkRed;
                        }
                        else
                        {
                            uniGridSplitter.BackColor = Color.Red;
                            splitterUniMap.BackColor = Color.Red;
                        }
                        break;
                }
            }
            else
            {
                switch (contentType)
                {
                    case PayloadType.CNT_ZAP:
                        //Zap
                        if (cbDarkTheme.Checked)
                        {
                            uniGridSplitter.BackColor = Color.DarkGray;
                        }
                        else
                        {
                            uniGridSplitter.BackColor = Color.WhiteSmoke;
                        }
                        break;

                    case PayloadType.CNT_IA5:
                        //IA5
                        if (cbDarkTheme.Checked)
                        {
                            uniGridSplitter.BackColor = Color.DarkViolet;
                        }
                        else
                        {
                            uniGridSplitter.BackColor = Color.Plum;
                        }
                        break;

                    case PayloadType.CNT_ITA2:
                        //ITA2
                        if (cbDarkTheme.Checked)
                        {
                            uniGridSplitter.BackColor = Color.DarkGray;
                        }
                        else
                        {
                            uniGridSplitter.BackColor = Color.LightGray;
                        }
                        break;

                    case PayloadType.CNT_BINARY:
                        //Binary
                        if (cbDarkTheme.Checked)
                        {
                            uniGridSplitter.BackColor = Color.DarkSalmon;
                        }
                        else
                        {
                            uniGridSplitter.BackColor = Color.LightSalmon;
                        }
                        break;
                }
            }
        }

        private void dbFlushToDiskTimer_Tick(object sender, EventArgs e)
        {
            database.Flush();
        }

        private void dgvUni_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                ///
                /// Compute the display data for uni[e.RowIndex]
                ///

                if (e.RowIndex == -1)
                {
                    /// User clicked on header.
                    /// The datasource we chose for the application does not support native sorting and filtering.
                    /// We will change it in the future.
                    /// TODO

                    return;
                }

                //load the Uni
                Uni currentUni = ((List<Uni>)unisBindingSource.DataSource).ElementAt(e.RowIndex);
                SelectedMIdOrLCNo = currentUni.MIdOrLCNo;

                //set the Uni

                // Allows us to decide if we want to update or not the text
                // for example if the user is scrolling the text and the text is not changed
                // there is no reason to refresh the content.
                if (currentUni.Message != null)
                {
                    if (!currentUni.Message.Equals(uniMsgBox.Text))
                    {
                        uniMsgBox.Text = currentUni.Message;
                    }
                }

                //set the background
                SetUniTypeBackground(currentUni.IsEGC, currentUni.Priority, currentUni.ContentType);

                //set the Service and Address
                lblUniServiceAddress.Text = PacketDecoderUtils.ReturnServiceCodeAndAddressName(currentUni.MessageType);

                //set the Sat and service header
                PacketDecoder7D pd7D = currentUni.BulletinBoard;
                if (pd7D != null)
                {
                    lblUniChannelTypeAndSat.Text = string.Format("{0}: {1}, {2}", pd7D.ChannelTypeName, pd7D.LesName, pd7D.SatName);
                    lblUniPriority.Text = string.Format("{0}:", PacketDecoderUtils.ReturnPriority(currentUni.Priority));

                    SetServices(pd7D.Services_, lblUniServices);
                    SetStatus(pd7D.Status_, lblUniStatus);
                }

                //draw the map
                if (currentUni.Area != null)
                {
                    GeoArea ga = currentUni.Area;
                    switch (currentUni.MessageType)
                    {
                        case 0x00:
                        case 0x31:
                            // add the xml part to the navArea
                            int navAreaId = ((NavArea)ga).Area;
                            ((NavArea)ga).XmlArea = navAreas.ReturnXmlNavArea(navAreaId);
                            if (((NavArea)ga).XmlArea != null)
                            {
                                ((NavArea)ga).Text = (((NavArea)ga).XmlArea.Name.Trim() == "") ? ((NavArea)ga).Text : ((NavArea)ga).XmlArea.Name;
                            }
                            uniGMapper.DrawNavArea((NavArea)ga);
                            lblUniServiceAddress.Text = string.Format("{0}\r\n{1}", ga.Text, lblUniServiceAddress.Text);
                            break;

                        case 0x04:
                        case 0x34:
                            uniGMapper.DrawRectangularArea((RectangularArea)ga);
                            lblUniServiceAddress.Text = string.Format("{0}\r\n{1}", ga.Text, lblUniServiceAddress.Text);
                            break;

                        case 0x13:
                            uniGMapper.DrawCoastalArea((CoastalArea)ga);
                            lblUniServiceAddress.Text = string.Format("{0}\r\n{1}", ga.Text, lblUniServiceAddress.Text);
                            break;

                        case 0x14:
                        case 0x24:
                        case 0x44:
                            uniGMapper.DrawCircularArea((CircularArea)ga);
                            lblUniServiceAddress.Text = string.Format("{0}\r\n{1}", ga.Text, lblUniServiceAddress.Text);
                            break;

                        default:
                            uniGMapper.Clear();
                            break;
                    }
                }
                else
                {
                    uniGMapper.Clear();
                }
            }
            catch (Exception ex)
            {
                Utils.Log.Error(ex);
            }
        }

        private void dgvUni_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.PageUp || e.KeyCode == Keys.Down || e.KeyCode == Keys.Up)
            {
                rowUniSelectedByKeyPress = true;
            }
        }

        private void dgvUni_RowEnter(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex != 0)
            {
                if (rowUniSelectedByKeyPress)
                {
                    rowUniSelectedByKeyPress = false;
                    dgvUni_CellClick(sender, new DataGridViewCellEventArgs(-1, e.RowIndex));
                }
                else
                {
                    dgvUni_CellClick(null, new DataGridViewCellEventArgs(-1, e.RowIndex));
                }
            }
            else
            {
                if (rowUniSelectedByKeyPress && e.RowIndex == 0)
                {
                    rowUniSelectedByKeyPress = false;
                    dgvUni_CellClick(sender, new DataGridViewCellEventArgs(-1, 0));
                }
            }
        }

        private void dgvUni_CellPainting(object sender, DataGridViewCellPaintingEventArgs e)
        {
            if (e.RowIndex >= 0 && e.ColumnIndex >= 0)
            {
                if (dgvUni.Columns[e.ColumnIndex].DataPropertyName.Equals("IsPartial"))
                {
                    if (dgvUni.Rows[e.RowIndex].Cells["UniIsEGC"].Value.ToString().ToLower() == "false")
                    {
                        e.PaintBackground(e.ClipBounds, true);
                        e.Handled = true;
                    }
                }
            }
        }

        private void dgvUni_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            if (dgvUni.Columns[e.ColumnIndex].DataPropertyName.Equals("Priority"))
            {
                switch (e.Value)
                {
                    case 0:
                        if (dgvUni.Rows[e.RowIndex].Cells["UniIsEGC"].Value.ToString().ToLower() == "true")
                        {
                            //routine
                            if (cbDarkTheme.Checked)
                            {
                                e.CellStyle.BackColor = Color.Turquoise;
                                e.CellStyle.ForeColor = Color.Turquoise;
                                e.CellStyle.SelectionBackColor = Color.DarkTurquoise;
                                e.CellStyle.SelectionForeColor = Color.DarkTurquoise;
                            }
                            else
                            {
                                e.CellStyle.BackColor = Color.PaleTurquoise;
                                e.CellStyle.ForeColor = Color.PaleTurquoise;
                                e.CellStyle.SelectionBackColor = Color.MediumTurquoise;
                                e.CellStyle.SelectionForeColor = Color.MediumTurquoise;
                            }
                            e.Value = "Routine";
                        }
                        else
                        {
                            //not EGC, have an empty cell
                            if (cbDarkTheme.Checked)
                            {
                                e.CellStyle.BackColor = Color.Gray;
                                e.CellStyle.ForeColor = Color.Gray;
                                e.CellStyle.SelectionBackColor = Color.DarkGray;
                                e.CellStyle.SelectionForeColor = Color.DarkGray;
                            }
                            else
                            {
                                e.CellStyle.BackColor = Color.White;
                                e.CellStyle.ForeColor = Color.White;
                                e.CellStyle.SelectionBackColor = Color.WhiteSmoke;
                                e.CellStyle.SelectionForeColor = Color.WhiteSmoke;
                            }
                            e.Value = "";
                        }
                        break;

                    case 1:
                        //safety
                        if (cbDarkTheme.Checked)
                        {
                            e.CellStyle.BackColor = Color.DarkGreen;
                            e.CellStyle.ForeColor = Color.DarkGreen;
                            e.CellStyle.SelectionBackColor = Color.Green;
                            e.CellStyle.SelectionForeColor = Color.Green;
                        }
                        else
                        {
                            e.CellStyle.BackColor = Color.LightGreen;
                            e.CellStyle.ForeColor = Color.LightGreen;
                            e.CellStyle.SelectionBackColor = Color.LimeGreen;
                            e.CellStyle.SelectionForeColor = Color.LimeGreen;
                        }
                        e.Value = "Safety";
                        break;

                    case 2:
                        //urgency
                        if (cbDarkTheme.Checked)
                        {
                            e.CellStyle.BackColor = Color.DarkGoldenrod;
                            e.CellStyle.ForeColor = Color.DarkGoldenrod;
                            e.CellStyle.SelectionBackColor = Color.DarkOrange;
                            e.CellStyle.SelectionForeColor = Color.DarkOrange;
                        }
                        else
                        {
                            e.CellStyle.BackColor = Color.Gold;
                            e.CellStyle.ForeColor = Color.Gold;
                            e.CellStyle.SelectionBackColor = Color.Orange;
                            e.CellStyle.SelectionForeColor = Color.Orange;
                        }
                        e.Value = "Urgency";
                        break;

                    case 3:
                        //distress
                        if (cbDarkTheme.Checked)
                        {
                            e.CellStyle.BackColor = Color.DarkRed;
                            e.CellStyle.ForeColor = Color.DarkRed;
                        }
                        else
                        {
                            e.CellStyle.BackColor = Color.Red;
                            e.CellStyle.ForeColor = Color.Red;
                        }
                        e.CellStyle.SelectionBackColor = Color.DarkRed;
                        e.CellStyle.SelectionForeColor = Color.DarkRed;
                        e.Value = "Distress";
                        break;
                }
            }
            else if (dgvUni.Columns[e.ColumnIndex].DataPropertyName.Equals("ContentType"))
            {
                switch (e.Value)
                {
                    case PayloadType.CNT_ZAP:
                        //Zap
                        if (cbDarkTheme.Checked)
                        {
                            e.CellStyle.BackColor = Color.Gray;
                            e.CellStyle.ForeColor = Color.Gray;
                            e.CellStyle.SelectionBackColor = Color.DarkGray;
                            e.CellStyle.SelectionForeColor = Color.DarkGray;
                        }
                        else
                        {
                            e.CellStyle.BackColor = Color.White;
                            e.CellStyle.ForeColor = Color.White;
                            e.CellStyle.SelectionBackColor = Color.WhiteSmoke;
                            e.CellStyle.SelectionForeColor = Color.WhiteSmoke;
                        }
                        e.Value = "Zap";
                        break;

                    case PayloadType.CNT_IA5:
                        //IA5
                        if (cbDarkTheme.Checked)
                        {
                            e.CellStyle.BackColor = Color.DarkViolet;
                            e.CellStyle.ForeColor = Color.DarkViolet;
                            e.CellStyle.SelectionBackColor = Color.Plum;
                            e.CellStyle.SelectionForeColor = Color.Plum;
                        }
                        else
                        {
                            e.CellStyle.BackColor = Color.Plum;
                            e.CellStyle.ForeColor = Color.Plum;
                            e.CellStyle.SelectionBackColor = Color.MediumOrchid;
                            e.CellStyle.SelectionForeColor = Color.MediumOrchid;
                        }
                        e.Value = "IA5";
                        break;

                    case PayloadType.CNT_ITA2:
                        //ITA2
                        if (cbDarkTheme.Checked)
                        {
                            e.CellStyle.BackColor = Color.DarkGray;
                            e.CellStyle.ForeColor = Color.DarkGray;
                            e.CellStyle.SelectionBackColor = Color.DarkSlateGray;
                            e.CellStyle.SelectionForeColor = Color.DarkSlateGray;
                        }
                        else
                        {
                            e.CellStyle.BackColor = Color.LightGray;
                            e.CellStyle.ForeColor = Color.LightGray;
                            e.CellStyle.SelectionBackColor = Color.DarkGray;
                            e.CellStyle.SelectionForeColor = Color.DarkGray;
                        }

                        e.Value = "ITA2";
                        break;

                    case PayloadType.CNT_BINARY:
                        //Binary
                        if (cbDarkTheme.Checked)
                        {
                            e.CellStyle.BackColor = Color.DarkSalmon;
                            e.CellStyle.ForeColor = Color.DarkSalmon;
                            e.CellStyle.SelectionBackColor = Color.Crimson;
                            e.CellStyle.SelectionForeColor = Color.Crimson;
                        }
                        else
                        {
                            e.CellStyle.BackColor = Color.LightSalmon;
                            e.CellStyle.ForeColor = Color.LightSalmon;
                            e.CellStyle.SelectionBackColor = Color.DarkSalmon;
                            e.CellStyle.SelectionForeColor = Color.DarkSalmon;
                        }
                        e.Value = "Binary";
                        break;
                }
            }
            else if (dgvUni.Columns[e.ColumnIndex].DataPropertyName.Equals("Time"))
            {
                if ((DateTime)e.Value == null)
                {
                    e.Value = "";
                }
                else if ((DateTime)e.Value == DateTime.MinValue)
                {
                    e.Value = "";
                }
                else
                {
                    e.Value = string.Format("{0:MM/dd/yyyy HH:mm:ss}", e.Value);
                }
            }
            else if (dgvUni.Columns[e.ColumnIndex].DataPropertyName.Equals("SatName"))
            {
                string satN = "";
                if (e.Value != null)
                {
                    try
                    {
                        satN = string.Format("{0}, {1}", dgvUni.Rows[e.RowIndex].Cells["UniSat"].Value.ToString(), e.Value).Split('(', ')')[1];
                    }
                    catch { }
                }
                e.Value = satN;

            }
            else if (dgvUni.Columns[e.ColumnIndex].DataPropertyName.Equals("StreamId"))
            {
                string sl = e.Value.ToString();
                e.Value = string.Format("*{0,5}", sl.Substring(sl.Length - 5));
            }
            else if (dgvUni.Columns[e.ColumnIndex].DataPropertyName.Equals("Repetition"))
            {
                if (dgvUni.Rows[e.RowIndex].Cells["UniIsEGC"].Value.ToString().ToLower() == "false")
                {
                    e.Value = "";
                }
            }
        }

        private void btnUniAutoScroll_Click(object sender, EventArgs e)
        {
            btnUniAutoScroll.Checked = !btnUniAutoScroll.Checked;

            btnUniAutoLastSelect.Enabled = btnUniAutoScroll.Checked;
            btnUniAutoLastSelect.Checked = btnUniAutoScroll.Checked;
        }

        private void SetTheme(bool enabled)
        {
            try
            {
                UpdateColorControls(this);
                if (dgvUni.SelectedRows.Count == 0)
                {
                    dgvUni_CellClick(null, new DataGridViewCellEventArgs(-1, 0));
                }
                else
                {
                    dgvUni_CellClick(null, new DataGridViewCellEventArgs(-1, dgvUni.SelectedRows[0].Index));
                }
            }
            finally
            {
                
            }
        }

        private void dgvUni_RowPrePaint(object sender, DataGridViewRowPrePaintEventArgs e)
        {
            if (cbDarkTheme.Checked)
            {
                dgvUni.Rows[e.RowIndex].DefaultCellStyle.BackColor = darkBackColor;
                dgvUni.Rows[e.RowIndex].DefaultCellStyle.ForeColor = controlTextColor;
            }
            else
            {
                dgvUni.Rows[e.RowIndex].DefaultCellStyle.BackColor = SystemColors.Window;
                dgvUni.Rows[e.RowIndex].DefaultCellStyle.ForeColor = SystemColors.WindowText;
            }
        }

        private void cbDarkTheme_CheckedChanged(object sender, EventArgs e)
        {
            //background
            int r = (int)nBkgR.Value;
            int g = (int)nBkgG.Value;
            int b = (int)nBkgB.Value;
            int rLite = (int)(nBkgR.Value + nBkg2.Value);
            int gLite = (int)(nBkgG.Value + nBkg2.Value);
            int bLite = (int)(nBkgB.Value + nBkg2.Value);
            if (rLite < 0) rLite = 0;
            if (rLite > 255) rLite = 255;
            if (gLite < 0) gLite = 0;
            if (gLite > 255) gLite = 255;
            if (bLite < 0) bLite = 0;
            if (bLite > 255) bLite = 255;

            //text
            int rTxt = (int)nTextR.Value;
            int gTxt = (int)nTextG.Value;
            int bTxt = (int)nTextB.Value;
            int rTxtLite = (int)(nTextR.Value + nText2.Value);
            int gTxtLite = (int)(nTextG.Value + nText2.Value);
            int bTxtLite = (int)(nTextB.Value + nText2.Value);
            if (rTxtLite < 0) rTxtLite = 0;
            if (rTxtLite > 255) rTxtLite = 255;
            if (gTxtLite < 0) gTxtLite = 0;
            if (gTxtLite > 255) gTxtLite = 255;
            if (bTxtLite < 0) bTxtLite = 0;
            if (bTxtLite > 255) bTxtLite = 255;

            if (cbDarkTheme.Checked)
            {
                lightBackColor = Color.FromArgb(rLite, gLite, bLite);
                darkBackColor = Color.FromArgb(r, g, b);

                controlTextColor = Color.FromArgb(rTxt, gTxt, bTxt);
                controlTextLiteColor = Color.FromArgb(rTxtLite, gTxtLite, bTxtLite);
            }

            SetTheme(cbDarkTheme.Enabled);
        }

        private void nR_ValueChanged(object sender, EventArgs e)
        {
            cbDarkTheme_CheckedChanged(null, null);
        }

        private void nChartDarkening_ValueChanged(object sender, EventArgs e)
        {
            cbDarkTheme_CheckedChanged(null, null);
        }

        /// <summary>
        /// https://stackoverflow.com/questions/22935285/change-color-of-all-controls-inside-the-form-in-c-sharp
        /// </summary>
        /// <param name="winControl"></param>
        private void UpdateColorControls(Control winControl)
        {
            if (winControl is Form || winControl is Panel || winControl is Button || winControl is GroupBox || winControl is CheckBox)
            {
                if (cbDarkTheme.Checked)
                {
                    winControl.BackColor = darkBackColor;
                    winControl.ForeColor = controlTextColor;
                }
                else
                {
                    winControl.BackColor = SystemColors.ButtonFace;
                    winControl.ForeColor = SystemColors.ControlText;
                }
            }

            if (winControl is RichTextBox)
            {
                if (cbDarkTheme.Checked)
                {
                    winControl.BackColor = lightBackColor;
                    winControl.ForeColor = controlTextLiteColor;
                }
                else
                {
                    winControl.BackColor = SystemColors.Window;
                    winControl.ForeColor = SystemColors.WindowText;
                }
            }

            if (winControl is TextBox tb)
            {
                if (cbDarkTheme.Checked)
                {
                    winControl.BackColor = lightBackColor;
                    winControl.ForeColor = controlTextColor;

                    if (tb == txtRawFrameUDPPort)
                    {
                        winControl.ForeColor = controlTextLiteColor;
                    }
                }
                else
                {
                    winControl.BackColor = SystemColors.Window;
                    winControl.ForeColor = SystemColors.WindowText;
                }
            }

            if (winControl is Label x)
            {
                if (x == ll1 || x == ll2 || x == ll3 || x == ll4 || x == ll5 || x == ll16 || x == ll7 || x == ll8 || x == ll9 || x == ll10
                    || x == ll11 || x == ll12 || x == ll13 || x == ll14 || x == ll15 || x == ll6 || x == ll17 || x == ll18 || x == ll19 
                    || x == ll20 || x == ll21 || x == ll22 || x == ll23 || x == ll24 || x == lblUniPriority || x == lblVersion || x == ll25
                    || x == ll26 || x == ll27 || x == ll28 || x == ll29
                    )
                {

                }
                else if (cbDarkTheme.Checked)
                {
                    winControl.BackColor = lightBackColor;
                    winControl.ForeColor = controlTextColor;

                    if (x == lbl0 || x == lbl1 || x == lbl2 || x == lbl3 || x == lbl4 || x == lbl5 || x == lbl6 || x == lbl7 || x == lbl8 || x == lbl9
                        || x == lblUniChannelTypeAndSat || x == lblUniServices || x == lblUniStatus
                        || x == lblUniServiceAddress
                        )
                    {
                        winControl.ForeColor = controlTextLiteColor;
                    }
                }
                else
                {
                    winControl.BackColor = SystemColors.ControlLight;
                    winControl.ForeColor = SystemColors.ControlText;
                }
            }

            if (winControl is DataGridView)
            {
                if (cbDarkTheme.Checked)
                {
                    ((DataGridView)winControl).BackgroundColor = darkBackColor;
                    ((DataGridView)winControl).ColumnHeadersDefaultCellStyle.BackColor = darkBackColor;
                    ((DataGridView)winControl).ColumnHeadersDefaultCellStyle.ForeColor = controlTextColor;
                }
                else
                {
                    ((DataGridView)winControl).BackgroundColor = SystemColors.Window;
                    ((DataGridView)winControl).ColumnHeadersDefaultCellStyle.BackColor = SystemColors.Window;
                    ((DataGridView)winControl).ColumnHeadersDefaultCellStyle.ForeColor = SystemColors.WindowText;
                }
            }

            foreach (Control subC in winControl.Controls)
            {
                UpdateColorControls(subC);
            }
        }

        private void uniGMap_Paint(object sender, PaintEventArgs e)
        {
            if (cbDarkTheme.Checked)
            {
                e.Graphics.FillRectangle(
                    new SolidBrush(Color.FromArgb((int)nChartDarkening.Value, 0, 0, 0)),
                    new Rectangle(uniGMap.Left, uniGMap.Top, uniGMap.Right, uniGMap.Bottom));
            }
        }

        private void toolStripButton1_Click(object sender, EventArgs e)
        {
            database.Clear();
            database.Flush();
            unis.Clear();
            LoadOnceGridFromDb();
            uniMsgBox.Text = "";
            uniGMapper.Clear();
            lblUniChannelTypeAndSat.Text = "";
            lblUniServices.Text = "";
            lblUniStatus.Text = "";
            lblUniServiceAddress.Text = "Service and address";
            uniGridSplitter.BackColor = SystemColors.ButtonFace;
            splitterUniMap.BackColor = SystemColors.ButtonFace;

            //to avoid logging over the same indexes after a database clear
            //we postpend the name of the log with "_"
            if (txtLogFolder.Text.Length > 0)
            {
                txtLogFolder.Text += "_";
                folderBrowserDialog1.SelectedPath = "_";
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (fontDialog1.ShowDialog() == DialogResult.OK)
            {
                uniMsgBox.Font = fontDialog1.Font;
                txtFont.Text = string.Format("{0}, {1:0.##}", uniMsgBox.Font.Name, uniMsgBox.Font.SizeInPoints);
            }
        }

        private void uniMsgBox_LinkClicked(object sender, LinkClickedEventArgs e)
        {
            Process.Start(e.LinkText);
        }

        private void btnLogFolder_Click(object sender, EventArgs e)
        {
            if (folderBrowserDialog1.ShowDialog() == DialogResult.OK)
            {
                txtLogFolder.Text = folderBrowserDialog1.SelectedPath;
            }
        }

        private void cbLogMessages_CheckedChanged(object sender, EventArgs e)
        {
            bool chked = cbLogMessages.Checked;
            FileLogger.IsLogMessages = chked;
            txtLogFolder.Enabled = chked;
            btnLogFolder.Enabled = chked;
            cbLogRaw.Enabled = chked;
        }

        private void txtLogFolder_TextChanged(object sender, EventArgs e)
        {
            FileLogger.DefaultPath = txtLogFolder.Text;
        }

        private void cbLogRaw_CheckedChanged(object sender, EventArgs e)
        {
            FileLogger.IsLogRaw = cbLogRaw.Checked;
        }

        private void rtbNotes_LinkClicked(object sender, LinkClickedEventArgs e)
        {
            Process.Start(e.LinkText);
        }
    }

    public static class ExtensionForRichTextBox
    {
        /// <summary>
        /// https://stackoverflow.com/questions/18966407/enable-copy-cut-past-window-in-a-rich-text-box
        /// </summary>
        /// <param name="rtb"></param>
        public static void AddContextMenu(this RichTextBox rtb, GMapper gmp)
        {
            string msg = "Cannot parse this selection. If you think the selection contains valid coordinates, copy the message and send it via email to the author to add the format in a future release.";

            if (rtb.ContextMenuStrip == null)
            {
                ContextMenuStrip cms = new ContextMenuStrip { ShowImageMargin = false };
                ToolStripMenuItem tsmiSelectAll = new ToolStripMenuItem("Select all text");
                tsmiSelectAll.Click += (sender, e) =>
                {
                    rtb.SelectAll();
                    rtb.Focus();
                };
                cms.Items.Add(tsmiSelectAll);

                ToolStripMenuItem tsmiCopy = new ToolStripMenuItem("Copy text to Clipboard");
                tsmiCopy.Click += (sender, e) => rtb.Copy();
                cms.Items.Add(tsmiCopy);

                cms.Items.Add("-");

                ToolStripMenuItem tsmiDrawSelectionOnChart = new ToolStripMenuItem("Draw selection on chart");
                tsmiDrawSelectionOnChart.Click += (sender, e) =>
                {
                    string input = rtb.SelectedText;
                    if (!CoordinatesList.TryParseCoordinates(input)) MessageBox.Show(msg, "Report");
                    gmp.DrawSelection(CoordinatesList.List);
                };
                cms.Items.Add(tsmiDrawSelectionOnChart);

                ToolStripMenuItem tsmiDrawSelectionOnChartMarkZoom = new ToolStripMenuItem("Draw, mark and zoom selection");
                tsmiDrawSelectionOnChartMarkZoom.Click += (sender, e) =>
                {
                    string input = rtb.SelectedText;
                    if (!CoordinatesList.TryParseCoordinates(input)) MessageBox.Show(msg, "Report");
                    gmp.DrawSelectionMarkZoom(CoordinatesList.List);
                };
                cms.Items.Add(tsmiDrawSelectionOnChartMarkZoom);

                ToolStripMenuItem tsmiMarkPointsZoom = new ToolStripMenuItem("Mark and zoom selection");
                tsmiMarkPointsZoom.Click += (sender, e) =>
                {
                    string input = rtb.SelectedText;
                    if (!CoordinatesList.TryParseCoordinates(input)) MessageBox.Show(msg, "Report");
                    gmp.MarkPointsZoom(CoordinatesList.List);
                };
                cms.Items.Add(tsmiMarkPointsZoom);

                ToolStripMenuItem tsmiClearSelections = new ToolStripMenuItem("Clear selection");
                tsmiClearSelections.Click += (sender, e) =>
                {
                    gmp.ClearSelections();
                };
                cms.Items.Add(tsmiClearSelections);

                rtb.ContextMenuStrip = cms;
            }
        }
    }

    /// <summary>
    /// POC, TODO a proper implementation
    /// </summary>
    public static class ExtensionForGMapControl
    {
        public static void AddContextMenu(this GMap.NET.WindowsForms.GMapControl gmc, GMapper gmp)
        {
            if (gmc.ContextMenuStrip == null)
            {
                ContextMenuStrip cms = new ContextMenuStrip { ShowImageMargin = false };
                ToolStripMenuItem tsmiGoogleMap = new ToolStripMenuItem("Google Map");
                tsmiGoogleMap.Click += (sender, e) =>
                {
                    gmp.SetNewProvider(0);
                };
                cms.Items.Add(tsmiGoogleMap);

                ToolStripMenuItem tsmiBingHybridMap = new ToolStripMenuItem("Bing Hybrid Map");
                tsmiBingHybridMap.Click += (sender, e) =>
                {
                    while (gmc.Zoom < 1) gmc.Zoom++;
                    gmp.SetNewProvider(1);
                };
                cms.Items.Add(tsmiBingHybridMap);

                gmc.ContextMenuStrip = cms;
            }
        }
    }
}
