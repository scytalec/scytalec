﻿/*
 * microp11 2018
 * 
 * This file is part of Scytale-C.
 * 
 * Scytale-C is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Scytale-C is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Scytale-C.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 */
 
using ScytaleC.PacketDecoders;
using System;
using System.IO;

namespace ScytaleC
{
    public static class FileLogger
    {
        public static string DefaultPath { get; set; }
        public static bool IsLogMessages { get; set; }
        public static bool IsLogRaw { get; set; }

        /// <summary>
        /// Will create if doesn't exist. Will append if exists.
        /// </summary>
        /// <param name="fileName"></param>
        /// <param name="data"></param>
        public static void LogRaw(string fileName, byte[] data)
        {
            if (!IsLogMessages) return;
            if (!IsLogRaw) return;
            if (DefaultPath.Length == 0) return;

            try
            {
                string path = string.Format("{0}\\{1}\\{2:yyyyMMdd}", DefaultPath, "raw", DateTime.Now);
                Directory.CreateDirectory(path);

                path += string.Format("\\{0}{1}", fileName, ".bin");

                using (Stream fs = new FileStream(path, FileMode.Append))
                {
                    BinaryWriter bw = new BinaryWriter(fs);
                    bw.Write(data);
                    bw.Close();
                }
            }
            catch (Exception ex)
            {
                Utils.Log.Error(ex);
            }
        }

        /// <summary>
        /// Will create if doesn't exist. Will append if exists.
        /// </summary>
        /// <param name="fileName"></param>
        /// <param name="message"></param>
        public async static void LogMsg(string fileName, string message)
        {
            if (!IsLogMessages) return;
            if (DefaultPath.Length == 0) return;

            try
            {
                string path = string.Format("{0}\\{1}\\{2:yyyyMMdd}", DefaultPath, "decoded", DateTime.Now);
                Directory.CreateDirectory(path);

                path += string.Format("\\{0}{1}", fileName, ".log");

                using (StreamWriter sr = File.AppendText(path))
                {
                    await sr.WriteAsync(message);
                    sr.Close();
                }
            }
            catch (Exception ex)
            {
                Utils.Log.Error(ex);
            }
        }
    }
}
