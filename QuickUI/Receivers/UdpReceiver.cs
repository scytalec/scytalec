/*
 * microp11 2017
 * 
 * https://stackoverflow.com/questions/461742/how-to-convert-an-ipv4-address-into-a-integer-in-c
 * https://www.codeproject.com/Articles/132623/Basic-UDP-Receiver
 * 
 * Receives UDP packets over the selected network card.
 * 
 */

using ScytaleC.PacketDecoders;
using ScytaleC.Interfaces;
using System;
using System.Net;
using System.Net.Sockets;

namespace ScytaleC.QuickUI
{
    public sealed class UdpReceiver : IDisposable
    {
        public UdpClient udpc;

        public UdpReceiver(int port)
        {
            IPEndPoint local = new IPEndPoint(IPAddress.Any, port);
            udpc = new UdpClient(local);
            udpc.Client.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.Broadcast, 1);
            udpc.Client.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.DontRoute, 1);
        }

        public void Start()
        {
            udpc.BeginReceive(requestCallback, null);
        }

        /// <summary>
        /// We assume every callback comes back with 1280 bytes of data.
        /// No length determination or reconstruction of packet is done here.
        /// This might create loss of data if the Inmarsat info is not fully contained withing the received UDP callback.
        /// </summary>
        /// <param name="ar"></param>
        private void requestCallback(IAsyncResult ar)
        {
            try
            {
                IPEndPoint receivedIpEndPoint = new IPEndPoint(IPAddress.Any, 0);

                if (udpc.Client == null)
                {
                    return;
                }

                Byte[] receiveBytes = udpc.EndReceive(ar, ref receivedIpEndPoint);

                UDPPacketArgs upa = new UDPPacketArgs();
                upa.Length = receiveBytes.Length;
                upa.UDPPacket = receiveBytes;
                OnUDPPacket?.Invoke(this, upa);

                // Restart listening for udp data packages
                udpc.BeginReceive(requestCallback, null);
            }
            catch (Exception ex1)
            {
                Utils.Log.Error(ex1);
            }
        }

        public void Dispose()
        {
            udpc?.Close();
        }

        /// <summary>
        /// Event handler for demodulated symbols.
        /// Each symbol is one byte.
        /// This allows for a soft Viterbi decoder implementation down the decoding chain.
        /// </summary>
        public event EventHandler<UDPPacketArgs> OnUDPPacket;
    }
}
