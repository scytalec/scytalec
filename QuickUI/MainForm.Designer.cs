﻿namespace ScytaleC.QuickUI
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.lbl6a = new System.Windows.Forms.Label();
            this.lbl3a = new System.Windows.Forms.Label();
            this.lbl6 = new System.Windows.Forms.Label();
            this.lbl3 = new System.Windows.Forms.Label();
            this.lbl8a = new System.Windows.Forms.Label();
            this.lbl5a = new System.Windows.Forms.Label();
            this.lbl2a = new System.Windows.Forms.Label();
            this.lbl8 = new System.Windows.Forms.Label();
            this.lbl5 = new System.Windows.Forms.Label();
            this.lbl2 = new System.Windows.Forms.Label();
            this.lbl9a = new System.Windows.Forms.Label();
            this.lbl7a = new System.Windows.Forms.Label();
            this.lbl4a = new System.Windows.Forms.Label();
            this.lbl1a = new System.Windows.Forms.Label();
            this.lbl0a = new System.Windows.Forms.Label();
            this.lbl9 = new System.Windows.Forms.Label();
            this.lbl7 = new System.Windows.Forms.Label();
            this.lbl4 = new System.Windows.Forms.Label();
            this.lbl1 = new System.Windows.Forms.Label();
            this.lbl0 = new System.Windows.Forms.Label();
            this.btnCredits = new System.Windows.Forms.Button();
            this.btnStart = new System.Windows.Forms.Button();
            this.btnMenu = new System.Windows.Forms.Button();
            this.panelMenu = new System.Windows.Forms.Panel();
            this.gbFrames = new System.Windows.Forms.GroupBox();
            this.cbA8 = new System.Windows.Forms.CheckBox();
            this.cbA3 = new System.Windows.Forms.CheckBox();
            this.cb81 = new System.Windows.Forms.CheckBox();
            this.cbAD = new System.Windows.Forms.CheckBox();
            this.cb91 = new System.Windows.Forms.CheckBox();
            this.cbA0 = new System.Windows.Forms.CheckBox();
            this.cbAC = new System.Windows.Forms.CheckBox();
            this.cb08 = new System.Windows.Forms.CheckBox();
            this.cb2A = new System.Windows.Forms.CheckBox();
            this.cb27 = new System.Windows.Forms.CheckBox();
            this.cb9A = new System.Windows.Forms.CheckBox();
            this.cb83 = new System.Windows.Forms.CheckBox();
            this.cbBE = new System.Windows.Forms.CheckBox();
            this.cbBD = new System.Windows.Forms.CheckBox();
            this.cbB2 = new System.Windows.Forms.CheckBox();
            this.cbB1 = new System.Windows.Forms.CheckBox();
            this.cbAB = new System.Windows.Forms.CheckBox();
            this.cbAA = new System.Windows.Forms.CheckBox();
            this.cb7D = new System.Windows.Forms.CheckBox();
            this.cb6C = new System.Windows.Forms.CheckBox();
            this.cb92 = new System.Windows.Forms.CheckBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.cbLogRaw = new System.Windows.Forms.CheckBox();
            this.cbLogMessages = new System.Windows.Forms.CheckBox();
            this.ll29 = new System.Windows.Forms.Label();
            this.btnLogFolder = new System.Windows.Forms.Button();
            this.txtLogFolder = new System.Windows.Forms.TextBox();
            this.cbSavePackets = new System.Windows.Forms.CheckBox();
            this.ll28 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.nChartDarkening = new System.Windows.Forms.NumericUpDown();
            this.ll25 = new System.Windows.Forms.Label();
            this.ll27 = new System.Windows.Forms.Label();
            this.ll26 = new System.Windows.Forms.Label();
            this.nText2 = new System.Windows.Forms.NumericUpDown();
            this.nTextG = new System.Windows.Forms.NumericUpDown();
            this.nTextB = new System.Windows.Forms.NumericUpDown();
            this.nTextR = new System.Windows.Forms.NumericUpDown();
            this.nBkg2 = new System.Windows.Forms.NumericUpDown();
            this.nBkgB = new System.Windows.Forms.NumericUpDown();
            this.nBkgG = new System.Windows.Forms.NumericUpDown();
            this.nBkgR = new System.Windows.Forms.NumericUpDown();
            this.cbDarkTheme = new System.Windows.Forms.CheckBox();
            this.nMaxStoredHours = new System.Windows.Forms.NumericUpDown();
            this.ll4 = new System.Windows.Forms.Label();
            this.cbUseLocalDbStore = new System.Windows.Forms.CheckBox();
            this.nMaxDisplayedDebugs = new System.Windows.Forms.NumericUpDown();
            this.ll3 = new System.Windows.Forms.Label();
            this.nMaxDisplayedPackets = new System.Windows.Forms.NumericUpDown();
            this.ll2 = new System.Windows.Forms.Label();
            this.txtFont = new System.Windows.Forms.TextBox();
            this.gbRepeater = new System.Windows.Forms.GroupBox();
            this.ll1 = new System.Windows.Forms.Label();
            this.txtRawFrameUDPPort = new System.Windows.Forms.TextBox();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.ll5 = new System.Windows.Forms.Label();
            this.lblVersion = new System.Windows.Forms.Label();
            this.udpConsumerTimer = new System.Windows.Forms.Timer(this.components);
            this.dbFlushToDiskTimer = new System.Windows.Forms.Timer(this.components);
            this.tabUni = new System.Windows.Forms.TabPage();
            this.panel9 = new System.Windows.Forms.Panel();
            this.pnlUniMap = new System.Windows.Forms.Panel();
            this.uniGMap = new GMap.NET.WindowsForms.GMapControl();
            this.splitterUniMap = new System.Windows.Forms.Splitter();
            this.uniMsgBox = new System.Windows.Forms.RichTextBox();
            this.pnlUniPriority = new System.Windows.Forms.Panel();
            this.lblUniServiceAddress = new System.Windows.Forms.Label();
            this.lblUniPriority = new System.Windows.Forms.Label();
            this.uniGridSplitter = new System.Windows.Forms.Splitter();
            this.panel14 = new System.Windows.Forms.Panel();
            this.dgvUni = new System.Windows.Forms.DataGridView();
            this.UniPriority = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.UniEncoding = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.UniTime = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.mIdOrLCNoDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.UniRepetition = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.UniPartsReceived = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.UniIsPartial = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.UniStation = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.UniSatName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.UniSat = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.UniStreamId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.uniIdDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.streamIdDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.UniIsEGC = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.satDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.satNameDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.lesIdDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.lesNameDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.timeDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.timeReceivedDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.repetitionDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.messageTypeDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.priorityDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.contentTypeDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.partsReceivedDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.messageDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.isPartialDataGridViewCheckBoxColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.addressHexDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.bulletinBoardDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.areaDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.unisBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.toolStrip2 = new System.Windows.Forms.ToolStrip();
            this.btnUniAutoScroll = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.btnUniAutoLastSelect = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButton1 = new System.Windows.Forms.ToolStripButton();
            this.panel15 = new System.Windows.Forms.Panel();
            this.ll22 = new System.Windows.Forms.Label();
            this.ll14 = new System.Windows.Forms.Label();
            this.ll13 = new System.Windows.Forms.Label();
            this.ll16 = new System.Windows.Forms.Label();
            this.ll21 = new System.Windows.Forms.Label();
            this.ll23 = new System.Windows.Forms.Label();
            this.ll24 = new System.Windows.Forms.Label();
            this.ll15 = new System.Windows.Forms.Label();
            this.ll9 = new System.Windows.Forms.Label();
            this.ll12 = new System.Windows.Forms.Label();
            this.ll17 = new System.Windows.Forms.Label();
            this.ll19 = new System.Windows.Forms.Label();
            this.ll20 = new System.Windows.Forms.Label();
            this.ll10 = new System.Windows.Forms.Label();
            this.ll18 = new System.Windows.Forms.Label();
            this.ll11 = new System.Windows.Forms.Label();
            this.ll6 = new System.Windows.Forms.Label();
            this.lblUniChannelTypeAndSat = new System.Windows.Forms.Label();
            this.lblUniStatus = new System.Windows.Forms.Label();
            this.lblUniServices = new System.Windows.Forms.Label();
            this.ll8 = new System.Windows.Forms.Label();
            this.ll7 = new System.Windows.Forms.Label();
            this.tabDebug = new System.Windows.Forms.TabPage();
            this.rtbDebug = new System.Windows.Forms.RichTextBox();
            this.tsBlocks = new System.Windows.Forms.ToolStrip();
            this.btnDebugClear = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.btnDebugCopy = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator17 = new System.Windows.Forms.ToolStripSeparator();
            this.btnDebugAutoScroll = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.btnJson = new System.Windows.Forms.ToolStripButton();
            this.tabPackets = new System.Windows.Forms.TabPage();
            this.rtbPackets = new System.Windows.Forms.RichTextBox();
            this.tsPackets = new System.Windows.Forms.ToolStrip();
            this.btnPacketsClear = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator14 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButton4 = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator18 = new System.Windows.Forms.ToolStripSeparator();
            this.btnPacketsAutoScroll = new System.Windows.Forms.ToolStripButton();
            this.tC = new System.Windows.Forms.TabControl();
            this.tabInfo = new System.Windows.Forms.TabPage();
            this.rtbNotes = new System.Windows.Forms.RichTextBox();
            this.rtbInfo = new System.Windows.Forms.RichTextBox();
            this.dataGridViewTextBoxColumn16 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn17 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fontDialog1 = new System.Windows.Forms.FontDialog();
            this.dataGridViewTextBoxColumn7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn9 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn10 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn11 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn12 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn13 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn14 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn15 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn18 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn19 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn20 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn21 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn22 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.folderBrowserDialog1 = new System.Windows.Forms.FolderBrowserDialog();
            this.dataGridViewTextBoxColumn23 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn24 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panelMenu.SuspendLayout();
            this.gbFrames.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nChartDarkening)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nText2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nTextG)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nTextB)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nTextR)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nBkg2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nBkgB)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nBkgG)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nBkgR)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nMaxStoredHours)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nMaxDisplayedDebugs)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nMaxDisplayedPackets)).BeginInit();
            this.gbRepeater.SuspendLayout();
            this.tabUni.SuspendLayout();
            this.panel9.SuspendLayout();
            this.pnlUniMap.SuspendLayout();
            this.pnlUniPriority.SuspendLayout();
            this.panel14.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvUni)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.unisBindingSource)).BeginInit();
            this.toolStrip2.SuspendLayout();
            this.panel15.SuspendLayout();
            this.tabDebug.SuspendLayout();
            this.tsBlocks.SuspendLayout();
            this.tabPackets.SuspendLayout();
            this.tsPackets.SuspendLayout();
            this.tC.SuspendLayout();
            this.tabInfo.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.panel1.Controls.Add(this.panel2);
            this.panel1.Controls.Add(this.btnCredits);
            this.panel1.Controls.Add(this.btnStart);
            this.panel1.Controls.Add(this.btnMenu);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1081, 77);
            this.panel1.TabIndex = 4;
            // 
            // panel2
            // 
            this.panel2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel2.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.panel2.Controls.Add(this.lbl6a);
            this.panel2.Controls.Add(this.lbl3a);
            this.panel2.Controls.Add(this.lbl6);
            this.panel2.Controls.Add(this.lbl3);
            this.panel2.Controls.Add(this.lbl8a);
            this.panel2.Controls.Add(this.lbl5a);
            this.panel2.Controls.Add(this.lbl2a);
            this.panel2.Controls.Add(this.lbl8);
            this.panel2.Controls.Add(this.lbl5);
            this.panel2.Controls.Add(this.lbl2);
            this.panel2.Controls.Add(this.lbl9a);
            this.panel2.Controls.Add(this.lbl7a);
            this.panel2.Controls.Add(this.lbl4a);
            this.panel2.Controls.Add(this.lbl1a);
            this.panel2.Controls.Add(this.lbl0a);
            this.panel2.Controls.Add(this.lbl9);
            this.panel2.Controls.Add(this.lbl7);
            this.panel2.Controls.Add(this.lbl4);
            this.panel2.Controls.Add(this.lbl1);
            this.panel2.Controls.Add(this.lbl0);
            this.panel2.Location = new System.Drawing.Point(158, 3);
            this.panel2.Name = "panel2";
            this.panel2.Padding = new System.Windows.Forms.Padding(1);
            this.panel2.Size = new System.Drawing.Size(765, 71);
            this.panel2.TabIndex = 12;
            // 
            // lbl6a
            // 
            this.lbl6a.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lbl6a.BackColor = System.Drawing.SystemColors.ControlLight;
            this.lbl6a.ForeColor = System.Drawing.Color.Navy;
            this.lbl6a.Location = new System.Drawing.Point(216, 54);
            this.lbl6a.Name = "lbl6a";
            this.lbl6a.Size = new System.Drawing.Size(91, 13);
            this.lbl6a.TabIndex = 19;
            this.lbl6a.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lbl3a
            // 
            this.lbl3a.BackColor = System.Drawing.SystemColors.ControlLight;
            this.lbl3a.ForeColor = System.Drawing.Color.Navy;
            this.lbl3a.Location = new System.Drawing.Point(4, 54);
            this.lbl3a.Name = "lbl3a";
            this.lbl3a.Size = new System.Drawing.Size(91, 13);
            this.lbl3a.TabIndex = 18;
            this.lbl3a.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lbl6
            // 
            this.lbl6.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lbl6.AutoEllipsis = true;
            this.lbl6.BackColor = System.Drawing.SystemColors.ControlLight;
            this.lbl6.Location = new System.Drawing.Point(309, 54);
            this.lbl6.Name = "lbl6";
            this.lbl6.Size = new System.Drawing.Size(179, 13);
            this.lbl6.TabIndex = 17;
            this.lbl6.Tag = "0";
            // 
            // lbl3
            // 
            this.lbl3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lbl3.AutoEllipsis = true;
            this.lbl3.BackColor = System.Drawing.SystemColors.ControlLight;
            this.lbl3.Location = new System.Drawing.Point(97, 54);
            this.lbl3.Name = "lbl3";
            this.lbl3.Size = new System.Drawing.Size(117, 13);
            this.lbl3.TabIndex = 16;
            this.lbl3.Tag = "0";
            // 
            // lbl8a
            // 
            this.lbl8a.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lbl8a.BackColor = System.Drawing.SystemColors.ControlLight;
            this.lbl8a.ForeColor = System.Drawing.Color.Navy;
            this.lbl8a.Location = new System.Drawing.Point(490, 37);
            this.lbl8a.Name = "lbl8a";
            this.lbl8a.Size = new System.Drawing.Size(91, 13);
            this.lbl8a.TabIndex = 15;
            this.lbl8a.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lbl5a
            // 
            this.lbl5a.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lbl5a.BackColor = System.Drawing.SystemColors.ControlLight;
            this.lbl5a.ForeColor = System.Drawing.Color.Navy;
            this.lbl5a.Location = new System.Drawing.Point(216, 37);
            this.lbl5a.Name = "lbl5a";
            this.lbl5a.Size = new System.Drawing.Size(91, 13);
            this.lbl5a.TabIndex = 14;
            this.lbl5a.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lbl2a
            // 
            this.lbl2a.BackColor = System.Drawing.SystemColors.ControlLight;
            this.lbl2a.ForeColor = System.Drawing.Color.Navy;
            this.lbl2a.Location = new System.Drawing.Point(4, 37);
            this.lbl2a.Name = "lbl2a";
            this.lbl2a.Size = new System.Drawing.Size(91, 13);
            this.lbl2a.TabIndex = 13;
            this.lbl2a.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lbl8
            // 
            this.lbl8.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lbl8.AutoEllipsis = true;
            this.lbl8.BackColor = System.Drawing.SystemColors.ControlLight;
            this.lbl8.Location = new System.Drawing.Point(583, 37);
            this.lbl8.Name = "lbl8";
            this.lbl8.Size = new System.Drawing.Size(178, 13);
            this.lbl8.TabIndex = 12;
            this.lbl8.Tag = "0";
            // 
            // lbl5
            // 
            this.lbl5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lbl5.AutoEllipsis = true;
            this.lbl5.BackColor = System.Drawing.SystemColors.ControlLight;
            this.lbl5.Location = new System.Drawing.Point(309, 37);
            this.lbl5.Name = "lbl5";
            this.lbl5.Size = new System.Drawing.Size(179, 13);
            this.lbl5.TabIndex = 11;
            this.lbl5.Tag = "0";
            // 
            // lbl2
            // 
            this.lbl2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lbl2.AutoEllipsis = true;
            this.lbl2.BackColor = System.Drawing.SystemColors.ControlLight;
            this.lbl2.Location = new System.Drawing.Point(97, 37);
            this.lbl2.Name = "lbl2";
            this.lbl2.Size = new System.Drawing.Size(117, 13);
            this.lbl2.TabIndex = 10;
            this.lbl2.Tag = "0";
            // 
            // lbl9a
            // 
            this.lbl9a.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lbl9a.BackColor = System.Drawing.SystemColors.ControlLight;
            this.lbl9a.ForeColor = System.Drawing.Color.Navy;
            this.lbl9a.Location = new System.Drawing.Point(490, 54);
            this.lbl9a.Name = "lbl9a";
            this.lbl9a.Size = new System.Drawing.Size(91, 13);
            this.lbl9a.TabIndex = 9;
            this.lbl9a.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lbl7a
            // 
            this.lbl7a.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lbl7a.BackColor = System.Drawing.SystemColors.ControlLight;
            this.lbl7a.ForeColor = System.Drawing.Color.Navy;
            this.lbl7a.Location = new System.Drawing.Point(490, 20);
            this.lbl7a.Name = "lbl7a";
            this.lbl7a.Size = new System.Drawing.Size(91, 13);
            this.lbl7a.TabIndex = 8;
            this.lbl7a.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lbl4a
            // 
            this.lbl4a.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lbl4a.BackColor = System.Drawing.SystemColors.ControlLight;
            this.lbl4a.ForeColor = System.Drawing.Color.Navy;
            this.lbl4a.Location = new System.Drawing.Point(216, 20);
            this.lbl4a.Name = "lbl4a";
            this.lbl4a.Size = new System.Drawing.Size(91, 13);
            this.lbl4a.TabIndex = 7;
            this.lbl4a.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lbl1a
            // 
            this.lbl1a.BackColor = System.Drawing.SystemColors.ControlLight;
            this.lbl1a.ForeColor = System.Drawing.Color.Navy;
            this.lbl1a.Location = new System.Drawing.Point(4, 20);
            this.lbl1a.Name = "lbl1a";
            this.lbl1a.Size = new System.Drawing.Size(91, 13);
            this.lbl1a.TabIndex = 6;
            this.lbl1a.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lbl0a
            // 
            this.lbl0a.BackColor = System.Drawing.SystemColors.ControlLight;
            this.lbl0a.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.lbl0a.ForeColor = System.Drawing.Color.Navy;
            this.lbl0a.Location = new System.Drawing.Point(4, 3);
            this.lbl0a.Name = "lbl0a";
            this.lbl0a.Size = new System.Drawing.Size(91, 13);
            this.lbl0a.TabIndex = 5;
            this.lbl0a.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lbl9
            // 
            this.lbl9.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lbl9.AutoEllipsis = true;
            this.lbl9.BackColor = System.Drawing.SystemColors.ControlLight;
            this.lbl9.Location = new System.Drawing.Point(583, 54);
            this.lbl9.Name = "lbl9";
            this.lbl9.Size = new System.Drawing.Size(178, 13);
            this.lbl9.TabIndex = 4;
            this.lbl9.Tag = "0";
            // 
            // lbl7
            // 
            this.lbl7.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lbl7.AutoEllipsis = true;
            this.lbl7.BackColor = System.Drawing.SystemColors.ControlLight;
            this.lbl7.Location = new System.Drawing.Point(583, 20);
            this.lbl7.Name = "lbl7";
            this.lbl7.Size = new System.Drawing.Size(178, 13);
            this.lbl7.TabIndex = 3;
            this.lbl7.Tag = "0";
            // 
            // lbl4
            // 
            this.lbl4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lbl4.AutoEllipsis = true;
            this.lbl4.BackColor = System.Drawing.SystemColors.ControlLight;
            this.lbl4.Location = new System.Drawing.Point(309, 20);
            this.lbl4.Name = "lbl4";
            this.lbl4.Size = new System.Drawing.Size(179, 13);
            this.lbl4.TabIndex = 2;
            this.lbl4.Tag = "0";
            // 
            // lbl1
            // 
            this.lbl1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lbl1.AutoEllipsis = true;
            this.lbl1.BackColor = System.Drawing.SystemColors.ControlLight;
            this.lbl1.Location = new System.Drawing.Point(97, 20);
            this.lbl1.Name = "lbl1";
            this.lbl1.Size = new System.Drawing.Size(117, 13);
            this.lbl1.TabIndex = 1;
            this.lbl1.Tag = "0";
            // 
            // lbl0
            // 
            this.lbl0.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lbl0.AutoEllipsis = true;
            this.lbl0.BackColor = System.Drawing.SystemColors.ControlLight;
            this.lbl0.Location = new System.Drawing.Point(97, 3);
            this.lbl0.Name = "lbl0";
            this.lbl0.Size = new System.Drawing.Size(664, 13);
            this.lbl0.TabIndex = 0;
            this.lbl0.Tag = "0";
            this.lbl0.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // btnCredits
            // 
            this.btnCredits.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCredits.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.btnCredits.FlatAppearance.BorderColor = System.Drawing.SystemColors.ButtonFace;
            this.btnCredits.FlatAppearance.BorderSize = 0;
            this.btnCredits.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCredits.Image = ((System.Drawing.Image)(resources.GetObject("btnCredits.Image")));
            this.btnCredits.Location = new System.Drawing.Point(1008, 12);
            this.btnCredits.Name = "btnCredits";
            this.btnCredits.Size = new System.Drawing.Size(61, 50);
            this.btnCredits.TabIndex = 11;
            this.btnCredits.TabStop = false;
            this.btnCredits.Tag = "";
            this.btnCredits.UseVisualStyleBackColor = false;
            this.btnCredits.Click += new System.EventHandler(this.btnCredits_Click);
            // 
            // btnStart
            // 
            this.btnStart.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.btnStart.FlatAppearance.BorderColor = System.Drawing.SystemColors.ButtonFace;
            this.btnStart.FlatAppearance.BorderSize = 0;
            this.btnStart.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnStart.Image = ((System.Drawing.Image)(resources.GetObject("btnStart.Image")));
            this.btnStart.Location = new System.Drawing.Point(68, 12);
            this.btnStart.Name = "btnStart";
            this.btnStart.Size = new System.Drawing.Size(61, 50);
            this.btnStart.TabIndex = 10;
            this.btnStart.TabStop = false;
            this.btnStart.Tag = "start";
            this.btnStart.UseVisualStyleBackColor = false;
            this.btnStart.Click += new System.EventHandler(this.btnStart_Click);
            // 
            // btnMenu
            // 
            this.btnMenu.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.btnMenu.FlatAppearance.BorderColor = System.Drawing.SystemColors.ButtonFace;
            this.btnMenu.FlatAppearance.BorderSize = 0;
            this.btnMenu.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnMenu.Image = ((System.Drawing.Image)(resources.GetObject("btnMenu.Image")));
            this.btnMenu.Location = new System.Drawing.Point(9, 12);
            this.btnMenu.Name = "btnMenu";
            this.btnMenu.Size = new System.Drawing.Size(61, 50);
            this.btnMenu.TabIndex = 0;
            this.btnMenu.TabStop = false;
            this.btnMenu.Tag = "menu-close";
            this.btnMenu.UseVisualStyleBackColor = false;
            this.btnMenu.Click += new System.EventHandler(this.btnMenu_Click);
            // 
            // panelMenu
            // 
            this.panelMenu.AutoScroll = true;
            this.panelMenu.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.panelMenu.Controls.Add(this.gbFrames);
            this.panelMenu.Controls.Add(this.groupBox1);
            this.panelMenu.Controls.Add(this.gbRepeater);
            this.panelMenu.Dock = System.Windows.Forms.DockStyle.Left;
            this.panelMenu.Location = new System.Drawing.Point(0, 77);
            this.panelMenu.Name = "panelMenu";
            this.panelMenu.Padding = new System.Windows.Forms.Padding(2);
            this.panelMenu.Size = new System.Drawing.Size(215, 557);
            this.panelMenu.TabIndex = 5;
            // 
            // gbFrames
            // 
            this.gbFrames.Controls.Add(this.cbA8);
            this.gbFrames.Controls.Add(this.cbA3);
            this.gbFrames.Controls.Add(this.cb81);
            this.gbFrames.Controls.Add(this.cbAD);
            this.gbFrames.Controls.Add(this.cb91);
            this.gbFrames.Controls.Add(this.cbA0);
            this.gbFrames.Controls.Add(this.cbAC);
            this.gbFrames.Controls.Add(this.cb08);
            this.gbFrames.Controls.Add(this.cb2A);
            this.gbFrames.Controls.Add(this.cb27);
            this.gbFrames.Controls.Add(this.cb9A);
            this.gbFrames.Controls.Add(this.cb83);
            this.gbFrames.Controls.Add(this.cbBE);
            this.gbFrames.Controls.Add(this.cbBD);
            this.gbFrames.Controls.Add(this.cbB2);
            this.gbFrames.Controls.Add(this.cbB1);
            this.gbFrames.Controls.Add(this.cbAB);
            this.gbFrames.Controls.Add(this.cbAA);
            this.gbFrames.Controls.Add(this.cb7D);
            this.gbFrames.Controls.Add(this.cb6C);
            this.gbFrames.Controls.Add(this.cb92);
            this.gbFrames.Dock = System.Windows.Forms.DockStyle.Top;
            this.gbFrames.Location = new System.Drawing.Point(2, 506);
            this.gbFrames.Name = "gbFrames";
            this.gbFrames.Size = new System.Drawing.Size(194, 508);
            this.gbFrames.TabIndex = 17;
            this.gbFrames.TabStop = false;
            this.gbFrames.Text = "Packet Decoding";
            // 
            // cbA8
            // 
            this.cbA8.AutoSize = true;
            this.cbA8.Checked = true;
            this.cbA8.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbA8.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cbA8.Location = new System.Drawing.Point(15, 484);
            this.cbA8.Name = "cbA8";
            this.cbA8.Size = new System.Drawing.Size(103, 17);
            this.cbA8.TabIndex = 110;
            this.cbA8.Text = "A8 - Confirmation";
            this.cbA8.UseVisualStyleBackColor = true;
            // 
            // cbA3
            // 
            this.cbA3.AutoSize = true;
            this.cbA3.Checked = true;
            this.cbA3.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbA3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cbA3.Location = new System.Drawing.Point(15, 461);
            this.cbA3.Name = "cbA3";
            this.cbA3.Size = new System.Drawing.Size(110, 17);
            this.cbA3.TabIndex = 109;
            this.cbA3.Text = "A3 - Individual Poll";
            this.cbA3.UseVisualStyleBackColor = true;
            // 
            // cb81
            // 
            this.cb81.AutoSize = true;
            this.cb81.Checked = true;
            this.cb81.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cb81.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cb81.Location = new System.Drawing.Point(15, 438);
            this.cb81.Name = "cb81";
            this.cb81.Size = new System.Drawing.Size(116, 17);
            this.cb81.TabIndex = 108;
            this.cb81.Text = "81 - Announcement";
            this.cb81.UseVisualStyleBackColor = true;
            // 
            // cbAD
            // 
            this.cbAD.AutoCheck = false;
            this.cbAD.AutoSize = true;
            this.cbAD.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cbAD.Location = new System.Drawing.Point(15, 415);
            this.cbAD.Name = "cbAD";
            this.cbAD.Size = new System.Drawing.Size(146, 17);
            this.cbAD.TabIndex = 107;
            this.cbAD.Text = "AD - Distress Test Results";
            this.cbAD.UseVisualStyleBackColor = true;
            // 
            // cb91
            // 
            this.cb91.AutoCheck = false;
            this.cb91.AutoSize = true;
            this.cb91.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cb91.Location = new System.Drawing.Point(15, 392);
            this.cb91.Name = "cb91";
            this.cb91.Size = new System.Drawing.Size(130, 17);
            this.cb91.TabIndex = 106;
            this.cb91.Text = "91 - Distress Alert Ack.";
            this.cb91.UseVisualStyleBackColor = true;
            // 
            // cbA0
            // 
            this.cbA0.AutoCheck = false;
            this.cbA0.AutoSize = true;
            this.cbA0.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cbA0.Location = new System.Drawing.Point(15, 369);
            this.cbA0.Name = "cbA0";
            this.cbA0.Size = new System.Drawing.Size(149, 17);
            this.cbA0.TabIndex = 105;
            this.cbA0.Text = "A0 - Distress Test Request";
            this.cbA0.UseVisualStyleBackColor = true;
            // 
            // cbAC
            // 
            this.cbAC.AutoCheck = false;
            this.cbAC.AutoSize = true;
            this.cbAC.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cbAC.Location = new System.Drawing.Point(15, 346);
            this.cbAC.Name = "cbAC";
            this.cbAC.Size = new System.Drawing.Size(119, 17);
            this.cbAC.TabIndex = 104;
            this.cbAC.Text = "AC - Request Status";
            this.cbAC.UseVisualStyleBackColor = true;
            // 
            // cb08
            // 
            this.cb08.AutoSize = true;
            this.cb08.Checked = true;
            this.cb08.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cb08.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.cb08.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cb08.Location = new System.Drawing.Point(15, 323);
            this.cb08.Name = "cb08";
            this.cb08.Size = new System.Drawing.Size(109, 17);
            this.cb08.TabIndex = 103;
            this.cb08.Text = "08 - Ack. Request";
            this.cb08.UseVisualStyleBackColor = true;
            // 
            // cb2A
            // 
            this.cb2A.AutoSize = true;
            this.cb2A.Checked = true;
            this.cb2A.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cb2A.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cb2A.Location = new System.Drawing.Point(15, 300);
            this.cb2A.Name = "cb2A";
            this.cb2A.Size = new System.Drawing.Size(155, 17);
            this.cb2A.TabIndex = 102;
            this.cb2A.Text = "2A - Inbound Message Ack.";
            this.cb2A.UseVisualStyleBackColor = true;
            // 
            // cb27
            // 
            this.cb27.AutoSize = true;
            this.cb27.Checked = true;
            this.cb27.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cb27.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cb27.Location = new System.Drawing.Point(15, 277);
            this.cb27.Name = "cb27";
            this.cb27.Size = new System.Drawing.Size(147, 17);
            this.cb27.TabIndex = 101;
            this.cb27.Text = "27 - Logical Channel Clear";
            this.cb27.UseVisualStyleBackColor = true;
            // 
            // cb9A
            // 
            this.cb9A.AutoCheck = false;
            this.cb9A.AutoSize = true;
            this.cb9A.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cb9A.Location = new System.Drawing.Point(15, 254);
            this.cb9A.Name = "cb9A";
            this.cb9A.Size = new System.Drawing.Size(168, 17);
            this.cb9A.TabIndex = 100;
            this.cb9A.Text = "9A - Enhanced Data Rpt. Ack.";
            this.cb9A.UseVisualStyleBackColor = true;
            // 
            // cb83
            // 
            this.cb83.AutoSize = true;
            this.cb83.Checked = true;
            this.cb83.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cb83.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cb83.Location = new System.Drawing.Point(15, 231);
            this.cb83.Name = "cb83";
            this.cb83.Size = new System.Drawing.Size(155, 17);
            this.cb83.TabIndex = 99;
            this.cb83.Text = "83 - Logical Channel Asgmt.";
            this.cb83.UseVisualStyleBackColor = true;
            // 
            // cbBE
            // 
            this.cbBE.AutoSize = true;
            this.cbBE.Checked = true;
            this.cbBE.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbBE.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cbBE.Location = new System.Drawing.Point(15, 208);
            this.cbBE.Name = "cbBE";
            this.cbBE.Size = new System.Drawing.Size(168, 17);
            this.cbBE.TabIndex = 98;
            this.cbBE.Text = "BE - Multiframe Message Cntd.";
            this.cbBE.UseVisualStyleBackColor = true;
            // 
            // cbBD
            // 
            this.cbBD.AutoSize = true;
            this.cbBD.Checked = true;
            this.cbBD.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbBD.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cbBD.Location = new System.Drawing.Point(15, 185);
            this.cbBD.Name = "cbBD";
            this.cbBD.Size = new System.Drawing.Size(141, 17);
            this.cbBD.TabIndex = 97;
            this.cbBD.Text = "BD - Multiframe Message";
            this.cbBD.UseVisualStyleBackColor = true;
            // 
            // cbB2
            // 
            this.cbB2.AutoSize = true;
            this.cbB2.Checked = true;
            this.cbB2.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbB2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cbB2.Location = new System.Drawing.Point(15, 162);
            this.cbB2.Name = "cbB2";
            this.cbB2.Size = new System.Drawing.Size(67, 17);
            this.cbB2.TabIndex = 96;
            this.cbB2.Text = "B2 - EGC";
            this.cbB2.UseVisualStyleBackColor = true;
            // 
            // cbB1
            // 
            this.cbB1.AutoSize = true;
            this.cbB1.Checked = true;
            this.cbB1.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbB1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cbB1.Location = new System.Drawing.Point(15, 139);
            this.cbB1.Name = "cbB1";
            this.cbB1.Size = new System.Drawing.Size(67, 17);
            this.cbB1.TabIndex = 95;
            this.cbB1.Text = "B1 - EGC";
            this.cbB1.UseVisualStyleBackColor = true;
            // 
            // cbAB
            // 
            this.cbAB.AutoSize = true;
            this.cbAB.Checked = true;
            this.cbAB.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbAB.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cbAB.Location = new System.Drawing.Point(15, 116);
            this.cbAB.Name = "cbAB";
            this.cbAB.Size = new System.Drawing.Size(85, 17);
            this.cbAB.TabIndex = 94;
            this.cbAB.Text = "AB - LES List";
            this.cbAB.UseVisualStyleBackColor = true;
            // 
            // cbAA
            // 
            this.cbAA.AutoSize = true;
            this.cbAA.Checked = true;
            this.cbAA.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbAA.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cbAA.Location = new System.Drawing.Point(15, 93);
            this.cbAA.Name = "cbAA";
            this.cbAA.Size = new System.Drawing.Size(89, 17);
            this.cbAA.TabIndex = 93;
            this.cbAA.Text = "AA - Message";
            this.cbAA.UseVisualStyleBackColor = true;
            // 
            // cb7D
            // 
            this.cb7D.AutoSize = true;
            this.cb7D.Checked = true;
            this.cb7D.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cb7D.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cb7D.Location = new System.Drawing.Point(15, 47);
            this.cb7D.Name = "cb7D";
            this.cb7D.Size = new System.Drawing.Size(111, 17);
            this.cb7D.TabIndex = 92;
            this.cb7D.Text = "7D - Bulletin Board";
            this.cb7D.UseVisualStyleBackColor = true;
            // 
            // cb6C
            // 
            this.cb6C.AutoSize = true;
            this.cb6C.Checked = true;
            this.cb6C.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cb6C.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cb6C.Location = new System.Drawing.Point(15, 24);
            this.cb6C.Name = "cb6C";
            this.cb6C.Size = new System.Drawing.Size(132, 17);
            this.cb6C.TabIndex = 91;
            this.cb6C.Text = "6C - Signalling Channel";
            this.cb6C.UseVisualStyleBackColor = true;
            // 
            // cb92
            // 
            this.cb92.AutoSize = true;
            this.cb92.Checked = true;
            this.cb92.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cb92.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cb92.Location = new System.Drawing.Point(15, 70);
            this.cb92.Name = "cb92";
            this.cb92.Size = new System.Drawing.Size(161, 17);
            this.cb92.TabIndex = 90;
            this.cb92.Text = "92 - Login Acknowledgement";
            this.cb92.UseVisualStyleBackColor = true;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.cbLogRaw);
            this.groupBox1.Controls.Add(this.cbLogMessages);
            this.groupBox1.Controls.Add(this.ll29);
            this.groupBox1.Controls.Add(this.btnLogFolder);
            this.groupBox1.Controls.Add(this.txtLogFolder);
            this.groupBox1.Controls.Add(this.cbSavePackets);
            this.groupBox1.Controls.Add(this.ll28);
            this.groupBox1.Controls.Add(this.button1);
            this.groupBox1.Controls.Add(this.nChartDarkening);
            this.groupBox1.Controls.Add(this.ll25);
            this.groupBox1.Controls.Add(this.ll27);
            this.groupBox1.Controls.Add(this.ll26);
            this.groupBox1.Controls.Add(this.nText2);
            this.groupBox1.Controls.Add(this.nTextG);
            this.groupBox1.Controls.Add(this.nTextB);
            this.groupBox1.Controls.Add(this.nTextR);
            this.groupBox1.Controls.Add(this.nBkg2);
            this.groupBox1.Controls.Add(this.nBkgB);
            this.groupBox1.Controls.Add(this.nBkgG);
            this.groupBox1.Controls.Add(this.nBkgR);
            this.groupBox1.Controls.Add(this.cbDarkTheme);
            this.groupBox1.Controls.Add(this.nMaxStoredHours);
            this.groupBox1.Controls.Add(this.ll4);
            this.groupBox1.Controls.Add(this.cbUseLocalDbStore);
            this.groupBox1.Controls.Add(this.nMaxDisplayedDebugs);
            this.groupBox1.Controls.Add(this.ll3);
            this.groupBox1.Controls.Add(this.nMaxDisplayedPackets);
            this.groupBox1.Controls.Add(this.ll2);
            this.groupBox1.Controls.Add(this.txtFont);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox1.Location = new System.Drawing.Point(2, 86);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(194, 420);
            this.groupBox1.TabIndex = 18;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Settings";
            // 
            // cbLogRaw
            // 
            this.cbLogRaw.AutoSize = true;
            this.cbLogRaw.Enabled = false;
            this.cbLogRaw.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cbLogRaw.Location = new System.Drawing.Point(143, 350);
            this.cbLogRaw.Name = "cbLogRaw";
            this.cbLogRaw.Size = new System.Drawing.Size(45, 17);
            this.cbLogRaw.TabIndex = 118;
            this.cbLogRaw.Text = "Raw";
            this.cbLogRaw.UseVisualStyleBackColor = true;
            this.cbLogRaw.CheckedChanged += new System.EventHandler(this.cbLogRaw_CheckedChanged);
            // 
            // cbLogMessages
            // 
            this.cbLogMessages.AutoSize = true;
            this.cbLogMessages.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cbLogMessages.Location = new System.Drawing.Point(15, 350);
            this.cbLogMessages.Name = "cbLogMessages";
            this.cbLogMessages.Size = new System.Drawing.Size(92, 17);
            this.cbLogMessages.TabIndex = 117;
            this.cbLogMessages.Text = "Log Messages";
            this.cbLogMessages.UseVisualStyleBackColor = true;
            this.cbLogMessages.CheckedChanged += new System.EventHandler(this.cbLogMessages_CheckedChanged);
            // 
            // ll29
            // 
            this.ll29.AutoSize = true;
            this.ll29.Location = new System.Drawing.Point(19, 374);
            this.ll29.Name = "ll29";
            this.ll29.Size = new System.Drawing.Size(39, 13);
            this.ll29.TabIndex = 114;
            this.ll29.Text = "Folder:";
            // 
            // btnLogFolder
            // 
            this.btnLogFolder.Enabled = false;
            this.btnLogFolder.Location = new System.Drawing.Point(164, 389);
            this.btnLogFolder.Name = "btnLogFolder";
            this.btnLogFolder.Size = new System.Drawing.Size(27, 22);
            this.btnLogFolder.TabIndex = 116;
            this.btnLogFolder.Text = "...";
            this.btnLogFolder.UseVisualStyleBackColor = true;
            this.btnLogFolder.Click += new System.EventHandler(this.btnLogFolder_Click);
            // 
            // txtLogFolder
            // 
            this.txtLogFolder.Enabled = false;
            this.txtLogFolder.Location = new System.Drawing.Point(22, 390);
            this.txtLogFolder.Name = "txtLogFolder";
            this.txtLogFolder.Size = new System.Drawing.Size(140, 20);
            this.txtLogFolder.TabIndex = 115;
            this.txtLogFolder.TextChanged += new System.EventHandler(this.txtLogFolder_TextChanged);
            // 
            // cbSavePackets
            // 
            this.cbSavePackets.AutoSize = true;
            this.cbSavePackets.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cbSavePackets.Location = new System.Drawing.Point(15, 115);
            this.cbSavePackets.Name = "cbSavePackets";
            this.cbSavePackets.Size = new System.Drawing.Size(132, 17);
            this.cbSavePackets.TabIndex = 113;
            this.cbSavePackets.Text = "Save Rx Frames to File";
            this.cbSavePackets.UseVisualStyleBackColor = true;
            // 
            // ll28
            // 
            this.ll28.AutoSize = true;
            this.ll28.Location = new System.Drawing.Point(12, 148);
            this.ll28.Name = "ll28";
            this.ll28.Size = new System.Drawing.Size(114, 13);
            this.ll28.TabIndex = 98;
            this.ll28.Text = "Message Display Font:";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(163, 163);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(27, 22);
            this.button1.TabIndex = 100;
            this.button1.Text = "...";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // nChartDarkening
            // 
            this.nChartDarkening.Location = new System.Drawing.Point(114, 315);
            this.nChartDarkening.Maximum = new decimal(new int[] {
            240,
            0,
            0,
            0});
            this.nChartDarkening.Name = "nChartDarkening";
            this.nChartDarkening.Size = new System.Drawing.Size(74, 20);
            this.nChartDarkening.TabIndex = 112;
            this.nChartDarkening.Value = new decimal(new int[] {
            50,
            0,
            0,
            0});
            this.nChartDarkening.ValueChanged += new System.EventHandler(this.nR_ValueChanged);
            // 
            // ll25
            // 
            this.ll25.AutoSize = true;
            this.ll25.Location = new System.Drawing.Point(20, 317);
            this.ll25.Name = "ll25";
            this.ll25.Size = new System.Drawing.Size(85, 13);
            this.ll25.TabIndex = 111;
            this.ll25.Text = "Chart darkening:";
            // 
            // ll27
            // 
            this.ll27.AutoSize = true;
            this.ll27.Location = new System.Drawing.Point(20, 269);
            this.ll27.Name = "ll27";
            this.ll27.Size = new System.Drawing.Size(31, 13);
            this.ll27.TabIndex = 106;
            this.ll27.Text = "Text:";
            // 
            // ll26
            // 
            this.ll26.AutoSize = true;
            this.ll26.Location = new System.Drawing.Point(20, 220);
            this.ll26.Name = "ll26";
            this.ll26.Size = new System.Drawing.Size(29, 13);
            this.ll26.TabIndex = 102;
            this.ll26.Text = "Bkg:";
            // 
            // nText2
            // 
            this.nText2.Location = new System.Drawing.Point(147, 291);
            this.nText2.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.nText2.Minimum = new decimal(new int[] {
            255,
            0,
            0,
            -2147483648});
            this.nText2.Name = "nText2";
            this.nText2.Size = new System.Drawing.Size(41, 20);
            this.nText2.TabIndex = 110;
            this.nText2.Value = new decimal(new int[] {
            110,
            0,
            0,
            0});
            this.nText2.ValueChanged += new System.EventHandler(this.nR_ValueChanged);
            // 
            // nTextG
            // 
            this.nTextG.Location = new System.Drawing.Point(100, 267);
            this.nTextG.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.nTextG.Name = "nTextG";
            this.nTextG.Size = new System.Drawing.Size(41, 20);
            this.nTextG.TabIndex = 108;
            this.nTextG.Value = new decimal(new int[] {
            235,
            0,
            0,
            0});
            this.nTextG.ValueChanged += new System.EventHandler(this.nR_ValueChanged);
            // 
            // nTextB
            // 
            this.nTextB.Location = new System.Drawing.Point(147, 267);
            this.nTextB.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.nTextB.Name = "nTextB";
            this.nTextB.Size = new System.Drawing.Size(41, 20);
            this.nTextB.TabIndex = 109;
            this.nTextB.Value = new decimal(new int[] {
            90,
            0,
            0,
            0});
            this.nTextB.ValueChanged += new System.EventHandler(this.nR_ValueChanged);
            // 
            // nTextR
            // 
            this.nTextR.Location = new System.Drawing.Point(53, 267);
            this.nTextR.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.nTextR.Name = "nTextR";
            this.nTextR.Size = new System.Drawing.Size(41, 20);
            this.nTextR.TabIndex = 107;
            this.nTextR.Value = new decimal(new int[] {
            175,
            0,
            0,
            0});
            this.nTextR.ValueChanged += new System.EventHandler(this.nR_ValueChanged);
            // 
            // nBkg2
            // 
            this.nBkg2.Location = new System.Drawing.Point(147, 242);
            this.nBkg2.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.nBkg2.Minimum = new decimal(new int[] {
            255,
            0,
            0,
            -2147483648});
            this.nBkg2.Name = "nBkg2";
            this.nBkg2.Size = new System.Drawing.Size(41, 20);
            this.nBkg2.TabIndex = 105;
            this.nBkg2.Value = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.nBkg2.ValueChanged += new System.EventHandler(this.nR_ValueChanged);
            // 
            // nBkgB
            // 
            this.nBkgB.Location = new System.Drawing.Point(147, 218);
            this.nBkgB.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.nBkgB.Name = "nBkgB";
            this.nBkgB.Size = new System.Drawing.Size(41, 20);
            this.nBkgB.TabIndex = 104;
            this.nBkgB.ValueChanged += new System.EventHandler(this.nR_ValueChanged);
            // 
            // nBkgG
            // 
            this.nBkgG.Location = new System.Drawing.Point(100, 218);
            this.nBkgG.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.nBkgG.Name = "nBkgG";
            this.nBkgG.Size = new System.Drawing.Size(41, 20);
            this.nBkgG.TabIndex = 103;
            this.nBkgG.Value = new decimal(new int[] {
            60,
            0,
            0,
            0});
            this.nBkgG.ValueChanged += new System.EventHandler(this.nR_ValueChanged);
            // 
            // nBkgR
            // 
            this.nBkgR.Location = new System.Drawing.Point(53, 218);
            this.nBkgR.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.nBkgR.Name = "nBkgR";
            this.nBkgR.Size = new System.Drawing.Size(41, 20);
            this.nBkgR.TabIndex = 102;
            this.nBkgR.ValueChanged += new System.EventHandler(this.nR_ValueChanged);
            // 
            // cbDarkTheme
            // 
            this.cbDarkTheme.AutoSize = true;
            this.cbDarkTheme.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cbDarkTheme.Location = new System.Drawing.Point(15, 195);
            this.cbDarkTheme.Name = "cbDarkTheme";
            this.cbDarkTheme.Size = new System.Drawing.Size(82, 17);
            this.cbDarkTheme.TabIndex = 101;
            this.cbDarkTheme.Text = "Dark Theme";
            this.cbDarkTheme.UseVisualStyleBackColor = true;
            this.cbDarkTheme.CheckedChanged += new System.EventHandler(this.cbDarkTheme_CheckedChanged);
            // 
            // nMaxStoredHours
            // 
            this.nMaxStoredHours.Increment = new decimal(new int[] {
            6,
            0,
            0,
            0});
            this.nMaxStoredHours.Location = new System.Drawing.Point(91, 66);
            this.nMaxStoredHours.Maximum = new decimal(new int[] {
            168,
            0,
            0,
            0});
            this.nMaxStoredHours.Minimum = new decimal(new int[] {
            6,
            0,
            0,
            0});
            this.nMaxStoredHours.Name = "nMaxStoredHours";
            this.nMaxStoredHours.Size = new System.Drawing.Size(99, 20);
            this.nMaxStoredHours.TabIndex = 96;
            this.nMaxStoredHours.Value = new decimal(new int[] {
            36,
            0,
            0,
            0});
            // 
            // ll4
            // 
            this.ll4.AutoSize = true;
            this.ll4.Location = new System.Drawing.Point(19, 68);
            this.ll4.Name = "ll4";
            this.ll4.Size = new System.Drawing.Size(55, 13);
            this.ll4.TabIndex = 95;
            this.ll4.Text = "Db Hours:";
            // 
            // cbUseLocalDbStore
            // 
            this.cbUseLocalDbStore.AutoSize = true;
            this.cbUseLocalDbStore.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cbUseLocalDbStore.Location = new System.Drawing.Point(15, 92);
            this.cbUseLocalDbStore.Name = "cbUseLocalDbStore";
            this.cbUseLocalDbStore.Size = new System.Drawing.Size(151, 17);
            this.cbUseLocalDbStore.TabIndex = 97;
            this.cbUseLocalDbStore.Text = "Save Packets to Database";
            this.cbUseLocalDbStore.UseVisualStyleBackColor = true;
            // 
            // nMaxDisplayedDebugs
            // 
            this.nMaxDisplayedDebugs.Increment = new decimal(new int[] {
            20,
            0,
            0,
            0});
            this.nMaxDisplayedDebugs.Location = new System.Drawing.Point(91, 42);
            this.nMaxDisplayedDebugs.Maximum = new decimal(new int[] {
            15000,
            0,
            0,
            0});
            this.nMaxDisplayedDebugs.Minimum = new decimal(new int[] {
            20,
            0,
            0,
            0});
            this.nMaxDisplayedDebugs.Name = "nMaxDisplayedDebugs";
            this.nMaxDisplayedDebugs.Size = new System.Drawing.Size(99, 20);
            this.nMaxDisplayedDebugs.TabIndex = 94;
            this.nMaxDisplayedDebugs.Value = new decimal(new int[] {
            100,
            0,
            0,
            0});
            // 
            // ll3
            // 
            this.ll3.AutoSize = true;
            this.ll3.Location = new System.Drawing.Point(19, 44);
            this.ll3.Name = "ll3";
            this.ll3.Size = new System.Drawing.Size(70, 13);
            this.ll3.TabIndex = 93;
            this.ll3.Text = "Debug Lines:";
            // 
            // nMaxDisplayedPackets
            // 
            this.nMaxDisplayedPackets.Increment = new decimal(new int[] {
            20,
            0,
            0,
            0});
            this.nMaxDisplayedPackets.Location = new System.Drawing.Point(91, 18);
            this.nMaxDisplayedPackets.Maximum = new decimal(new int[] {
            15000,
            0,
            0,
            0});
            this.nMaxDisplayedPackets.Minimum = new decimal(new int[] {
            20,
            0,
            0,
            0});
            this.nMaxDisplayedPackets.Name = "nMaxDisplayedPackets";
            this.nMaxDisplayedPackets.Size = new System.Drawing.Size(99, 20);
            this.nMaxDisplayedPackets.TabIndex = 92;
            this.nMaxDisplayedPackets.Value = new decimal(new int[] {
            100,
            0,
            0,
            0});
            // 
            // ll2
            // 
            this.ll2.AutoSize = true;
            this.ll2.Location = new System.Drawing.Point(19, 20);
            this.ll2.Name = "ll2";
            this.ll2.Size = new System.Drawing.Size(72, 13);
            this.ll2.TabIndex = 91;
            this.ll2.Text = "Packet Lines:";
            // 
            // txtFont
            // 
            this.txtFont.Location = new System.Drawing.Point(15, 164);
            this.txtFont.Name = "txtFont";
            this.txtFont.ReadOnly = true;
            this.txtFont.Size = new System.Drawing.Size(147, 20);
            this.txtFont.TabIndex = 99;
            // 
            // gbRepeater
            // 
            this.gbRepeater.Controls.Add(this.ll1);
            this.gbRepeater.Controls.Add(this.txtRawFrameUDPPort);
            this.gbRepeater.Dock = System.Windows.Forms.DockStyle.Top;
            this.gbRepeater.Location = new System.Drawing.Point(2, 2);
            this.gbRepeater.Name = "gbRepeater";
            this.gbRepeater.Size = new System.Drawing.Size(194, 84);
            this.gbRepeater.TabIndex = 1;
            this.gbRepeater.TabStop = false;
            this.gbRepeater.Text = "Sources";
            // 
            // ll1
            // 
            this.ll1.AutoSize = true;
            this.ll1.Location = new System.Drawing.Point(20, 22);
            this.ll1.Name = "ll1";
            this.ll1.Size = new System.Drawing.Size(147, 13);
            this.ll1.TabIndex = 93;
            this.ll1.Text = "UDP Ports (comma delimited):";
            // 
            // txtRawFrameUDPPort
            // 
            this.txtRawFrameUDPPort.BackColor = System.Drawing.SystemColors.Window;
            this.txtRawFrameUDPPort.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtRawFrameUDPPort.Location = new System.Drawing.Point(15, 43);
            this.txtRawFrameUDPPort.Multiline = true;
            this.txtRawFrameUDPPort.Name = "txtRawFrameUDPPort";
            this.txtRawFrameUDPPort.Size = new System.Drawing.Size(171, 33);
            this.txtRawFrameUDPPort.TabIndex = 91;
            this.txtRawFrameUDPPort.Text = "15003, 15004";
            // 
            // statusStrip1
            // 
            this.statusStrip1.BackColor = System.Drawing.Color.Transparent;
            this.statusStrip1.Location = new System.Drawing.Point(0, 634);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(1081, 22);
            this.statusStrip1.TabIndex = 68;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // ll5
            // 
            this.ll5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.ll5.AutoSize = true;
            this.ll5.BackColor = System.Drawing.Color.Transparent;
            this.ll5.ForeColor = System.Drawing.SystemColors.ControlDark;
            this.ll5.Location = new System.Drawing.Point(906, 638);
            this.ll5.Name = "ll5";
            this.ll5.Size = new System.Drawing.Size(163, 13);
            this.ll5.TabIndex = 69;
            this.ll5.Text = "Open source Inmarsat-C decoder";
            // 
            // lblVersion
            // 
            this.lblVersion.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.lblVersion.ForeColor = System.Drawing.Color.DeepPink;
            this.lblVersion.Location = new System.Drawing.Point(660, 638);
            this.lblVersion.Name = "lblVersion";
            this.lblVersion.Size = new System.Drawing.Size(240, 13);
            this.lblVersion.TabIndex = 70;
            this.lblVersion.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // udpConsumerTimer
            // 
            this.udpConsumerTimer.Enabled = true;
            this.udpConsumerTimer.Tick += new System.EventHandler(this.udpConsumerTimer_Tick);
            // 
            // dbFlushToDiskTimer
            // 
            this.dbFlushToDiskTimer.Enabled = true;
            this.dbFlushToDiskTimer.Interval = 600000;
            this.dbFlushToDiskTimer.Tick += new System.EventHandler(this.dbFlushToDiskTimer_Tick);
            // 
            // tabUni
            // 
            this.tabUni.Controls.Add(this.panel9);
            this.tabUni.Controls.Add(this.uniGridSplitter);
            this.tabUni.Controls.Add(this.panel14);
            this.tabUni.Controls.Add(this.panel15);
            this.tabUni.Location = new System.Drawing.Point(4, 22);
            this.tabUni.Name = "tabUni";
            this.tabUni.Size = new System.Drawing.Size(858, 531);
            this.tabUni.TabIndex = 21;
            this.tabUni.Text = "Messages";
            this.tabUni.UseVisualStyleBackColor = true;
            // 
            // panel9
            // 
            this.panel9.Controls.Add(this.pnlUniMap);
            this.panel9.Controls.Add(this.splitterUniMap);
            this.panel9.Controls.Add(this.uniMsgBox);
            this.panel9.Controls.Add(this.pnlUniPriority);
            this.panel9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel9.Location = new System.Drawing.Point(538, 67);
            this.panel9.Name = "panel9";
            this.panel9.Padding = new System.Windows.Forms.Padding(1, 1, 1, 0);
            this.panel9.Size = new System.Drawing.Size(320, 464);
            this.panel9.TabIndex = 3;
            // 
            // pnlUniMap
            // 
            this.pnlUniMap.BackColor = System.Drawing.Color.White;
            this.pnlUniMap.Controls.Add(this.uniGMap);
            this.pnlUniMap.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlUniMap.Location = new System.Drawing.Point(1, 283);
            this.pnlUniMap.Name = "pnlUniMap";
            this.pnlUniMap.Padding = new System.Windows.Forms.Padding(0, 1, 0, 0);
            this.pnlUniMap.Size = new System.Drawing.Size(318, 181);
            this.pnlUniMap.TabIndex = 71;
            this.pnlUniMap.Visible = false;
            // 
            // uniGMap
            // 
            this.uniGMap.BackColor = System.Drawing.SystemColors.Control;
            this.uniGMap.Bearing = 0F;
            this.uniGMap.CanDragMap = true;
            this.uniGMap.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uniGMap.EmptyTileColor = System.Drawing.Color.Navy;
            this.uniGMap.GrayScaleMode = false;
            this.uniGMap.HelperLineOption = GMap.NET.WindowsForms.HelperLineOptions.DontShow;
            this.uniGMap.LevelsKeepInMemmory = 5;
            this.uniGMap.Location = new System.Drawing.Point(0, 1);
            this.uniGMap.MarkersEnabled = true;
            this.uniGMap.MaxZoom = 18;
            this.uniGMap.MinZoom = 1;
            this.uniGMap.MouseWheelZoomEnabled = true;
            this.uniGMap.MouseWheelZoomType = GMap.NET.MouseWheelZoomType.MousePositionAndCenter;
            this.uniGMap.Name = "uniGMap";
            this.uniGMap.NegativeMode = false;
            this.uniGMap.PolygonsEnabled = true;
            this.uniGMap.RetryLoadTile = 0;
            this.uniGMap.RoutesEnabled = true;
            this.uniGMap.ScaleMode = GMap.NET.WindowsForms.ScaleModes.Integer;
            this.uniGMap.SelectedAreaFillColor = System.Drawing.Color.Gray;
            this.uniGMap.ShowTileGridLines = false;
            this.uniGMap.Size = new System.Drawing.Size(318, 180);
            this.uniGMap.TabIndex = 1;
            this.uniGMap.Zoom = 8D;
            this.uniGMap.Paint += new System.Windows.Forms.PaintEventHandler(this.uniGMap_Paint);
            // 
            // splitterUniMap
            // 
            this.splitterUniMap.BackColor = System.Drawing.SystemColors.Control;
            this.splitterUniMap.Cursor = System.Windows.Forms.Cursors.HSplit;
            this.splitterUniMap.Dock = System.Windows.Forms.DockStyle.Top;
            this.splitterUniMap.Location = new System.Drawing.Point(1, 278);
            this.splitterUniMap.Name = "splitterUniMap";
            this.splitterUniMap.Size = new System.Drawing.Size(318, 5);
            this.splitterUniMap.TabIndex = 69;
            this.splitterUniMap.TabStop = false;
            // 
            // uniMsgBox
            // 
            this.uniMsgBox.BackColor = System.Drawing.SystemColors.Window;
            this.uniMsgBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.uniMsgBox.Dock = System.Windows.Forms.DockStyle.Top;
            this.uniMsgBox.Font = new System.Drawing.Font("Courier New", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uniMsgBox.ForeColor = System.Drawing.Color.Navy;
            this.uniMsgBox.Location = new System.Drawing.Point(1, 47);
            this.uniMsgBox.Name = "uniMsgBox";
            this.uniMsgBox.ReadOnly = true;
            this.uniMsgBox.Size = new System.Drawing.Size(318, 231);
            this.uniMsgBox.TabIndex = 68;
            this.uniMsgBox.Text = "";
            this.uniMsgBox.LinkClicked += new System.Windows.Forms.LinkClickedEventHandler(this.uniMsgBox_LinkClicked);
            // 
            // pnlUniPriority
            // 
            this.pnlUniPriority.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.pnlUniPriority.Controls.Add(this.lblUniServiceAddress);
            this.pnlUniPriority.Controls.Add(this.lblUniPriority);
            this.pnlUniPriority.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlUniPriority.Location = new System.Drawing.Point(1, 1);
            this.pnlUniPriority.Name = "pnlUniPriority";
            this.pnlUniPriority.Size = new System.Drawing.Size(318, 46);
            this.pnlUniPriority.TabIndex = 70;
            // 
            // lblUniServiceAddress
            // 
            this.lblUniServiceAddress.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblUniServiceAddress.AutoEllipsis = true;
            this.lblUniServiceAddress.BackColor = System.Drawing.SystemColors.ControlLight;
            this.lblUniServiceAddress.ForeColor = System.Drawing.Color.Navy;
            this.lblUniServiceAddress.Location = new System.Drawing.Point(55, 10);
            this.lblUniServiceAddress.Name = "lblUniServiceAddress";
            this.lblUniServiceAddress.Size = new System.Drawing.Size(256, 27);
            this.lblUniServiceAddress.TabIndex = 13;
            this.lblUniServiceAddress.Text = "Service and address";
            // 
            // lblUniPriority
            // 
            this.lblUniPriority.Location = new System.Drawing.Point(3, 10);
            this.lblUniPriority.Name = "lblUniPriority";
            this.lblUniPriority.Size = new System.Drawing.Size(56, 13);
            this.lblUniPriority.TabIndex = 14;
            this.lblUniPriority.Text = "Priority:";
            this.lblUniPriority.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // uniGridSplitter
            // 
            this.uniGridSplitter.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.uniGridSplitter.Location = new System.Drawing.Point(533, 67);
            this.uniGridSplitter.Name = "uniGridSplitter";
            this.uniGridSplitter.Size = new System.Drawing.Size(5, 464);
            this.uniGridSplitter.TabIndex = 2;
            this.uniGridSplitter.TabStop = false;
            // 
            // panel14
            // 
            this.panel14.BackColor = System.Drawing.Color.WhiteSmoke;
            this.panel14.Controls.Add(this.dgvUni);
            this.panel14.Controls.Add(this.toolStrip2);
            this.panel14.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel14.Location = new System.Drawing.Point(0, 67);
            this.panel14.Name = "panel14";
            this.panel14.Size = new System.Drawing.Size(533, 464);
            this.panel14.TabIndex = 0;
            // 
            // dgvUni
            // 
            this.dgvUni.AllowUserToAddRows = false;
            this.dgvUni.AllowUserToDeleteRows = false;
            this.dgvUni.AllowUserToOrderColumns = true;
            this.dgvUni.AllowUserToResizeRows = false;
            this.dgvUni.AutoGenerateColumns = false;
            this.dgvUni.BackgroundColor = System.Drawing.SystemColors.Window;
            this.dgvUni.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dgvUni.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            this.dgvUni.ColumnHeadersHeight = 30;
            this.dgvUni.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dgvUni.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.UniPriority,
            this.UniEncoding,
            this.UniTime,
            this.mIdOrLCNoDataGridViewTextBoxColumn,
            this.UniRepetition,
            this.UniPartsReceived,
            this.UniIsPartial,
            this.UniStation,
            this.UniSatName,
            this.UniSat,
            this.UniStreamId,
            this.uniIdDataGridViewTextBoxColumn,
            this.streamIdDataGridViewTextBoxColumn,
            this.UniIsEGC,
            this.satDataGridViewTextBoxColumn,
            this.satNameDataGridViewTextBoxColumn,
            this.lesIdDataGridViewTextBoxColumn,
            this.lesNameDataGridViewTextBoxColumn,
            this.timeDataGridViewTextBoxColumn,
            this.timeReceivedDataGridViewTextBoxColumn,
            this.repetitionDataGridViewTextBoxColumn,
            this.messageTypeDataGridViewTextBoxColumn,
            this.priorityDataGridViewTextBoxColumn,
            this.contentTypeDataGridViewTextBoxColumn,
            this.partsReceivedDataGridViewTextBoxColumn,
            this.messageDataGridViewTextBoxColumn,
            this.isPartialDataGridViewCheckBoxColumn,
            this.addressHexDataGridViewTextBoxColumn,
            this.bulletinBoardDataGridViewTextBoxColumn,
            this.areaDataGridViewTextBoxColumn});
            this.dgvUni.DataSource = this.unisBindingSource;
            this.dgvUni.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvUni.EnableHeadersVisualStyles = false;
            this.dgvUni.Location = new System.Drawing.Point(0, 0);
            this.dgvUni.Margin = new System.Windows.Forms.Padding(3, 3, 3, 10);
            this.dgvUni.MultiSelect = false;
            this.dgvUni.Name = "dgvUni";
            this.dgvUni.ReadOnly = true;
            this.dgvUni.RowHeadersVisible = false;
            this.dgvUni.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.dgvUni.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.dgvUni.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvUni.Size = new System.Drawing.Size(533, 438);
            this.dgvUni.TabIndex = 0;
            this.dgvUni.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvUni_CellClick);
            this.dgvUni.CellFormatting += new System.Windows.Forms.DataGridViewCellFormattingEventHandler(this.dgvUni_CellFormatting);
            this.dgvUni.CellPainting += new System.Windows.Forms.DataGridViewCellPaintingEventHandler(this.dgvUni_CellPainting);
            this.dgvUni.RowEnter += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvUni_RowEnter);
            this.dgvUni.RowPrePaint += new System.Windows.Forms.DataGridViewRowPrePaintEventHandler(this.dgvUni_RowPrePaint);
            this.dgvUni.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dgvUni_KeyDown);
            // 
            // UniPriority
            // 
            this.UniPriority.DataPropertyName = "Priority";
            this.UniPriority.HeaderText = "";
            this.UniPriority.Name = "UniPriority";
            this.UniPriority.ReadOnly = true;
            this.UniPriority.ToolTipText = "Priority";
            this.UniPriority.Width = 13;
            // 
            // UniEncoding
            // 
            this.UniEncoding.DataPropertyName = "ContentType";
            this.UniEncoding.HeaderText = "";
            this.UniEncoding.Name = "UniEncoding";
            this.UniEncoding.ReadOnly = true;
            this.UniEncoding.ToolTipText = "Encoding";
            this.UniEncoding.Width = 13;
            // 
            // UniTime
            // 
            this.UniTime.DataPropertyName = "Time";
            this.UniTime.HeaderText = "Local Time";
            this.UniTime.Name = "UniTime";
            this.UniTime.ReadOnly = true;
            this.UniTime.Width = 120;
            // 
            // mIdOrLCNoDataGridViewTextBoxColumn
            // 
            this.mIdOrLCNoDataGridViewTextBoxColumn.DataPropertyName = "MIdOrLCNo";
            this.mIdOrLCNoDataGridViewTextBoxColumn.HeaderText = "EGC/LCN";
            this.mIdOrLCNoDataGridViewTextBoxColumn.Name = "mIdOrLCNoDataGridViewTextBoxColumn";
            this.mIdOrLCNoDataGridViewTextBoxColumn.ReadOnly = true;
            this.mIdOrLCNoDataGridViewTextBoxColumn.ToolTipText = "EGC / Logical Channel Number";
            this.mIdOrLCNoDataGridViewTextBoxColumn.Width = 60;
            // 
            // UniRepetition
            // 
            this.UniRepetition.DataPropertyName = "Repetition";
            this.UniRepetition.HeaderText = "REP";
            this.UniRepetition.Name = "UniRepetition";
            this.UniRepetition.ReadOnly = true;
            this.UniRepetition.ToolTipText = "Repetition";
            this.UniRepetition.Width = 40;
            // 
            // UniPartsReceived
            // 
            this.UniPartsReceived.DataPropertyName = "PartsReceived";
            this.UniPartsReceived.HeaderText = "PTS";
            this.UniPartsReceived.Name = "UniPartsReceived";
            this.UniPartsReceived.ReadOnly = true;
            this.UniPartsReceived.ToolTipText = "Parts";
            this.UniPartsReceived.Width = 40;
            // 
            // UniIsPartial
            // 
            this.UniIsPartial.DataPropertyName = "IsPartial";
            this.UniIsPartial.FalseValue = "";
            this.UniIsPartial.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.UniIsPartial.HeaderText = "Partial";
            this.UniIsPartial.IndeterminateValue = "";
            this.UniIsPartial.Name = "UniIsPartial";
            this.UniIsPartial.ReadOnly = true;
            this.UniIsPartial.ToolTipText = "If checked, this message is either incomplete or still being received";
            this.UniIsPartial.TrueValue = "";
            this.UniIsPartial.Width = 42;
            // 
            // UniStation
            // 
            this.UniStation.DataPropertyName = "LesName";
            this.UniStation.HeaderText = "Station";
            this.UniStation.Name = "UniStation";
            this.UniStation.ReadOnly = true;
            this.UniStation.Width = 80;
            // 
            // UniSatName
            // 
            this.UniSatName.DataPropertyName = "SatName";
            this.UniSatName.HeaderText = "Sat";
            this.UniSatName.Name = "UniSatName";
            this.UniSatName.ReadOnly = true;
            this.UniSatName.Width = 50;
            // 
            // UniSat
            // 
            this.UniSat.DataPropertyName = "Sat";
            this.UniSat.HeaderText = "Sat";
            this.UniSat.Name = "UniSat";
            this.UniSat.ReadOnly = true;
            this.UniSat.Visible = false;
            // 
            // UniStreamId
            // 
            this.UniStreamId.DataPropertyName = "StreamId";
            this.UniStreamId.HeaderText = "SId";
            this.UniStreamId.Name = "UniStreamId";
            this.UniStreamId.ReadOnly = true;
            this.UniStreamId.Width = 50;
            // 
            // uniIdDataGridViewTextBoxColumn
            // 
            this.uniIdDataGridViewTextBoxColumn.DataPropertyName = "UniId";
            this.uniIdDataGridViewTextBoxColumn.HeaderText = "UniId";
            this.uniIdDataGridViewTextBoxColumn.Name = "uniIdDataGridViewTextBoxColumn";
            this.uniIdDataGridViewTextBoxColumn.ReadOnly = true;
            this.uniIdDataGridViewTextBoxColumn.Visible = false;
            // 
            // streamIdDataGridViewTextBoxColumn
            // 
            this.streamIdDataGridViewTextBoxColumn.DataPropertyName = "StreamId";
            this.streamIdDataGridViewTextBoxColumn.HeaderText = "StreamId";
            this.streamIdDataGridViewTextBoxColumn.Name = "streamIdDataGridViewTextBoxColumn";
            this.streamIdDataGridViewTextBoxColumn.ReadOnly = true;
            this.streamIdDataGridViewTextBoxColumn.Visible = false;
            // 
            // UniIsEGC
            // 
            this.UniIsEGC.DataPropertyName = "IsEGC";
            this.UniIsEGC.HeaderText = "IsEGC";
            this.UniIsEGC.Name = "UniIsEGC";
            this.UniIsEGC.ReadOnly = true;
            this.UniIsEGC.Visible = false;
            // 
            // satDataGridViewTextBoxColumn
            // 
            this.satDataGridViewTextBoxColumn.DataPropertyName = "Sat";
            this.satDataGridViewTextBoxColumn.HeaderText = "Sat";
            this.satDataGridViewTextBoxColumn.Name = "satDataGridViewTextBoxColumn";
            this.satDataGridViewTextBoxColumn.ReadOnly = true;
            this.satDataGridViewTextBoxColumn.Visible = false;
            // 
            // satNameDataGridViewTextBoxColumn
            // 
            this.satNameDataGridViewTextBoxColumn.DataPropertyName = "SatName";
            this.satNameDataGridViewTextBoxColumn.HeaderText = "SatName";
            this.satNameDataGridViewTextBoxColumn.Name = "satNameDataGridViewTextBoxColumn";
            this.satNameDataGridViewTextBoxColumn.ReadOnly = true;
            this.satNameDataGridViewTextBoxColumn.Visible = false;
            // 
            // lesIdDataGridViewTextBoxColumn
            // 
            this.lesIdDataGridViewTextBoxColumn.DataPropertyName = "LesId";
            this.lesIdDataGridViewTextBoxColumn.HeaderText = "LesId";
            this.lesIdDataGridViewTextBoxColumn.Name = "lesIdDataGridViewTextBoxColumn";
            this.lesIdDataGridViewTextBoxColumn.ReadOnly = true;
            this.lesIdDataGridViewTextBoxColumn.Visible = false;
            // 
            // lesNameDataGridViewTextBoxColumn
            // 
            this.lesNameDataGridViewTextBoxColumn.DataPropertyName = "LesName";
            this.lesNameDataGridViewTextBoxColumn.HeaderText = "LesName";
            this.lesNameDataGridViewTextBoxColumn.Name = "lesNameDataGridViewTextBoxColumn";
            this.lesNameDataGridViewTextBoxColumn.ReadOnly = true;
            this.lesNameDataGridViewTextBoxColumn.Visible = false;
            // 
            // timeDataGridViewTextBoxColumn
            // 
            this.timeDataGridViewTextBoxColumn.DataPropertyName = "Time";
            this.timeDataGridViewTextBoxColumn.HeaderText = "Time";
            this.timeDataGridViewTextBoxColumn.Name = "timeDataGridViewTextBoxColumn";
            this.timeDataGridViewTextBoxColumn.ReadOnly = true;
            this.timeDataGridViewTextBoxColumn.Visible = false;
            // 
            // timeReceivedDataGridViewTextBoxColumn
            // 
            this.timeReceivedDataGridViewTextBoxColumn.DataPropertyName = "TimeReceived";
            this.timeReceivedDataGridViewTextBoxColumn.HeaderText = "TimeReceived";
            this.timeReceivedDataGridViewTextBoxColumn.Name = "timeReceivedDataGridViewTextBoxColumn";
            this.timeReceivedDataGridViewTextBoxColumn.ReadOnly = true;
            this.timeReceivedDataGridViewTextBoxColumn.Visible = false;
            // 
            // repetitionDataGridViewTextBoxColumn
            // 
            this.repetitionDataGridViewTextBoxColumn.DataPropertyName = "Repetition";
            this.repetitionDataGridViewTextBoxColumn.HeaderText = "Repetition";
            this.repetitionDataGridViewTextBoxColumn.Name = "repetitionDataGridViewTextBoxColumn";
            this.repetitionDataGridViewTextBoxColumn.ReadOnly = true;
            this.repetitionDataGridViewTextBoxColumn.Visible = false;
            // 
            // messageTypeDataGridViewTextBoxColumn
            // 
            this.messageTypeDataGridViewTextBoxColumn.DataPropertyName = "MessageType";
            this.messageTypeDataGridViewTextBoxColumn.HeaderText = "MessageType";
            this.messageTypeDataGridViewTextBoxColumn.Name = "messageTypeDataGridViewTextBoxColumn";
            this.messageTypeDataGridViewTextBoxColumn.ReadOnly = true;
            this.messageTypeDataGridViewTextBoxColumn.Visible = false;
            // 
            // priorityDataGridViewTextBoxColumn
            // 
            this.priorityDataGridViewTextBoxColumn.DataPropertyName = "Priority";
            this.priorityDataGridViewTextBoxColumn.HeaderText = "Priority";
            this.priorityDataGridViewTextBoxColumn.Name = "priorityDataGridViewTextBoxColumn";
            this.priorityDataGridViewTextBoxColumn.ReadOnly = true;
            this.priorityDataGridViewTextBoxColumn.Visible = false;
            // 
            // contentTypeDataGridViewTextBoxColumn
            // 
            this.contentTypeDataGridViewTextBoxColumn.DataPropertyName = "ContentType";
            this.contentTypeDataGridViewTextBoxColumn.HeaderText = "ContentType";
            this.contentTypeDataGridViewTextBoxColumn.Name = "contentTypeDataGridViewTextBoxColumn";
            this.contentTypeDataGridViewTextBoxColumn.ReadOnly = true;
            this.contentTypeDataGridViewTextBoxColumn.Visible = false;
            // 
            // partsReceivedDataGridViewTextBoxColumn
            // 
            this.partsReceivedDataGridViewTextBoxColumn.DataPropertyName = "PartsReceived";
            this.partsReceivedDataGridViewTextBoxColumn.HeaderText = "PartsReceived";
            this.partsReceivedDataGridViewTextBoxColumn.Name = "partsReceivedDataGridViewTextBoxColumn";
            this.partsReceivedDataGridViewTextBoxColumn.ReadOnly = true;
            this.partsReceivedDataGridViewTextBoxColumn.Visible = false;
            // 
            // messageDataGridViewTextBoxColumn
            // 
            this.messageDataGridViewTextBoxColumn.DataPropertyName = "Message";
            this.messageDataGridViewTextBoxColumn.HeaderText = "Message";
            this.messageDataGridViewTextBoxColumn.Name = "messageDataGridViewTextBoxColumn";
            this.messageDataGridViewTextBoxColumn.ReadOnly = true;
            this.messageDataGridViewTextBoxColumn.Visible = false;
            // 
            // isPartialDataGridViewCheckBoxColumn
            // 
            this.isPartialDataGridViewCheckBoxColumn.DataPropertyName = "IsPartial";
            this.isPartialDataGridViewCheckBoxColumn.HeaderText = "IsPartial";
            this.isPartialDataGridViewCheckBoxColumn.Name = "isPartialDataGridViewCheckBoxColumn";
            this.isPartialDataGridViewCheckBoxColumn.ReadOnly = true;
            this.isPartialDataGridViewCheckBoxColumn.Visible = false;
            // 
            // addressHexDataGridViewTextBoxColumn
            // 
            this.addressHexDataGridViewTextBoxColumn.DataPropertyName = "AddressHex";
            this.addressHexDataGridViewTextBoxColumn.HeaderText = "AddressHex";
            this.addressHexDataGridViewTextBoxColumn.Name = "addressHexDataGridViewTextBoxColumn";
            this.addressHexDataGridViewTextBoxColumn.ReadOnly = true;
            this.addressHexDataGridViewTextBoxColumn.Visible = false;
            // 
            // bulletinBoardDataGridViewTextBoxColumn
            // 
            this.bulletinBoardDataGridViewTextBoxColumn.DataPropertyName = "BulletinBoard";
            this.bulletinBoardDataGridViewTextBoxColumn.HeaderText = "BulletinBoard";
            this.bulletinBoardDataGridViewTextBoxColumn.Name = "bulletinBoardDataGridViewTextBoxColumn";
            this.bulletinBoardDataGridViewTextBoxColumn.ReadOnly = true;
            this.bulletinBoardDataGridViewTextBoxColumn.Visible = false;
            // 
            // areaDataGridViewTextBoxColumn
            // 
            this.areaDataGridViewTextBoxColumn.DataPropertyName = "Area";
            this.areaDataGridViewTextBoxColumn.HeaderText = "Area";
            this.areaDataGridViewTextBoxColumn.Name = "areaDataGridViewTextBoxColumn";
            this.areaDataGridViewTextBoxColumn.ReadOnly = true;
            this.areaDataGridViewTextBoxColumn.Visible = false;
            // 
            // unisBindingSource
            // 
            this.unisBindingSource.DataSource = typeof(ScytaleC.QuickUI.Uni);
            // 
            // toolStrip2
            // 
            this.toolStrip2.BackColor = System.Drawing.Color.Transparent;
            this.toolStrip2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.toolStrip2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.toolStrip2.Font = new System.Drawing.Font("Segoe UI", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.toolStrip2.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolStrip2.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btnUniAutoScroll,
            this.toolStripSeparator1,
            this.btnUniAutoLastSelect,
            this.toolStripSeparator3,
            this.toolStripButton1});
            this.toolStrip2.LayoutStyle = System.Windows.Forms.ToolStripLayoutStyle.HorizontalStackWithOverflow;
            this.toolStrip2.Location = new System.Drawing.Point(0, 438);
            this.toolStrip2.Name = "toolStrip2";
            this.toolStrip2.Padding = new System.Windows.Forms.Padding(0, 2, 0, 1);
            this.toolStrip2.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.toolStrip2.Size = new System.Drawing.Size(533, 26);
            this.toolStrip2.TabIndex = 75;
            this.toolStrip2.Text = "toolStrip1";
            // 
            // btnUniAutoScroll
            // 
            this.btnUniAutoScroll.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.btnUniAutoScroll.Image = ((System.Drawing.Image)(resources.GetObject("btnUniAutoScroll.Image")));
            this.btnUniAutoScroll.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnUniAutoScroll.Margin = new System.Windows.Forms.Padding(0, 1, 50, 2);
            this.btnUniAutoScroll.Name = "btnUniAutoScroll";
            this.btnUniAutoScroll.Size = new System.Drawing.Size(51, 20);
            this.btnUniAutoScroll.Text = "Auto Scroll";
            this.btnUniAutoScroll.Click += new System.EventHandler(this.btnUniAutoScroll_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 23);
            // 
            // btnUniAutoLastSelect
            // 
            this.btnUniAutoLastSelect.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.btnUniAutoLastSelect.Enabled = false;
            this.btnUniAutoLastSelect.Image = ((System.Drawing.Image)(resources.GetObject("btnUniAutoLastSelect.Image")));
            this.btnUniAutoLastSelect.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnUniAutoLastSelect.Name = "btnUniAutoLastSelect";
            this.btnUniAutoLastSelect.Size = new System.Drawing.Size(72, 20);
            this.btnUniAutoLastSelect.Text = "Auto Last Select";
            this.btnUniAutoLastSelect.Click += new System.EventHandler(this.toolStripButton_Click);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(6, 23);
            // 
            // toolStripButton1
            // 
            this.toolStripButton1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripButton1.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton1.Image")));
            this.toolStripButton1.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton1.Name = "toolStripButton1";
            this.toolStripButton1.Size = new System.Drawing.Size(70, 20);
            this.toolStripButton1.Text = "Clear Database";
            this.toolStripButton1.Click += new System.EventHandler(this.toolStripButton1_Click);
            // 
            // panel15
            // 
            this.panel15.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.panel15.Controls.Add(this.ll22);
            this.panel15.Controls.Add(this.ll14);
            this.panel15.Controls.Add(this.ll13);
            this.panel15.Controls.Add(this.ll16);
            this.panel15.Controls.Add(this.ll21);
            this.panel15.Controls.Add(this.ll23);
            this.panel15.Controls.Add(this.ll24);
            this.panel15.Controls.Add(this.ll15);
            this.panel15.Controls.Add(this.ll9);
            this.panel15.Controls.Add(this.ll12);
            this.panel15.Controls.Add(this.ll17);
            this.panel15.Controls.Add(this.ll19);
            this.panel15.Controls.Add(this.ll20);
            this.panel15.Controls.Add(this.ll10);
            this.panel15.Controls.Add(this.ll18);
            this.panel15.Controls.Add(this.ll11);
            this.panel15.Controls.Add(this.ll6);
            this.panel15.Controls.Add(this.lblUniChannelTypeAndSat);
            this.panel15.Controls.Add(this.lblUniStatus);
            this.panel15.Controls.Add(this.lblUniServices);
            this.panel15.Controls.Add(this.ll8);
            this.panel15.Controls.Add(this.ll7);
            this.panel15.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel15.Location = new System.Drawing.Point(0, 0);
            this.panel15.Name = "panel15";
            this.panel15.Size = new System.Drawing.Size(858, 67);
            this.panel15.TabIndex = 0;
            // 
            // ll22
            // 
            this.ll22.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.ll22.BackColor = System.Drawing.Color.Plum;
            this.ll22.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ll22.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ll22.Location = new System.Drawing.Point(791, 20);
            this.ll22.Name = "ll22";
            this.ll22.Size = new System.Drawing.Size(13, 13);
            this.ll22.TabIndex = 45;
            // 
            // ll14
            // 
            this.ll14.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.ll14.AutoSize = true;
            this.ll14.Location = new System.Drawing.Point(806, 20);
            this.ll14.Name = "ll14";
            this.ll14.Size = new System.Drawing.Size(23, 13);
            this.ll14.TabIndex = 46;
            this.ll14.Text = "IA5";
            // 
            // ll13
            // 
            this.ll13.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.ll13.AutoSize = true;
            this.ll13.Location = new System.Drawing.Point(806, 6);
            this.ll13.Name = "ll13";
            this.ll13.Size = new System.Drawing.Size(36, 13);
            this.ll13.TabIndex = 40;
            this.ll13.Text = "Binary";
            // 
            // ll16
            // 
            this.ll16.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.ll16.AutoSize = true;
            this.ll16.Location = new System.Drawing.Point(806, 48);
            this.ll16.Name = "ll16";
            this.ll16.Size = new System.Drawing.Size(34, 13);
            this.ll16.TabIndex = 44;
            this.ll16.Text = "ASCII";
            // 
            // ll21
            // 
            this.ll21.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.ll21.BackColor = System.Drawing.Color.LightSalmon;
            this.ll21.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ll21.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ll21.Location = new System.Drawing.Point(791, 6);
            this.ll21.Name = "ll21";
            this.ll21.Size = new System.Drawing.Size(13, 13);
            this.ll21.TabIndex = 39;
            // 
            // ll23
            // 
            this.ll23.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.ll23.BackColor = System.Drawing.Color.LightGray;
            this.ll23.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ll23.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ll23.Location = new System.Drawing.Point(791, 34);
            this.ll23.Name = "ll23";
            this.ll23.Size = new System.Drawing.Size(13, 13);
            this.ll23.TabIndex = 41;
            // 
            // ll24
            // 
            this.ll24.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.ll24.BackColor = System.Drawing.Color.White;
            this.ll24.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ll24.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ll24.Location = new System.Drawing.Point(791, 48);
            this.ll24.Name = "ll24";
            this.ll24.Size = new System.Drawing.Size(13, 13);
            this.ll24.TabIndex = 43;
            // 
            // ll15
            // 
            this.ll15.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.ll15.AutoSize = true;
            this.ll15.Location = new System.Drawing.Point(806, 34);
            this.ll15.Name = "ll15";
            this.ll15.Size = new System.Drawing.Size(30, 13);
            this.ll15.TabIndex = 42;
            this.ll15.Text = "ITA2";
            // 
            // ll9
            // 
            this.ll9.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.ll9.AutoSize = true;
            this.ll9.Location = new System.Drawing.Point(737, 6);
            this.ll9.Name = "ll9";
            this.ll9.Size = new System.Drawing.Size(44, 13);
            this.ll9.TabIndex = 1;
            this.ll9.Text = "Distress";
            // 
            // ll12
            // 
            this.ll12.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.ll12.AutoSize = true;
            this.ll12.Location = new System.Drawing.Point(737, 48);
            this.ll12.Name = "ll12";
            this.ll12.Size = new System.Drawing.Size(44, 13);
            this.ll12.TabIndex = 7;
            this.ll12.Text = "Routine";
            // 
            // ll17
            // 
            this.ll17.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.ll17.BackColor = System.Drawing.Color.Red;
            this.ll17.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ll17.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ll17.Location = new System.Drawing.Point(722, 6);
            this.ll17.Name = "ll17";
            this.ll17.Size = new System.Drawing.Size(13, 13);
            this.ll17.TabIndex = 0;
            // 
            // ll19
            // 
            this.ll19.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.ll19.BackColor = System.Drawing.Color.LightGreen;
            this.ll19.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ll19.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ll19.Location = new System.Drawing.Point(722, 34);
            this.ll19.Name = "ll19";
            this.ll19.Size = new System.Drawing.Size(13, 13);
            this.ll19.TabIndex = 4;
            // 
            // ll20
            // 
            this.ll20.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.ll20.BackColor = System.Drawing.Color.PaleTurquoise;
            this.ll20.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ll20.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ll20.Location = new System.Drawing.Point(722, 48);
            this.ll20.Name = "ll20";
            this.ll20.Size = new System.Drawing.Size(13, 13);
            this.ll20.TabIndex = 6;
            // 
            // ll10
            // 
            this.ll10.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.ll10.AutoSize = true;
            this.ll10.Location = new System.Drawing.Point(737, 20);
            this.ll10.Name = "ll10";
            this.ll10.Size = new System.Drawing.Size(47, 13);
            this.ll10.TabIndex = 3;
            this.ll10.Text = "Urgency";
            // 
            // ll18
            // 
            this.ll18.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.ll18.BackColor = System.Drawing.Color.Gold;
            this.ll18.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ll18.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ll18.Location = new System.Drawing.Point(722, 20);
            this.ll18.Name = "ll18";
            this.ll18.Size = new System.Drawing.Size(13, 13);
            this.ll18.TabIndex = 2;
            // 
            // ll11
            // 
            this.ll11.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.ll11.AutoSize = true;
            this.ll11.Location = new System.Drawing.Point(737, 34);
            this.ll11.Name = "ll11";
            this.ll11.Size = new System.Drawing.Size(37, 13);
            this.ll11.TabIndex = 5;
            this.ll11.Text = "Safety";
            // 
            // ll6
            // 
            this.ll6.AutoSize = true;
            this.ll6.Location = new System.Drawing.Point(11, 6);
            this.ll6.Name = "ll6";
            this.ll6.Size = new System.Drawing.Size(49, 13);
            this.ll6.TabIndex = 8;
            this.ll6.Text = "Channel:";
            // 
            // lblUniChannelTypeAndSat
            // 
            this.lblUniChannelTypeAndSat.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblUniChannelTypeAndSat.AutoEllipsis = true;
            this.lblUniChannelTypeAndSat.BackColor = System.Drawing.SystemColors.ControlLight;
            this.lblUniChannelTypeAndSat.ForeColor = System.Drawing.Color.Navy;
            this.lblUniChannelTypeAndSat.Location = new System.Drawing.Point(63, 7);
            this.lblUniChannelTypeAndSat.Name = "lblUniChannelTypeAndSat";
            this.lblUniChannelTypeAndSat.Size = new System.Drawing.Size(641, 13);
            this.lblUniChannelTypeAndSat.TabIndex = 12;
            // 
            // lblUniStatus
            // 
            this.lblUniStatus.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblUniStatus.AutoEllipsis = true;
            this.lblUniStatus.BackColor = System.Drawing.SystemColors.ControlLight;
            this.lblUniStatus.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblUniStatus.ForeColor = System.Drawing.Color.Navy;
            this.lblUniStatus.Location = new System.Drawing.Point(63, 44);
            this.lblUniStatus.Name = "lblUniStatus";
            this.lblUniStatus.Size = new System.Drawing.Size(641, 14);
            this.lblUniStatus.TabIndex = 36;
            // 
            // lblUniServices
            // 
            this.lblUniServices.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblUniServices.AutoEllipsis = true;
            this.lblUniServices.BackColor = System.Drawing.SystemColors.ControlLight;
            this.lblUniServices.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblUniServices.ForeColor = System.Drawing.Color.Navy;
            this.lblUniServices.Location = new System.Drawing.Point(63, 25);
            this.lblUniServices.Name = "lblUniServices";
            this.lblUniServices.Size = new System.Drawing.Size(641, 14);
            this.lblUniServices.TabIndex = 35;
            // 
            // ll8
            // 
            this.ll8.AutoSize = true;
            this.ll8.Location = new System.Drawing.Point(17, 45);
            this.ll8.Name = "ll8";
            this.ll8.Size = new System.Drawing.Size(40, 13);
            this.ll8.TabIndex = 29;
            this.ll8.Text = "Status:";
            // 
            // ll7
            // 
            this.ll7.AutoSize = true;
            this.ll7.Location = new System.Drawing.Point(11, 25);
            this.ll7.Name = "ll7";
            this.ll7.Size = new System.Drawing.Size(46, 13);
            this.ll7.TabIndex = 10;
            this.ll7.Text = "Service:";
            // 
            // tabDebug
            // 
            this.tabDebug.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.tabDebug.Controls.Add(this.rtbDebug);
            this.tabDebug.Controls.Add(this.tsBlocks);
            this.tabDebug.Location = new System.Drawing.Point(4, 22);
            this.tabDebug.Name = "tabDebug";
            this.tabDebug.Padding = new System.Windows.Forms.Padding(1);
            this.tabDebug.Size = new System.Drawing.Size(858, 531);
            this.tabDebug.TabIndex = 4;
            this.tabDebug.Text = "Debug";
            // 
            // rtbDebug
            // 
            this.rtbDebug.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.rtbDebug.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rtbDebug.Font = new System.Drawing.Font("Courier New", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rtbDebug.Location = new System.Drawing.Point(1, 26);
            this.rtbDebug.Name = "rtbDebug";
            this.rtbDebug.Size = new System.Drawing.Size(856, 504);
            this.rtbDebug.TabIndex = 67;
            this.rtbDebug.Text = "";
            this.rtbDebug.WordWrap = false;
            // 
            // tsBlocks
            // 
            this.tsBlocks.BackColor = System.Drawing.Color.Transparent;
            this.tsBlocks.Font = new System.Drawing.Font("Segoe UI", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tsBlocks.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btnDebugClear,
            this.toolStripSeparator4,
            this.btnDebugCopy,
            this.toolStripSeparator17,
            this.btnDebugAutoScroll,
            this.toolStripSeparator2,
            this.btnJson});
            this.tsBlocks.LayoutStyle = System.Windows.Forms.ToolStripLayoutStyle.HorizontalStackWithOverflow;
            this.tsBlocks.Location = new System.Drawing.Point(1, 1);
            this.tsBlocks.Name = "tsBlocks";
            this.tsBlocks.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.tsBlocks.Size = new System.Drawing.Size(856, 25);
            this.tsBlocks.TabIndex = 74;
            // 
            // btnDebugClear
            // 
            this.btnDebugClear.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.btnDebugClear.Image = ((System.Drawing.Image)(resources.GetObject("btnDebugClear.Image")));
            this.btnDebugClear.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnDebugClear.Name = "btnDebugClear";
            this.btnDebugClear.Size = new System.Drawing.Size(30, 22);
            this.btnDebugClear.Text = "Clear";
            this.btnDebugClear.Click += new System.EventHandler(this.btnClear_Click);
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new System.Drawing.Size(6, 25);
            // 
            // btnDebugCopy
            // 
            this.btnDebugCopy.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.btnDebugCopy.Image = ((System.Drawing.Image)(resources.GetObject("btnDebugCopy.Image")));
            this.btnDebugCopy.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnDebugCopy.Name = "btnDebugCopy";
            this.btnDebugCopy.Size = new System.Drawing.Size(29, 22);
            this.btnDebugCopy.Text = "Copy";
            this.btnDebugCopy.Click += new System.EventHandler(this.btnDebugCopy_Click);
            // 
            // toolStripSeparator17
            // 
            this.toolStripSeparator17.Name = "toolStripSeparator17";
            this.toolStripSeparator17.Size = new System.Drawing.Size(6, 25);
            // 
            // btnDebugAutoScroll
            // 
            this.btnDebugAutoScroll.Checked = true;
            this.btnDebugAutoScroll.CheckState = System.Windows.Forms.CheckState.Checked;
            this.btnDebugAutoScroll.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.btnDebugAutoScroll.Image = ((System.Drawing.Image)(resources.GetObject("btnDebugAutoScroll.Image")));
            this.btnDebugAutoScroll.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnDebugAutoScroll.Name = "btnDebugAutoScroll";
            this.btnDebugAutoScroll.Size = new System.Drawing.Size(51, 22);
            this.btnDebugAutoScroll.Text = "Auto Scroll";
            this.btnDebugAutoScroll.Click += new System.EventHandler(this.toolStripButton_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(6, 25);
            // 
            // btnJson
            // 
            this.btnJson.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.btnJson.Image = ((System.Drawing.Image)(resources.GetObject("btnJson.Image")));
            this.btnJson.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnJson.Name = "btnJson";
            this.btnJson.Size = new System.Drawing.Size(26, 22);
            this.btnJson.Text = "Json";
            this.btnJson.Click += new System.EventHandler(this.toolStripButton_Click);
            // 
            // tabPackets
            // 
            this.tabPackets.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.tabPackets.Controls.Add(this.rtbPackets);
            this.tabPackets.Controls.Add(this.tsPackets);
            this.tabPackets.Location = new System.Drawing.Point(4, 22);
            this.tabPackets.Name = "tabPackets";
            this.tabPackets.Padding = new System.Windows.Forms.Padding(1);
            this.tabPackets.Size = new System.Drawing.Size(858, 531);
            this.tabPackets.TabIndex = 2;
            this.tabPackets.Tag = "";
            this.tabPackets.Text = "Packets";
            // 
            // rtbPackets
            // 
            this.rtbPackets.BackColor = System.Drawing.SystemColors.Window;
            this.rtbPackets.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.rtbPackets.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rtbPackets.Font = new System.Drawing.Font("Courier New", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rtbPackets.Location = new System.Drawing.Point(1, 26);
            this.rtbPackets.MaxLength = 500;
            this.rtbPackets.Name = "rtbPackets";
            this.rtbPackets.Size = new System.Drawing.Size(856, 504);
            this.rtbPackets.TabIndex = 66;
            this.rtbPackets.Text = "";
            this.rtbPackets.WordWrap = false;
            // 
            // tsPackets
            // 
            this.tsPackets.BackColor = System.Drawing.Color.Transparent;
            this.tsPackets.Font = new System.Drawing.Font("Segoe UI", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tsPackets.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btnPacketsClear,
            this.toolStripSeparator14,
            this.toolStripButton4,
            this.toolStripSeparator18,
            this.btnPacketsAutoScroll});
            this.tsPackets.LayoutStyle = System.Windows.Forms.ToolStripLayoutStyle.HorizontalStackWithOverflow;
            this.tsPackets.Location = new System.Drawing.Point(1, 1);
            this.tsPackets.Name = "tsPackets";
            this.tsPackets.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.tsPackets.Size = new System.Drawing.Size(856, 25);
            this.tsPackets.TabIndex = 77;
            this.tsPackets.Text = "toolStrip1";
            // 
            // btnPacketsClear
            // 
            this.btnPacketsClear.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.btnPacketsClear.Image = ((System.Drawing.Image)(resources.GetObject("btnPacketsClear.Image")));
            this.btnPacketsClear.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnPacketsClear.Name = "btnPacketsClear";
            this.btnPacketsClear.Size = new System.Drawing.Size(30, 22);
            this.btnPacketsClear.Text = "Clear";
            this.btnPacketsClear.Click += new System.EventHandler(this.btnClear_Click);
            // 
            // toolStripSeparator14
            // 
            this.toolStripSeparator14.Name = "toolStripSeparator14";
            this.toolStripSeparator14.Size = new System.Drawing.Size(6, 25);
            // 
            // toolStripButton4
            // 
            this.toolStripButton4.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripButton4.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton4.Image")));
            this.toolStripButton4.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton4.Name = "toolStripButton4";
            this.toolStripButton4.Size = new System.Drawing.Size(29, 22);
            this.toolStripButton4.Text = "Copy";
            this.toolStripButton4.Click += new System.EventHandler(this.btnPacketsCopy_Click);
            // 
            // toolStripSeparator18
            // 
            this.toolStripSeparator18.Name = "toolStripSeparator18";
            this.toolStripSeparator18.Size = new System.Drawing.Size(6, 25);
            // 
            // btnPacketsAutoScroll
            // 
            this.btnPacketsAutoScroll.Checked = true;
            this.btnPacketsAutoScroll.CheckState = System.Windows.Forms.CheckState.Checked;
            this.btnPacketsAutoScroll.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.btnPacketsAutoScroll.Image = ((System.Drawing.Image)(resources.GetObject("btnPacketsAutoScroll.Image")));
            this.btnPacketsAutoScroll.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnPacketsAutoScroll.Name = "btnPacketsAutoScroll";
            this.btnPacketsAutoScroll.Size = new System.Drawing.Size(51, 22);
            this.btnPacketsAutoScroll.Text = "Auto Scroll";
            this.btnPacketsAutoScroll.Click += new System.EventHandler(this.toolStripButton_Click);
            // 
            // tC
            // 
            this.tC.Controls.Add(this.tabInfo);
            this.tC.Controls.Add(this.tabPackets);
            this.tC.Controls.Add(this.tabDebug);
            this.tC.Controls.Add(this.tabUni);
            this.tC.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tC.HotTrack = true;
            this.tC.Location = new System.Drawing.Point(215, 77);
            this.tC.Multiline = true;
            this.tC.Name = "tC";
            this.tC.SelectedIndex = 0;
            this.tC.Size = new System.Drawing.Size(866, 557);
            this.tC.TabIndex = 67;
            // 
            // tabInfo
            // 
            this.tabInfo.Controls.Add(this.rtbNotes);
            this.tabInfo.Controls.Add(this.rtbInfo);
            this.tabInfo.Location = new System.Drawing.Point(4, 22);
            this.tabInfo.Name = "tabInfo";
            this.tabInfo.Padding = new System.Windows.Forms.Padding(3);
            this.tabInfo.Size = new System.Drawing.Size(858, 531);
            this.tabInfo.TabIndex = 22;
            this.tabInfo.Text = "Info";
            this.tabInfo.UseVisualStyleBackColor = true;
            // 
            // rtbNotes
            // 
            this.rtbNotes.BackColor = System.Drawing.SystemColors.Info;
            this.rtbNotes.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.rtbNotes.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rtbNotes.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rtbNotes.Location = new System.Drawing.Point(3, 40);
            this.rtbNotes.Name = "rtbNotes";
            this.rtbNotes.ReadOnly = true;
            this.rtbNotes.Size = new System.Drawing.Size(852, 488);
            this.rtbNotes.TabIndex = 4;
            this.rtbNotes.Text = "Will be loaded at run-time from the notesUI.rtf file.";
            this.rtbNotes.LinkClicked += new System.Windows.Forms.LinkClickedEventHandler(this.rtbNotes_LinkClicked);
            // 
            // rtbInfo
            // 
            this.rtbInfo.BackColor = System.Drawing.SystemColors.Info;
            this.rtbInfo.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.rtbInfo.Dock = System.Windows.Forms.DockStyle.Top;
            this.rtbInfo.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rtbInfo.Location = new System.Drawing.Point(3, 3);
            this.rtbInfo.Name = "rtbInfo";
            this.rtbInfo.ReadOnly = true;
            this.rtbInfo.Size = new System.Drawing.Size(852, 37);
            this.rtbInfo.TabIndex = 3;
            this.rtbInfo.Text = "Quick UI for Scytale-C";
            // 
            // dataGridViewTextBoxColumn16
            // 
            this.dataGridViewTextBoxColumn16.DataPropertyName = "BulletinBoard";
            this.dataGridViewTextBoxColumn16.HeaderText = "BulletinBoard";
            this.dataGridViewTextBoxColumn16.Name = "dataGridViewTextBoxColumn16";
            this.dataGridViewTextBoxColumn16.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn17
            // 
            this.dataGridViewTextBoxColumn17.DataPropertyName = "Area";
            this.dataGridViewTextBoxColumn17.HeaderText = "Area";
            this.dataGridViewTextBoxColumn17.Name = "dataGridViewTextBoxColumn17";
            this.dataGridViewTextBoxColumn17.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.DataPropertyName = "BulletinBoard";
            this.dataGridViewTextBoxColumn1.HeaderText = "BulletinBoard";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.ReadOnly = true;
            this.dataGridViewTextBoxColumn1.Visible = false;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.DataPropertyName = "Area";
            this.dataGridViewTextBoxColumn2.HeaderText = "Area";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.ReadOnly = true;
            this.dataGridViewTextBoxColumn2.Visible = false;
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.DataPropertyName = "BulletinBoard";
            this.dataGridViewTextBoxColumn3.HeaderText = "BulletinBoard";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            this.dataGridViewTextBoxColumn3.ReadOnly = true;
            this.dataGridViewTextBoxColumn3.Visible = false;
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.DataPropertyName = "Area";
            this.dataGridViewTextBoxColumn4.HeaderText = "Area";
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            this.dataGridViewTextBoxColumn4.ReadOnly = true;
            this.dataGridViewTextBoxColumn4.Visible = false;
            // 
            // dataGridViewTextBoxColumn5
            // 
            this.dataGridViewTextBoxColumn5.DataPropertyName = "BulletinBoard";
            this.dataGridViewTextBoxColumn5.HeaderText = "BulletinBoard";
            this.dataGridViewTextBoxColumn5.Name = "dataGridViewTextBoxColumn5";
            this.dataGridViewTextBoxColumn5.ReadOnly = true;
            this.dataGridViewTextBoxColumn5.Visible = false;
            // 
            // dataGridViewTextBoxColumn6
            // 
            this.dataGridViewTextBoxColumn6.DataPropertyName = "Area";
            this.dataGridViewTextBoxColumn6.HeaderText = "Area";
            this.dataGridViewTextBoxColumn6.Name = "dataGridViewTextBoxColumn6";
            this.dataGridViewTextBoxColumn6.ReadOnly = true;
            this.dataGridViewTextBoxColumn6.Visible = false;
            // 
            // dataGridViewTextBoxColumn7
            // 
            this.dataGridViewTextBoxColumn7.DataPropertyName = "BulletinBoard";
            this.dataGridViewTextBoxColumn7.HeaderText = "BulletinBoard";
            this.dataGridViewTextBoxColumn7.Name = "dataGridViewTextBoxColumn7";
            this.dataGridViewTextBoxColumn7.Visible = false;
            // 
            // dataGridViewTextBoxColumn8
            // 
            this.dataGridViewTextBoxColumn8.DataPropertyName = "Area";
            this.dataGridViewTextBoxColumn8.HeaderText = "Area";
            this.dataGridViewTextBoxColumn8.Name = "dataGridViewTextBoxColumn8";
            this.dataGridViewTextBoxColumn8.Visible = false;
            // 
            // dataGridViewTextBoxColumn9
            // 
            this.dataGridViewTextBoxColumn9.DataPropertyName = "BulletinBoard";
            this.dataGridViewTextBoxColumn9.HeaderText = "BulletinBoard";
            this.dataGridViewTextBoxColumn9.Name = "dataGridViewTextBoxColumn9";
            this.dataGridViewTextBoxColumn9.ReadOnly = true;
            this.dataGridViewTextBoxColumn9.Visible = false;
            // 
            // dataGridViewTextBoxColumn10
            // 
            this.dataGridViewTextBoxColumn10.DataPropertyName = "Area";
            this.dataGridViewTextBoxColumn10.HeaderText = "Area";
            this.dataGridViewTextBoxColumn10.Name = "dataGridViewTextBoxColumn10";
            this.dataGridViewTextBoxColumn10.ReadOnly = true;
            this.dataGridViewTextBoxColumn10.Visible = false;
            // 
            // dataGridViewTextBoxColumn11
            // 
            this.dataGridViewTextBoxColumn11.DataPropertyName = "BulletinBoard";
            this.dataGridViewTextBoxColumn11.HeaderText = "BulletinBoard";
            this.dataGridViewTextBoxColumn11.Name = "dataGridViewTextBoxColumn11";
            this.dataGridViewTextBoxColumn11.ReadOnly = true;
            this.dataGridViewTextBoxColumn11.Visible = false;
            // 
            // dataGridViewTextBoxColumn12
            // 
            this.dataGridViewTextBoxColumn12.DataPropertyName = "Area";
            this.dataGridViewTextBoxColumn12.HeaderText = "Area";
            this.dataGridViewTextBoxColumn12.Name = "dataGridViewTextBoxColumn12";
            this.dataGridViewTextBoxColumn12.ReadOnly = true;
            this.dataGridViewTextBoxColumn12.Visible = false;
            // 
            // dataGridViewTextBoxColumn13
            // 
            this.dataGridViewTextBoxColumn13.DataPropertyName = "BulletinBoard";
            this.dataGridViewTextBoxColumn13.HeaderText = "BulletinBoard";
            this.dataGridViewTextBoxColumn13.Name = "dataGridViewTextBoxColumn13";
            this.dataGridViewTextBoxColumn13.ReadOnly = true;
            this.dataGridViewTextBoxColumn13.Visible = false;
            // 
            // dataGridViewTextBoxColumn14
            // 
            this.dataGridViewTextBoxColumn14.DataPropertyName = "Area";
            this.dataGridViewTextBoxColumn14.HeaderText = "Area";
            this.dataGridViewTextBoxColumn14.Name = "dataGridViewTextBoxColumn14";
            this.dataGridViewTextBoxColumn14.ReadOnly = true;
            this.dataGridViewTextBoxColumn14.Visible = false;
            // 
            // dataGridViewTextBoxColumn15
            // 
            this.dataGridViewTextBoxColumn15.DataPropertyName = "BulletinBoard";
            this.dataGridViewTextBoxColumn15.HeaderText = "BulletinBoard";
            this.dataGridViewTextBoxColumn15.Name = "dataGridViewTextBoxColumn15";
            this.dataGridViewTextBoxColumn15.ReadOnly = true;
            this.dataGridViewTextBoxColumn15.Visible = false;
            // 
            // dataGridViewTextBoxColumn18
            // 
            this.dataGridViewTextBoxColumn18.DataPropertyName = "Area";
            this.dataGridViewTextBoxColumn18.HeaderText = "Area";
            this.dataGridViewTextBoxColumn18.Name = "dataGridViewTextBoxColumn18";
            this.dataGridViewTextBoxColumn18.ReadOnly = true;
            this.dataGridViewTextBoxColumn18.Visible = false;
            // 
            // dataGridViewTextBoxColumn19
            // 
            this.dataGridViewTextBoxColumn19.DataPropertyName = "BulletinBoard";
            this.dataGridViewTextBoxColumn19.HeaderText = "BulletinBoard";
            this.dataGridViewTextBoxColumn19.Name = "dataGridViewTextBoxColumn19";
            this.dataGridViewTextBoxColumn19.ReadOnly = true;
            this.dataGridViewTextBoxColumn19.Visible = false;
            // 
            // dataGridViewTextBoxColumn20
            // 
            this.dataGridViewTextBoxColumn20.DataPropertyName = "Area";
            this.dataGridViewTextBoxColumn20.HeaderText = "Area";
            this.dataGridViewTextBoxColumn20.Name = "dataGridViewTextBoxColumn20";
            this.dataGridViewTextBoxColumn20.ReadOnly = true;
            this.dataGridViewTextBoxColumn20.Visible = false;
            // 
            // dataGridViewTextBoxColumn21
            // 
            this.dataGridViewTextBoxColumn21.DataPropertyName = "BulletinBoard";
            this.dataGridViewTextBoxColumn21.HeaderText = "BulletinBoard";
            this.dataGridViewTextBoxColumn21.Name = "dataGridViewTextBoxColumn21";
            this.dataGridViewTextBoxColumn21.ReadOnly = true;
            this.dataGridViewTextBoxColumn21.Visible = false;
            // 
            // dataGridViewTextBoxColumn22
            // 
            this.dataGridViewTextBoxColumn22.DataPropertyName = "Area";
            this.dataGridViewTextBoxColumn22.HeaderText = "Area";
            this.dataGridViewTextBoxColumn22.Name = "dataGridViewTextBoxColumn22";
            this.dataGridViewTextBoxColumn22.ReadOnly = true;
            this.dataGridViewTextBoxColumn22.Visible = false;
            // 
            // dataGridViewTextBoxColumn23
            // 
            this.dataGridViewTextBoxColumn23.DataPropertyName = "BulletinBoard";
            this.dataGridViewTextBoxColumn23.HeaderText = "BulletinBoard";
            this.dataGridViewTextBoxColumn23.Name = "dataGridViewTextBoxColumn23";
            this.dataGridViewTextBoxColumn23.Visible = false;
            // 
            // dataGridViewTextBoxColumn24
            // 
            this.dataGridViewTextBoxColumn24.DataPropertyName = "Area";
            this.dataGridViewTextBoxColumn24.HeaderText = "Area";
            this.dataGridViewTextBoxColumn24.Name = "dataGridViewTextBoxColumn24";
            this.dataGridViewTextBoxColumn24.Visible = false;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.ClientSize = new System.Drawing.Size(1081, 656);
            this.Controls.Add(this.lblVersion);
            this.Controls.Add(this.ll5);
            this.Controls.Add(this.tC);
            this.Controls.Add(this.panelMenu);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.statusStrip1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "MainForm";
            this.Text = "Quick UI for Scytale-C";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainForm_FormClosing);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.MainForm_FormClosed);
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panelMenu.ResumeLayout(false);
            this.gbFrames.ResumeLayout(false);
            this.gbFrames.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nChartDarkening)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nText2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nTextG)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nTextB)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nTextR)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nBkg2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nBkgB)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nBkgG)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nBkgR)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nMaxStoredHours)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nMaxDisplayedDebugs)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nMaxDisplayedPackets)).EndInit();
            this.gbRepeater.ResumeLayout(false);
            this.gbRepeater.PerformLayout();
            this.tabUni.ResumeLayout(false);
            this.panel9.ResumeLayout(false);
            this.pnlUniMap.ResumeLayout(false);
            this.pnlUniPriority.ResumeLayout(false);
            this.panel14.ResumeLayout(false);
            this.panel14.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvUni)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.unisBindingSource)).EndInit();
            this.toolStrip2.ResumeLayout(false);
            this.toolStrip2.PerformLayout();
            this.panel15.ResumeLayout(false);
            this.panel15.PerformLayout();
            this.tabDebug.ResumeLayout(false);
            this.tabDebug.PerformLayout();
            this.tsBlocks.ResumeLayout(false);
            this.tsBlocks.PerformLayout();
            this.tabPackets.ResumeLayout(false);
            this.tabPackets.PerformLayout();
            this.tsPackets.ResumeLayout(false);
            this.tsPackets.PerformLayout();
            this.tC.ResumeLayout(false);
            this.tabInfo.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button btnCredits;
        private System.Windows.Forms.Button btnStart;
        private System.Windows.Forms.Button btnMenu;
        private System.Windows.Forms.Panel panelMenu;
        private System.Windows.Forms.GroupBox gbFrames;
        private System.Windows.Forms.CheckBox cb92;
        private System.Windows.Forms.GroupBox gbRepeater;
        private System.Windows.Forms.Label ll1;
        private System.Windows.Forms.TextBox txtRawFrameUDPPort;
        private System.Windows.Forms.CheckBox cbBE;
        private System.Windows.Forms.CheckBox cbBD;
        private System.Windows.Forms.CheckBox cbB2;
        private System.Windows.Forms.CheckBox cbB1;
        private System.Windows.Forms.CheckBox cbAB;
        private System.Windows.Forms.CheckBox cbAA;
        private System.Windows.Forms.CheckBox cb7D;
        private System.Windows.Forms.CheckBox cb6C;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.Label ll5;
        private System.Windows.Forms.CheckBox cbAD;
        private System.Windows.Forms.CheckBox cb91;
        private System.Windows.Forms.CheckBox cbA0;
        private System.Windows.Forms.CheckBox cbAC;
        private System.Windows.Forms.CheckBox cb08;
        private System.Windows.Forms.CheckBox cb2A;
        private System.Windows.Forms.CheckBox cb27;
        private System.Windows.Forms.CheckBox cb9A;
        private System.Windows.Forms.CheckBox cb83;
        private System.Windows.Forms.Label lblVersion;
        private System.Windows.Forms.Timer udpConsumerTimer;
        private System.Windows.Forms.Label lbl9;
        private System.Windows.Forms.Label lbl7;
        private System.Windows.Forms.Label lbl4;
        private System.Windows.Forms.Label lbl1;
        private System.Windows.Forms.Label lbl0;
        private System.Windows.Forms.Label lbl0a;
        private System.Windows.Forms.Label lbl9a;
        private System.Windows.Forms.Label lbl7a;
        private System.Windows.Forms.Label lbl4a;
        private System.Windows.Forms.Label lbl1a;
        private System.Windows.Forms.CheckBox cb81;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.NumericUpDown nMaxDisplayedDebugs;
        private System.Windows.Forms.Label ll3;
        private System.Windows.Forms.NumericUpDown nMaxDisplayedPackets;
        private System.Windows.Forms.Label ll2;
        private System.Windows.Forms.CheckBox cbA8;
        private System.Windows.Forms.CheckBox cbA3;
        private System.Windows.Forms.CheckBox cbUseLocalDbStore;
        private System.Windows.Forms.Timer dbFlushToDiskTimer;
        private System.Windows.Forms.BindingSource unisBindingSource;
        private System.Windows.Forms.TabPage tabUni;
        private System.Windows.Forms.Panel panel9;
        private System.Windows.Forms.Panel pnlUniMap;
        private GMap.NET.WindowsForms.GMapControl uniGMap;
        private System.Windows.Forms.Splitter splitterUniMap;
        internal System.Windows.Forms.RichTextBox uniMsgBox;
        private System.Windows.Forms.Panel pnlUniPriority;
        private System.Windows.Forms.Label lblUniServiceAddress;
        private System.Windows.Forms.Splitter uniGridSplitter;
        private System.Windows.Forms.Panel panel14;
        private System.Windows.Forms.DataGridView dgvUni;
        private System.Windows.Forms.Panel panel15;
        private System.Windows.Forms.Label ll14;
        private System.Windows.Forms.Label ll22;
        private System.Windows.Forms.Label ll13;
        private System.Windows.Forms.Label ll16;
        private System.Windows.Forms.Label ll21;
        private System.Windows.Forms.Label ll23;
        private System.Windows.Forms.Label ll24;
        private System.Windows.Forms.Label ll15;
        private System.Windows.Forms.Label lblUniStatus;
        private System.Windows.Forms.Label lblUniServices;
        private System.Windows.Forms.Label ll8;
        private System.Windows.Forms.Label lblUniChannelTypeAndSat;
        private System.Windows.Forms.Label ll7;
        private System.Windows.Forms.Label ll6;
        private System.Windows.Forms.Label ll9;
        private System.Windows.Forms.Label ll12;
        private System.Windows.Forms.Label ll17;
        private System.Windows.Forms.Label ll19;
        private System.Windows.Forms.Label ll20;
        private System.Windows.Forms.Label ll10;
        private System.Windows.Forms.Label ll18;
        private System.Windows.Forms.Label ll11;
        private System.Windows.Forms.TabPage tabDebug;
        internal System.Windows.Forms.RichTextBox rtbDebug;
        private System.Windows.Forms.TabPage tabPackets;
        internal System.Windows.Forms.RichTextBox rtbPackets;
        internal System.Windows.Forms.ToolStrip tsPackets;
        private System.Windows.Forms.ToolStripButton btnPacketsClear;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator14;
        private System.Windows.Forms.ToolStripButton toolStripButton4;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator18;
        private System.Windows.Forms.ToolStripButton btnPacketsAutoScroll;
        private System.Windows.Forms.TabControl tC;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn16;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn17;
        private System.Windows.Forms.Label lblUniPriority;
        private System.Windows.Forms.Label lbl6a;
        private System.Windows.Forms.Label lbl3a;
        private System.Windows.Forms.Label lbl6;
        private System.Windows.Forms.Label lbl3;
        private System.Windows.Forms.Label lbl8a;
        private System.Windows.Forms.Label lbl5a;
        private System.Windows.Forms.Label lbl2a;
        private System.Windows.Forms.Label lbl8;
        private System.Windows.Forms.Label lbl5;
        private System.Windows.Forms.Label lbl2;
        internal System.Windows.Forms.ToolStrip tsBlocks;
        private System.Windows.Forms.ToolStripButton btnDebugClear;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
        private System.Windows.Forms.ToolStripButton btnDebugCopy;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator17;
        private System.Windows.Forms.ToolStripButton btnDebugAutoScroll;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        internal System.Windows.Forms.ToolStripButton btnJson;
        internal System.Windows.Forms.ToolStrip toolStrip2;
        private System.Windows.Forms.ToolStripButton btnUniAutoScroll;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripButton btnUniAutoLastSelect;
        private System.Windows.Forms.NumericUpDown nMaxStoredHours;
        private System.Windows.Forms.Label ll4;
        private System.Windows.Forms.CheckBox cbDarkTheme;
        private System.Windows.Forms.NumericUpDown nBkgB;
        private System.Windows.Forms.NumericUpDown nBkgG;
        private System.Windows.Forms.NumericUpDown nBkgR;
        private System.Windows.Forms.NumericUpDown nBkg2;
        private System.Windows.Forms.NumericUpDown nChartDarkening;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.Label ll25;
        private System.Windows.Forms.Label ll27;
        private System.Windows.Forms.Label ll26;
        private System.Windows.Forms.NumericUpDown nText2;
        private System.Windows.Forms.NumericUpDown nTextG;
        private System.Windows.Forms.NumericUpDown nTextB;
        private System.Windows.Forms.NumericUpDown nTextR;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripButton toolStripButton1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn5;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn6;
        private System.Windows.Forms.FontDialog fontDialog1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn7;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn8;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn9;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn10;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn11;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn12;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn13;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn14;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn15;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn18;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn19;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn20;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn21;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn22;
        private System.Windows.Forms.DataGridViewTextBoxColumn UniPriority;
        private System.Windows.Forms.DataGridViewTextBoxColumn UniEncoding;
        private System.Windows.Forms.DataGridViewTextBoxColumn UniTime;
        private System.Windows.Forms.DataGridViewTextBoxColumn mIdOrLCNoDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn UniRepetition;
        private System.Windows.Forms.DataGridViewTextBoxColumn UniPartsReceived;
        private System.Windows.Forms.DataGridViewCheckBoxColumn UniIsPartial;
        private System.Windows.Forms.DataGridViewTextBoxColumn UniStation;
        private System.Windows.Forms.DataGridViewTextBoxColumn UniSatName;
        private System.Windows.Forms.DataGridViewTextBoxColumn UniSat;
        private System.Windows.Forms.DataGridViewTextBoxColumn UniStreamId;
        private System.Windows.Forms.DataGridViewTextBoxColumn uniIdDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn streamIdDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewCheckBoxColumn UniIsEGC;
        private System.Windows.Forms.DataGridViewTextBoxColumn satDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn satNameDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn lesIdDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn lesNameDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn timeDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn timeReceivedDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn repetitionDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn messageTypeDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn priorityDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn contentTypeDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn partsReceivedDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn messageDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewCheckBoxColumn isPartialDataGridViewCheckBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn addressHexDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn bulletinBoardDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn areaDataGridViewTextBoxColumn;
        private System.Windows.Forms.CheckBox cbLogMessages;
        private System.Windows.Forms.Label ll29;
        private System.Windows.Forms.Button btnLogFolder;
        private System.Windows.Forms.TextBox txtLogFolder;
        private System.Windows.Forms.CheckBox cbSavePackets;
        private System.Windows.Forms.Label ll28;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox txtFont;
        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog1;
        private System.Windows.Forms.CheckBox cbLogRaw;
        private System.Windows.Forms.TabPage tabInfo;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn23;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn24;
        private System.Windows.Forms.RichTextBox rtbInfo;
        private System.Windows.Forms.RichTextBox rtbNotes;
    }
}

